-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-08-2016 a las 23:18:52
-- Versión del servidor: 5.6.16
-- Versión de PHP: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `medkeep`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blog_categorias`
--

CREATE TABLE IF NOT EXISTS `blog_categorias` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `categoria` varchar(50) DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `activo` tinyint(4) DEFAULT NULL,
  `fecha_creado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blog_comentarios`
--

CREATE TABLE IF NOT EXISTS `blog_comentarios` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cod_usuario` tinyint(4) DEFAULT NULL,
  `cod_noticia` tinyint(4) DEFAULT NULL,
  `asunto` varchar(50) DEFAULT NULL,
  `comentario` mediumtext,
  `publicado` tinyint(4) DEFAULT NULL,
  `fecha_creado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blog_noticias`
--

CREATE TABLE IF NOT EXISTS `blog_noticias` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `titulo_noticia` varchar(255) DEFAULT NULL,
  `noticia_corta` mediumtext,
  `noticia_completa` mediumtext,
  `caratula_noticia` varchar(255) DEFAULT NULL,
  `publicada` tinyint(4) DEFAULT NULL,
  `cod_categoria` tinyint(4) DEFAULT NULL,
  `cod_usuario` tinyint(4) DEFAULT NULL,
  `fecha_creado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cat_tipos_documento`
--

CREATE TABLE IF NOT EXISTS `cat_tipos_documento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(100) NOT NULL DEFAULT '0',
  `activo` tinyint(4) NOT NULL DEFAULT '0',
  `fecha_creado` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `cat_tipos_documento`
--

INSERT INTO `cat_tipos_documento` (`id`, `tipo`, `activo`, `fecha_creado`) VALUES
(1, 'Receta médica', 1, '0000-00-00 00:00:00'),
(2, 'Estudio médico', 1, '0000-00-00 00:00:00'),
(3, 'Radiografía', 1, '0000-00-00 00:00:00'),
(4, 'Expediente', 1, '0000-00-00 00:00:00'),
(5, 'Nota médica', 1, '0000-00-00 00:00:00'),
(6, 'Certificado de salud', 1, '0000-00-00 00:00:00'),
(7, 'Otro', 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cat_tipo_usuario`
--

CREATE TABLE IF NOT EXISTS `cat_tipo_usuario` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tipo` varchar(20) DEFAULT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT NULL,
  `fecha_creado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `cat_tipo_usuario`
--

INSERT INTO `cat_tipo_usuario` (`id`, `tipo`, `descripcion`, `activo`, `fecha_creado`) VALUES
(1, 'registrado', 'Registrado', 1, '2016-07-07 00:00:00'),
(2, 'suscrito', 'Suscrito', 1, '2016-07-07 00:00:00'),
(3, 'administrador', 'Administrador', 1, '2016-07-07 00:00:00'),
(4, 'doctor', 'Doctor(a)', 0, '2016-07-11 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cat_usuarios`
--

CREATE TABLE IF NOT EXISTS `cat_usuarios` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `usuario` varchar(10) DEFAULT NULL,
  `correo` varchar(50) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `nombre` varchar(30) DEFAULT NULL,
  `ape_paterno` varchar(30) DEFAULT NULL,
  `ape_materno` varchar(30) DEFAULT NULL,
  `domicilio` varchar(100) DEFAULT NULL,
  `colonia` varchar(50) DEFAULT NULL,
  `ciudad` varchar(100) DEFAULT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT NULL,
  `codigo` varchar(50) DEFAULT NULL,
  `cod_tipo` tinyint(1) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `fecha_creado` datetime DEFAULT NULL,
  `fecha_actualizado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `cat_usuarios`
--

INSERT INTO `cat_usuarios` (`id`, `usuario`, `correo`, `password`, `nombre`, `ape_paterno`, `ape_materno`, `domicilio`, `colonia`, `ciudad`, `telefono`, `activo`, `codigo`, `cod_tipo`, `foto`, `fecha_creado`, `fecha_actualizado`) VALUES
(3, NULL, 'admin@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'Admin', 'Admin', 'Admin', NULL, NULL, NULL, '4737351400', 1, NULL, 3, 'uncle_sam.jpg', '2016-07-14 00:21:21', NULL),
(4, NULL, 'fabishodev@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'Fabricio ', 'Magaña', 'Ruiz', '', '', '', '4731081408', 1, NULL, 2, NULL, '2016-07-29 00:50:21', '2016-08-04 18:05:24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `citas_pacientes`
--

CREATE TABLE IF NOT EXISTS `citas_pacientes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cod_usuario` int(11) DEFAULT NULL,
  `cod_doctor` int(11) DEFAULT NULL,
  `doctor` varchar(100) DEFAULT NULL,
  `cod_hospital` int(11) DEFAULT NULL,
  `hospital` varchar(100) DEFAULT NULL,
  `fecha_cita` date DEFAULT NULL,
  `hora_cita` varchar(5) DEFAULT NULL,
  `motivo_cita` mediumtext,
  `confirmacion` tinyint(4) DEFAULT NULL,
  `nota` mediumtext,
  `fecha_creado` datetime DEFAULT NULL,
  `fecha_actualizado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Volcado de datos para la tabla `citas_pacientes`
--

INSERT INTO `citas_pacientes` (`id`, `cod_usuario`, `cod_doctor`, `doctor`, `cod_hospital`, `hospital`, `fecha_cita`, `hora_cita`, `motivo_cita`, `confirmacion`, `nota`, `fecha_creado`, `fecha_actualizado`) VALUES
(9, 4, NULL, 'Hector Banda', NULL, 'T1 IMSS', '2016-08-06', '13:15', 'Prueba maximo citas', 0, NULL, '2016-08-05 13:05:36', NULL),
(10, 4, NULL, 'Hector Banda', NULL, 'T1 IMSS', '2016-08-08', '13:15', 'Prueba maximo citas', 0, NULL, '2016-08-05 13:05:53', NULL),
(11, 4, NULL, 'Hector Banda', NULL, 'T1 IMSS', '2016-08-09', '16:15', 'Prueba de cita notificacion', 0, NULL, '2016-08-05 16:11:51', NULL),
(12, 4, NULL, 'Hector Banda', NULL, 'T1 IMSS', '2016-08-09', '16:15', 'Prueba de cita notificacion', 0, NULL, '2016-08-05 16:12:04', NULL),
(13, 4, NULL, 'Hector Banda', NULL, 'T1 IMSS', '2016-08-10', '16:15', 'Prueba de cita notificacion', 0, NULL, '2016-08-05 16:14:03', NULL),
(14, 4, NULL, 'Hector Banda', NULL, 'T1 IMSS', '2016-08-10', '16:15', 'Prueba de cita notificacion', 0, NULL, '2016-08-05 16:15:28', NULL),
(15, 4, NULL, 'Hector Banda', NULL, 'T1 IMSS', '2016-08-19', '16:30', 'Prueba de cita notificacion', 0, NULL, '2016-08-05 16:17:01', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto_mensajes`
--

CREATE TABLE IF NOT EXISTS `contacto_mensajes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `correo` varchar(255) DEFAULT NULL,
  `asunto` varchar(150) DEFAULT NULL,
  `mensaje` text,
  `fecha_creado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos_doctores`
--

CREATE TABLE IF NOT EXISTS `datos_doctores` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cod_usuario` tinyint(4) DEFAULT NULL,
  `especialidad` varchar(50) DEFAULT NULL,
  `cedula_profesional` varchar(20) DEFAULT NULL,
  `fecha_creado` datetime DEFAULT NULL,
  `fecha_actualizado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentos_pacientes`
--

CREATE TABLE IF NOT EXISTS `documentos_pacientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_usuario` tinyint(4) DEFAULT '0',
  `cod_doctor` tinyint(4) DEFAULT '0',
  `doctor` varchar(100) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT '0',
  `cod_tipo_documento` tinyint(4) DEFAULT NULL,
  `hospital` varchar(100) DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT '0',
  `nombre_archivo` varchar(255) DEFAULT NULL,
  `ruta` varchar(100) DEFAULT '0',
  `fecha_creado` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf32 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `documentos_pacientes`
--

INSERT INTO `documentos_pacientes` (`id`, `cod_usuario`, `cod_doctor`, `doctor`, `nombre`, `cod_tipo_documento`, `hospital`, `descripcion`, `nombre_archivo`, `ruta`, `fecha_creado`) VALUES
(8, 4, NULL, 'Hector Banda', 'Receta resfriado', 1, 'T1 IMSS', 'Receta de tratamiento para resfriado', 'test.pdf', 'media/4/test.pdf', '2016-08-05 14:39:22'),
(9, 4, NULL, 'Hector Banda', 'Receta resfriado', 1, 'T1 IMSS', 'Receta de tratamiento para resfriado', 'justificacion-2da-junio.pdf', 'media/4/justificacion-2da-junio.pdf', '2016-08-05 14:49:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `programacion_citas`
--

CREATE TABLE IF NOT EXISTS `programacion_citas` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(150) DEFAULT NULL,
  `body` mediumtext NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `class` varchar(45) NOT NULL DEFAULT 'info',
  `start` varchar(15) NOT NULL,
  `end` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `programacion_citas`
--

INSERT INTO `programacion_citas` (`id`, `title`, `body`, `url`, `class`, `start`, `end`) VALUES
(1, '<p>Cita con Dr(a): </p><p>Motivo: Revision general</p><p>En: Hospital Angeles</p><p>Hora: 19:15</p>', 'Revision general', NULL, 'event-warning', '1470442500000', '1470442500000'),
(2, '<p>Cita con Dr(a): Gibran Estrada</p><p>Motivo: Odontologia</p><p>En: Torre M1</p><p>Hora: 10:00</p>', 'Odontologia', NULL, 'event-warning', '1470495600000', '1470495600000'),
(3, '<p>Cita con Dr(a): Hector Banda</p><p>Motivo: Prueba maximo citas</p><p>En: T1 IMSS</p><p>Hora: 12:45</p>', 'Prueba maximo citas', NULL, 'event-warning', '1470678300000', '1470678300000'),
(4, '<p>Cita con Dr(a): Hector Banda</p><p>Motivo: Prueba maximo citas</p><p>En: T1 IMSS</p><p>Hora: 12:45</p>', 'Prueba maximo citas', NULL, 'event-warning', '1470764700000', '1470764700000'),
(5, '<p>Cita con Dr(a): Hector Banda</p><p>Motivo: Prueba maximo citas</p><p>En: T1 IMSS</p><p>Hora: 13:00</p>', 'Prueba maximo citas', NULL, 'event-warning', '1470506400000', '1470506400000'),
(6, '<p>Cita con Dr(a): Hector Banda</p><p>Motivo: Prueba maximo citas</p><p>En: T1 IMSS</p><p>Hora: 13:00</p>', 'Prueba maximo citas', NULL, 'event-warning', '1470679200000', '1470679200000'),
(7, '<p>Cita con Dr(a): Hector Banda</p><p>Motivo: Prueba maximo citas</p><p>En: T1 IMSS</p><p>Hora: 13:15</p>', 'Prueba maximo citas', NULL, 'event-warning', '1470507300000', '1470507300000'),
(8, '<p>Cita con Dr(a): Hector Banda</p><p>Motivo: Prueba maximo citas</p><p>En: T1 IMSS</p><p>Hora: 13:15</p>', 'Prueba maximo citas', NULL, 'event-warning', '1470680100000', '1470680100000'),
(9, '<p>Cita con Dr(a): Hector Banda</p><p>Motivo: Prueba maximo citas</p><p>En: T1 IMSS</p><p>Hora: 13:15</p>', 'Prueba maximo citas', NULL, 'event-warning', '1470507300000', '1470507300000'),
(10, '<p>Cita con Dr(a): Hector Banda</p><p>Motivo: Prueba maximo citas</p><p>En: T1 IMSS</p><p>Hora: 13:15</p>', 'Prueba maximo citas', NULL, 'event-warning', '1470680100000', '1470680100000'),
(11, '<p>Cita con Dr(a): Hector Banda</p><p>Motivo: Prueba de cita notificacion</p><p>En: T1 IMSS</p><p>Hora: 16:15</p>', 'Prueba de cita notificacion', NULL, 'event-warning', '1470777300000', '1470777300000'),
(12, '<p>Cita con Dr(a): Hector Banda</p><p>Motivo: Prueba de cita notificacion</p><p>En: T1 IMSS</p><p>Hora: 16:15</p>', 'Prueba de cita notificacion', NULL, 'event-warning', '1470777300000', '1470777300000'),
(13, '<p>Cita con Dr(a): Hector Banda</p><p>Motivo: Prueba de cita notificacion</p><p>En: T1 IMSS</p><p>Hora: 16:15</p>', 'Prueba de cita notificacion', NULL, 'event-warning', '1470863700000', '1470863700000'),
(14, '<p>Cita con Dr(a): Hector Banda</p><p>Motivo: Prueba de cita notificacion</p><p>En: T1 IMSS</p><p>Hora: 16:15</p>', 'Prueba de cita notificacion', NULL, 'event-warning', '1470863700000', '1470863700000'),
(15, '<p>Cita con Dr(a): Hector Banda</p><p>Motivo: Prueba de cita notificacion</p><p>En: T1 IMSS</p><p>Hora: 16:30</p>', 'Prueba de cita notificacion', NULL, 'event-warning', '1471642200000', '1471642200000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `programacion_tratamientos`
--

CREATE TABLE IF NOT EXISTS `programacion_tratamientos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(150) DEFAULT NULL,
  `body` mediumtext NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `class` varchar(45) NOT NULL DEFAULT 'info',
  `start` varchar(15) NOT NULL,
  `end` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tratamientos_pacientes`
--

CREATE TABLE IF NOT EXISTS `tratamientos_pacientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_usuario` int(11) DEFAULT '0',
  `fecha_inicio` datetime DEFAULT '0000-00-00 00:00:00',
  `fecha_fin` datetime DEFAULT '0000-00-00 00:00:00',
  `hora` varchar(10) DEFAULT '0',
  `medicamento` varchar(255) DEFAULT '0',
  `observaciones` mediumtext,
  `fecha_creado` datetime DEFAULT '0000-00-00 00:00:00',
  `title` varchar(150) DEFAULT NULL,
  `body` mediumtext,
  `url` varchar(150) DEFAULT NULL,
  `class` varchar(45) DEFAULT NULL,
  `start` varchar(15) DEFAULT NULL,
  `end` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `tratamientos_pacientes`
--

INSERT INTO `tratamientos_pacientes` (`id`, `cod_usuario`, `fecha_inicio`, `fecha_fin`, `hora`, `medicamento`, `observaciones`, `fecha_creado`, `title`, `body`, `url`, `class`, `start`, `end`) VALUES
(4, 4, '2016-08-06 00:00:00', '2016-08-06 00:00:00', '14:00', 'Aspirina', 'Tomar una pildora', '2016-08-05 13:50:09', '<p>Tratamiento de: Aspirina</p><p>Observaciones: Tomar una pildora</p><p>Hora: 14:00</p>', '<p>Tratamiento de: Aspirina</p><p>Observaciones: Tomar una pildora</p><p>Hora: 14:00</p>', NULL, 'event-special', '1470510000000', '1470510000000');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vw_citas_programadas`
--
CREATE TABLE IF NOT EXISTS `vw_citas_programadas` (
`id_cita` int(11) unsigned
,`cod_usuario` int(11)
,`cod_doctor` int(11)
,`fecha_cita` date
,`hora_cita` varchar(5)
,`motivo_cita` mediumtext
,`confirmacion` tinyint(4)
,`nota` mediumtext
,`fecha_creado` datetime
,`fecha_actualizado` datetime
,`nombre` varchar(30)
,`ape_paterno` varchar(30)
,`ape_materno` varchar(30)
,`title` varchar(150)
,`nombre_doc` varchar(30)
,`ape_paterno_doc` varchar(30)
,`ape_materno_doc` varchar(30)
,`nombre_completo_paciente` varchar(92)
,`nombre_completo_doc` varchar(92)
,`title1` mediumtext
,`body` mediumtext
,`url` varchar(150)
,`clase` varchar(45)
,`start` varchar(15)
,`end` varchar(15)
,`class` varchar(15)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vw_cita_paciente`
--
CREATE TABLE IF NOT EXISTS `vw_cita_paciente` (
`id_cita` int(11) unsigned
,`cod_usuario` int(11)
,`nombre` varchar(30)
,`ape_paterno` varchar(30)
,`ape_materno` varchar(30)
,`correo` varchar(50)
,`telefono` varchar(10)
,`cod_tipo` tinyint(1)
,`cod_doctor` int(11)
,`nombre_doc` varchar(30)
,`ape_paterno_doc` varchar(30)
,`ape_materno_doc` varchar(30)
,`correo_doc` varchar(50)
,`telefono_doc` varchar(10)
,`especialidad` varchar(50)
,`fecha_cita` date
,`hora_cita` varchar(5)
,`motivo_cita` mediumtext
,`confirmacion` tinyint(4)
,`nota` mediumtext
,`fecha_creado_cita` datetime
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vw_comentarios`
--
CREATE TABLE IF NOT EXISTS `vw_comentarios` (
`id` int(11) unsigned
,`cod_usuario` tinyint(4)
,`cod_noticia` tinyint(4)
,`asunto` varchar(50)
,`comentario` mediumtext
,`publicado` tinyint(4)
,`fecha_comentario` datetime
,`nombre` varchar(30)
,`ape_paterno` varchar(30)
,`ape_materno` varchar(30)
,`foto` varchar(255)
,`titulo_noticia` varchar(255)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vw_doctor`
--
CREATE TABLE IF NOT EXISTS `vw_doctor` (
`id` int(11) unsigned
,`usuario` varchar(10)
,`correo` varchar(50)
,`password` varchar(100)
,`nombre` varchar(30)
,`ape_paterno` varchar(30)
,`ape_materno` varchar(30)
,`domicilio` varchar(100)
,`colonia` varchar(50)
,`ciudad` varchar(100)
,`telefono` varchar(10)
,`activo` tinyint(1)
,`cod_tipo` tinyint(1)
,`fecha_creado` datetime
,`fecha_actualizado` datetime
,`id_tipo_usuario` int(11) unsigned
,`tipo` varchar(20)
,`descripcion` varchar(50)
,`especialidad` varchar(50)
,`cedula_profesional` varchar(20)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vw_documentos`
--
CREATE TABLE IF NOT EXISTS `vw_documentos` (
`id` int(11)
,`cod_usuario` tinyint(4)
,`cod_tipo` tinyint(1)
,`doctor` varchar(100)
,`cod_doctor` tinyint(4)
,`nombre_documento` varchar(100)
,`descripcion` varchar(100)
,`ruta` varchar(100)
,`nombre_archivo` varchar(255)
,`fecha_creado` datetime
,`nombre` varchar(30)
,`ape_paterno` varchar(30)
,`ape_materno` varchar(30)
,`nombre_paciente` varchar(30)
,`ape_paterno_paciente` varchar(30)
,`ape_materno_paciente` varchar(30)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vw_noticia`
--
CREATE TABLE IF NOT EXISTS `vw_noticia` (
`id` int(11) unsigned
,`titulo_noticia` varchar(255)
,`noticia_corta` mediumtext
,`noticia_completa` mediumtext
,`caratula_noticia` varchar(255)
,`publicada` tinyint(4)
,`cod_categoria` tinyint(4)
,`fecha_creado` datetime
,`categoria` varchar(50)
,`descripcion` varchar(100)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vw_tratamientos`
--
CREATE TABLE IF NOT EXISTS `vw_tratamientos` (
`id` int(11)
,`cod_usuario` int(11)
,`cod_tipo` tinyint(1)
,`fecha_inicio` datetime
,`fecha_fin` datetime
,`hora` varchar(10)
,`medicamento` varchar(255)
,`observaciones` mediumtext
,`fecha_creado` datetime
,`title` varchar(150)
,`class` varchar(45)
,`nombre` varchar(30)
,`ape_paterno` varchar(30)
,`ape_materno` varchar(30)
,`descripcion` varchar(50)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vw_usuario`
--
CREATE TABLE IF NOT EXISTS `vw_usuario` (
`id` int(11) unsigned
,`usuario` varchar(10)
,`correo` varchar(50)
,`password` varchar(100)
,`nombre` varchar(30)
,`ape_paterno` varchar(30)
,`ape_materno` varchar(30)
,`domicilio` varchar(100)
,`colonia` varchar(50)
,`ciudad` varchar(100)
,`telefono` varchar(10)
,`activo` tinyint(1)
,`cod_tipo` tinyint(1)
,`fecha_creado` datetime
,`fecha_actualizado` datetime
,`id_tipo_usuario` int(11) unsigned
,`tipo` varchar(20)
,`descripcion` varchar(50)
);
-- --------------------------------------------------------

--
-- Estructura para la vista `vw_citas_programadas`
--
DROP TABLE IF EXISTS `vw_citas_programadas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_citas_programadas` AS (select `a`.`id` AS `id_cita`,`a`.`cod_usuario` AS `cod_usuario`,`a`.`cod_doctor` AS `cod_doctor`,`a`.`fecha_cita` AS `fecha_cita`,`a`.`hora_cita` AS `hora_cita`,`a`.`motivo_cita` AS `motivo_cita`,`a`.`confirmacion` AS `confirmacion`,`a`.`nota` AS `nota`,`a`.`fecha_creado` AS `fecha_creado`,`a`.`fecha_actualizado` AS `fecha_actualizado`,`c`.`nombre` AS `nombre`,`c`.`ape_paterno` AS `ape_paterno`,`c`.`ape_materno` AS `ape_materno`,`b`.`title` AS `title`,`d`.`nombre` AS `nombre_doc`,`d`.`ape_paterno` AS `ape_paterno_doc`,`d`.`ape_materno` AS `ape_materno_doc`,concat(`c`.`nombre`,' ',`c`.`ape_paterno`,' ',`c`.`ape_materno`) AS `nombre_completo_paciente`,concat(`d`.`nombre`,' ',`d`.`ape_paterno`,' ',`d`.`ape_materno`) AS `nombre_completo_doc`,concat(`b`.`title`,' ',concat(`d`.`nombre`,' ',`d`.`ape_paterno`,' ',`d`.`ape_materno`,' Motivo: ',`a`.`motivo_cita`,' Hora: ',`a`.`hora_cita`,' Nota: ',`a`.`nota`)) AS `title1`,concat(' Motivo: ',`a`.`motivo_cita`,' Fecha: ',`a`.`fecha_cita`,' Hora: ',`a`.`hora_cita`,' Nota: ',`a`.`nota`) AS `body`,`b`.`url` AS `url`,`b`.`class` AS `clase`,`b`.`start` AS `start`,`b`.`end` AS `end`,(case `a`.`confirmacion` when 1 then 'event-success' when 0 then 'event-warning' when 2 then 'event-important' end) AS `class` from (((`citas_pacientes` `a` left join `programacion_citas` `b` on((`a`.`id` = `b`.`id`))) left join `cat_usuarios` `c` on((`a`.`cod_usuario` = `c`.`id`))) left join `cat_usuarios` `d` on((`a`.`cod_doctor` = `d`.`id`))));

-- --------------------------------------------------------

--
-- Estructura para la vista `vw_cita_paciente`
--
DROP TABLE IF EXISTS `vw_cita_paciente`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_cita_paciente` AS (select `a`.`id` AS `id_cita`,`a`.`cod_usuario` AS `cod_usuario`,`b`.`nombre` AS `nombre`,`b`.`ape_paterno` AS `ape_paterno`,`b`.`ape_materno` AS `ape_materno`,`b`.`correo` AS `correo`,`b`.`telefono` AS `telefono`,`b`.`cod_tipo` AS `cod_tipo`,`a`.`cod_doctor` AS `cod_doctor`,`c`.`nombre` AS `nombre_doc`,`c`.`ape_paterno` AS `ape_paterno_doc`,`c`.`ape_materno` AS `ape_materno_doc`,`c`.`correo` AS `correo_doc`,`c`.`telefono` AS `telefono_doc`,`c`.`especialidad` AS `especialidad`,`a`.`fecha_cita` AS `fecha_cita`,`a`.`hora_cita` AS `hora_cita`,`a`.`motivo_cita` AS `motivo_cita`,`a`.`confirmacion` AS `confirmacion`,`a`.`nota` AS `nota`,`a`.`fecha_creado` AS `fecha_creado_cita` from ((`citas_pacientes` `a` left join `vw_usuario` `b` on((`a`.`cod_usuario` = `b`.`id`))) left join `vw_doctor` `c` on((`a`.`cod_doctor` = `c`.`id`))));

-- --------------------------------------------------------

--
-- Estructura para la vista `vw_comentarios`
--
DROP TABLE IF EXISTS `vw_comentarios`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_comentarios` AS (select `a`.`id` AS `id`,`a`.`cod_usuario` AS `cod_usuario`,`a`.`cod_noticia` AS `cod_noticia`,`a`.`asunto` AS `asunto`,`a`.`comentario` AS `comentario`,`a`.`publicado` AS `publicado`,`a`.`fecha_creado` AS `fecha_comentario`,`b`.`nombre` AS `nombre`,`b`.`ape_paterno` AS `ape_paterno`,`b`.`ape_materno` AS `ape_materno`,`b`.`foto` AS `foto`,`c`.`titulo_noticia` AS `titulo_noticia` from ((`blog_comentarios` `a` left join `cat_usuarios` `b` on((`a`.`cod_usuario` = `b`.`id`))) left join `blog_noticias` `c` on((`a`.`cod_noticia` = `c`.`id`))));

-- --------------------------------------------------------

--
-- Estructura para la vista `vw_doctor`
--
DROP TABLE IF EXISTS `vw_doctor`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_doctor` AS (select `a`.`id` AS `id`,`a`.`usuario` AS `usuario`,`a`.`correo` AS `correo`,`a`.`password` AS `password`,`a`.`nombre` AS `nombre`,`a`.`ape_paterno` AS `ape_paterno`,`a`.`ape_materno` AS `ape_materno`,`a`.`domicilio` AS `domicilio`,`a`.`colonia` AS `colonia`,`a`.`ciudad` AS `ciudad`,`a`.`telefono` AS `telefono`,`a`.`activo` AS `activo`,`a`.`cod_tipo` AS `cod_tipo`,`a`.`fecha_creado` AS `fecha_creado`,`a`.`fecha_actualizado` AS `fecha_actualizado`,`b`.`id` AS `id_tipo_usuario`,`b`.`tipo` AS `tipo`,`b`.`descripcion` AS `descripcion`,`c`.`especialidad` AS `especialidad`,`c`.`cedula_profesional` AS `cedula_profesional` from ((`cat_usuarios` `a` left join `cat_tipo_usuario` `b` on((`a`.`cod_tipo` = `b`.`id`))) left join `datos_doctores` `c` on((`a`.`id` = `c`.`cod_usuario`))));

-- --------------------------------------------------------

--
-- Estructura para la vista `vw_documentos`
--
DROP TABLE IF EXISTS `vw_documentos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_documentos` AS (select `a`.`id` AS `id`,`a`.`cod_usuario` AS `cod_usuario`,`c`.`cod_tipo` AS `cod_tipo`,`a`.`doctor` AS `doctor`,`a`.`cod_doctor` AS `cod_doctor`,`a`.`nombre` AS `nombre_documento`,`a`.`descripcion` AS `descripcion`,`a`.`ruta` AS `ruta`,`a`.`nombre_archivo` AS `nombre_archivo`,`a`.`fecha_creado` AS `fecha_creado`,`b`.`nombre` AS `nombre`,`b`.`ape_paterno` AS `ape_paterno`,`b`.`ape_materno` AS `ape_materno`,`c`.`nombre` AS `nombre_paciente`,`c`.`ape_paterno` AS `ape_paterno_paciente`,`c`.`ape_materno` AS `ape_materno_paciente` from ((`documentos_pacientes` `a` left join `vw_doctor` `b` on((`a`.`cod_doctor` = `b`.`id`))) left join `vw_usuario` `c` on((`a`.`cod_usuario` = `c`.`id`))));

-- --------------------------------------------------------

--
-- Estructura para la vista `vw_noticia`
--
DROP TABLE IF EXISTS `vw_noticia`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_noticia` AS (select `a`.`id` AS `id`,`a`.`titulo_noticia` AS `titulo_noticia`,`a`.`noticia_corta` AS `noticia_corta`,`a`.`noticia_completa` AS `noticia_completa`,`a`.`caratula_noticia` AS `caratula_noticia`,`a`.`publicada` AS `publicada`,`a`.`cod_categoria` AS `cod_categoria`,`a`.`fecha_creado` AS `fecha_creado`,`b`.`categoria` AS `categoria`,`b`.`descripcion` AS `descripcion` from (`blog_noticias` `a` left join `blog_categorias` `b` on((`a`.`cod_categoria` = `b`.`id`))));

-- --------------------------------------------------------

--
-- Estructura para la vista `vw_tratamientos`
--
DROP TABLE IF EXISTS `vw_tratamientos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_tratamientos` AS (select `a`.`id` AS `id`,`a`.`cod_usuario` AS `cod_usuario`,`b`.`cod_tipo` AS `cod_tipo`,`a`.`fecha_inicio` AS `fecha_inicio`,`a`.`fecha_fin` AS `fecha_fin`,`a`.`hora` AS `hora`,`a`.`medicamento` AS `medicamento`,`a`.`observaciones` AS `observaciones`,`a`.`fecha_creado` AS `fecha_creado`,`a`.`title` AS `title`,`a`.`class` AS `class`,`b`.`nombre` AS `nombre`,`b`.`ape_paterno` AS `ape_paterno`,`b`.`ape_materno` AS `ape_materno`,`b`.`descripcion` AS `descripcion` from (`tratamientos_pacientes` `a` left join `vw_usuario` `b` on((`a`.`cod_usuario` = `b`.`id`))));

-- --------------------------------------------------------

--
-- Estructura para la vista `vw_usuario`
--
DROP TABLE IF EXISTS `vw_usuario`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_usuario` AS (select `a`.`id` AS `id`,`a`.`usuario` AS `usuario`,`a`.`correo` AS `correo`,`a`.`password` AS `password`,`a`.`nombre` AS `nombre`,`a`.`ape_paterno` AS `ape_paterno`,`a`.`ape_materno` AS `ape_materno`,`a`.`domicilio` AS `domicilio`,`a`.`colonia` AS `colonia`,`a`.`ciudad` AS `ciudad`,`a`.`telefono` AS `telefono`,`a`.`activo` AS `activo`,`a`.`cod_tipo` AS `cod_tipo`,`a`.`fecha_creado` AS `fecha_creado`,`a`.`fecha_actualizado` AS `fecha_actualizado`,`b`.`id` AS `id_tipo_usuario`,`b`.`tipo` AS `tipo`,`b`.`descripcion` AS `descripcion` from (`cat_usuarios` `a` left join `cat_tipo_usuario` `b` on((`a`.`cod_tipo` = `b`.`id`))));

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
