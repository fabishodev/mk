-- MySQL dump 10.13  Distrib 5.6.23, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: mk
-- ------------------------------------------------------
-- Server version	5.6.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `blog_categorias`
--

DROP TABLE IF EXISTS `blog_categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_categorias` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `categoria` varchar(50) DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `activo` tinyint(4) DEFAULT NULL,
  `fecha_creado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_categorias`
--

LOCK TABLES `blog_categorias` WRITE;
/*!40000 ALTER TABLE `blog_categorias` DISABLE KEYS */;
INSERT INTO `blog_categorias` VALUES (1,'Salud General','Salud general',1,NULL);
/*!40000 ALTER TABLE `blog_categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_comentarios`
--

DROP TABLE IF EXISTS `blog_comentarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_comentarios` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cod_usuario` tinyint(4) DEFAULT NULL,
  `cod_noticia` tinyint(4) DEFAULT NULL,
  `asunto` varchar(50) DEFAULT NULL,
  `comentario` mediumtext,
  `publicado` tinyint(4) DEFAULT NULL,
  `fecha_creado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_comentarios`
--

LOCK TABLES `blog_comentarios` WRITE;
/*!40000 ALTER TABLE `blog_comentarios` DISABLE KEYS */;
INSERT INTO `blog_comentarios` VALUES (1,4,1,'Prueba de comentario','Prueba de comentario',1,'2016-08-08 20:25:53'),(2,4,1,'Comentario','Cuerpo del comentario',1,'2016-08-09 11:24:46'),(3,4,2,'Prueba envio comentario','Etiam dignissim, enim nec aliquam suscipit, est lacus fermentum lorem, vitae condimentum velit dui quis metus. Ut hendrerit in ex a vulputate.',1,'2016-08-17 14:18:21'),(4,4,1,'Prueba envio comentario','Etiam dignissim, enim nec aliquam suscipit, est lacus fermentum lorem, vitae condimentum velit dui quis metus. Ut hendrerit in ex a vulputate.',1,'2016-08-17 14:18:37');
/*!40000 ALTER TABLE `blog_comentarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_noticias`
--

DROP TABLE IF EXISTS `blog_noticias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_noticias` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `titulo_noticia` varchar(255) DEFAULT NULL,
  `noticia_corta` mediumtext,
  `noticia_completa` mediumtext,
  `caratula_noticia` varchar(255) DEFAULT NULL,
  `publicada` tinyint(4) DEFAULT NULL,
  `cod_categoria` tinyint(4) DEFAULT NULL,
  `cod_usuario` tinyint(4) DEFAULT NULL,
  `fecha_creado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_noticias`
--

LOCK TABLES `blog_noticias` WRITE;
/*!40000 ALTER TABLE `blog_noticias` DISABLE KEYS */;
INSERT INTO `blog_noticias` VALUES (1,'Excelente Herramienta','<p style=\"text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sit amet ante enim. In euismod tincidunt turpis, fermentum malesuada purus maximus a. Aliquam sit amet ultricies diam. Aliquam erat volutpat. Nam efficitur massa porttitor, ultricies libero eget, congue ex. Sed elementum metus volutpat quam viverra hendrerit. Mauris luctus metus ut libero consectetur, non tincidunt ligula pretium. Nam ipsum lorem, sollicitudin et rhoncus nec, fringilla at sem.</p>','<p style=\"text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sit amet ante enim. In euismod tincidunt turpis, fermentum malesuada purus maximus a. Aliquam sit amet ultricies diam. Aliquam erat volutpat. Nam efficitur massa porttitor, ultricies libero eget, congue ex. Sed elementum metus volutpat quam viverra hendrerit. Mauris luctus metus ut libero consectetur, non tincidunt ligula pretium. Nam ipsum lorem, sollicitudin et rhoncus nec, fringilla at sem.</p>','post-img2.jpg',1,1,NULL,'2016-08-17 14:22:13'),(2,'Nueva Noticia','<p style=\"text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sit amet ante enim. In euismod tincidunt turpis, fermentum malesuada purus maximus a. Aliquam sit amet ultricies diam. Aliquam erat volutpat. Nam efficitur massa porttitor, ultricies libero eget, congue ex. Sed elementum metus volutpat quam viverra hendrerit. Mauris luctus metus ut libero consectetur, non tincidunt ligula pretium. Nam ipsum lorem, sollicitudin et rhoncus nec, fringilla at sem.</p>','<p style=\"text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sit amet ante enim. In euismod tincidunt turpis, fermentum malesuada purus maximus a. Aliquam sit amet ultricies diam. Aliquam erat volutpat. Nam efficitur massa porttitor, ultricies libero eget, congue ex. Sed elementum metus volutpat quam viverra hendrerit. Mauris luctus metus ut libero consectetur, non tincidunt ligula pretium. Nam ipsum lorem, sollicitudin et rhoncus nec, fringilla at sem.</p>\r\n<p style=\"text-align: justify;\">Vestibulum odio tortor, tempus eu mollis non, feugiat in tortor. Vestibulum dapibus non sapien eu facilisis. Aliquam pharetra eros in libero auctor, sed ornare elit molestie. Aenean tincidunt dictum pretium. Nunc ut arcu vel massa malesuada pellentesque at et ante. Praesent laoreet sit amet nisi at bibendum. Donec sodales urna felis, ut consectetur lectus pellentesque a. Integer eleifend sapien magna, sit amet imperdiet purus tempor convallis. Proin sodales eros tortor, eu interdum est egestas ac. Nulla quis lobortis odio, in tristique elit.</p>\r\n<p style=\"text-align: justify;\">Nulla in bibendum nunc. Praesent dignissim tellus bibendum, dictum felis vel, venenatis sem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur risus mauris, finibus ac iaculis sit amet, bibendum in massa. Sed neque odio, rhoncus ut cursus ut, rutrum et risus. Aliquam erat volutpat. Pellentesque faucibus mi porttitor tincidunt molestie. Nunc rutrum magna justo. In hac habitasse platea dictumst.</p>','post-img5.jpg',1,1,NULL,'2016-08-17 14:22:37');
/*!40000 ALTER TABLE `blog_noticias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cat_tipo_usuario`
--

DROP TABLE IF EXISTS `cat_tipo_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cat_tipo_usuario` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tipo` varchar(20) DEFAULT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT NULL,
  `fecha_creado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf16;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cat_tipo_usuario`
--

LOCK TABLES `cat_tipo_usuario` WRITE;
/*!40000 ALTER TABLE `cat_tipo_usuario` DISABLE KEYS */;
INSERT INTO `cat_tipo_usuario` VALUES (1,'registrado','Registrado',1,'2016-07-07 00:00:00'),(2,'suscrito','Suscrito',1,'2016-07-07 00:00:00'),(3,'administrador','Administrador',1,'2016-07-07 00:00:00'),(4,'doctor','Doctor(a)',0,'2016-07-11 00:00:00');
/*!40000 ALTER TABLE `cat_tipo_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cat_tipos_documento`
--

DROP TABLE IF EXISTS `cat_tipos_documento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cat_tipos_documento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(100) NOT NULL DEFAULT '0',
  `activo` tinyint(4) NOT NULL DEFAULT '0',
  `fecha_creado` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cat_tipos_documento`
--

LOCK TABLES `cat_tipos_documento` WRITE;
/*!40000 ALTER TABLE `cat_tipos_documento` DISABLE KEYS */;
INSERT INTO `cat_tipos_documento` VALUES (1,'Receta médica',1,'0000-00-00 00:00:00'),(2,'Estudio médico',1,'0000-00-00 00:00:00'),(3,'Radiografía',1,'0000-00-00 00:00:00'),(4,'Expediente',1,'0000-00-00 00:00:00'),(5,'Nota médica',1,'0000-00-00 00:00:00'),(6,'Certificado de salud',1,'0000-00-00 00:00:00'),(7,'Otro',1,'0000-00-00 00:00:00');
/*!40000 ALTER TABLE `cat_tipos_documento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cat_usuarios`
--

DROP TABLE IF EXISTS `cat_usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cat_usuarios` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `usuario` varchar(10) DEFAULT NULL,
  `correo` varchar(50) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `nombre` varchar(30) DEFAULT NULL,
  `ape_paterno` varchar(30) DEFAULT NULL,
  `ape_materno` varchar(30) DEFAULT NULL,
  `domicilio` varchar(100) DEFAULT NULL,
  `colonia` varchar(50) DEFAULT NULL,
  `ciudad` varchar(100) DEFAULT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT NULL,
  `codigo` varchar(50) DEFAULT NULL,
  `cod_tipo` tinyint(1) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `fecha_creado` datetime DEFAULT NULL,
  `fecha_actualizado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cat_usuarios`
--

LOCK TABLES `cat_usuarios` WRITE;
/*!40000 ALTER TABLE `cat_usuarios` DISABLE KEYS */;
INSERT INTO `cat_usuarios` VALUES (3,NULL,'admin@gmail.com','827ccb0eea8a706c4c34a16891f84e7b','Admin','Admin','Admin',NULL,NULL,NULL,'4737351400',1,NULL,3,'uncle_sam.jpg','2016-07-14 00:21:21',NULL),(4,NULL,'fabishodev@gmail.com','827ccb0eea8a706c4c34a16891f84e7b','Fabricio ','Magaña','Ruiz','','','','4731081408',1,NULL,2,NULL,'2016-07-29 00:50:21','2016-08-05 21:35:15'),(5,NULL,'jpdiaz.consulting@gmail.com','e10adc3949ba59abbe56e057f20f883e','Jorge','Diaz',' ','','','','4771355520',1,NULL,1,NULL,'2016-08-08 17:05:03','2016-08-12 15:26:29'),(6,NULL,'ljhands@hotmail.com','827ccb0eea8a706c4c34a16891f84e7b','Luis','Diaz','Hernandez',NULL,NULL,NULL,'4775678890',1,NULL,1,NULL,'2016-08-08 19:28:10',NULL),(7,NULL,'ljhands@hotmail.com.mx','e10adc3949ba59abbe56e057f20f883e','JORGE','MARIN','DIAZ',NULL,NULL,NULL,'4771123546',1,NULL,1,NULL,'2016-08-08 19:58:14',NULL);
/*!40000 ALTER TABLE `cat_usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `citas_pacientes`
--

DROP TABLE IF EXISTS `citas_pacientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `citas_pacientes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cod_usuario` int(11) DEFAULT NULL,
  `cod_doctor` int(11) DEFAULT NULL,
  `doctor` varchar(100) DEFAULT NULL,
  `cod_hospital` int(11) DEFAULT NULL,
  `hospital` varchar(100) DEFAULT NULL,
  `fecha_cita` date DEFAULT NULL,
  `hora_cita` varchar(5) DEFAULT NULL,
  `motivo_cita` mediumtext,
  `confirmacion` tinyint(4) DEFAULT NULL,
  `nota` mediumtext,
  `notificacion` tinyint(4) DEFAULT '0',
  `fecha_creado` datetime DEFAULT NULL,
  `fecha_actualizado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `citas_pacientes`
--

LOCK TABLES `citas_pacientes` WRITE;
/*!40000 ALTER TABLE `citas_pacientes` DISABLE KEYS */;
INSERT INTO `citas_pacientes` VALUES (1,4,NULL,'Hector Banda Salgado',NULL,'Los Angeles','2016-08-08','19:30','Revision General',1,'',2,'2016-08-06 15:25:01','2016-08-08 18:55:44'),(2,4,NULL,'Hector Banda Salgado',NULL,'Los Angeles','2016-08-09','8:00','Prueba recordatorio',0,NULL,2,'2016-08-08 15:38:16',NULL),(3,3,NULL,'JUAN BARRAGAN',NULL,'ARANDA DE LA PARRA','2016-08-12','17:30','REVISIÓN DE PROSTATA',0,NULL,0,'2016-08-08 16:29:52',NULL),(4,3,NULL,'QFB LUISA MARTINEZ',NULL,'MEDICA CAMPESTRE','2016-08-23','9:45','EXAMENES DE SANGRE',0,NULL,2,'2016-08-08 16:35:47',NULL),(5,3,NULL,'JUAN BARRAGAN',NULL,'MEDICA CAMPESTRE','2016-08-30','18:00','REVISIÓN EXAMENES',0,NULL,0,'2016-08-08 16:37:45',NULL),(6,5,NULL,'JUAN VAZQUEZ',NULL,'HOSPITAL LA LUZ','2016-08-09','17:00','REVISION DE RUTINA',0,NULL,0,'2016-08-08 17:07:13',NULL),(7,5,NULL,'ISAURO LLAMAS',NULL,'LOS ANGELES','2016-08-25','17:45','DOLOR ABDOMINAL',0,NULL,0,'2016-08-08 17:39:00',NULL),(8,6,NULL,'JULIAN JASSO',NULL,'MEDICA DEL VALLE','2016-08-12','20:30','DOLOR ABDOMINAL',0,NULL,0,'2016-08-08 19:33:17',NULL),(9,7,NULL,'julian alfonso',NULL,'MEDICA DEL VALLE','2016-08-18','17:30','REVISION DE RUTINA',0,NULL,0,'2016-08-08 20:01:55',NULL),(10,4,NULL,'Hector Salgado',NULL,'Los Angeles','2016-08-17','11:00','Revision general',0,NULL,0,'2016-08-16 22:57:36',NULL);
/*!40000 ALTER TABLE `citas_pacientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacto_mensajes`
--

DROP TABLE IF EXISTS `contacto_mensajes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacto_mensajes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `correo` varchar(255) DEFAULT NULL,
  `asunto` varchar(150) DEFAULT NULL,
  `mensaje` text,
  `fecha_creado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacto_mensajes`
--

LOCK TABLES `contacto_mensajes` WRITE;
/*!40000 ALTER TABLE `contacto_mensajes` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacto_mensajes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `datos_doctores`
--

DROP TABLE IF EXISTS `datos_doctores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datos_doctores` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cod_usuario` tinyint(4) DEFAULT NULL,
  `especialidad` varchar(50) DEFAULT NULL,
  `cedula_profesional` varchar(20) DEFAULT NULL,
  `fecha_creado` datetime DEFAULT NULL,
  `fecha_actualizado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datos_doctores`
--

LOCK TABLES `datos_doctores` WRITE;
/*!40000 ALTER TABLE `datos_doctores` DISABLE KEYS */;
/*!40000 ALTER TABLE `datos_doctores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documentos_pacientes`
--

DROP TABLE IF EXISTS `documentos_pacientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documentos_pacientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_usuario` tinyint(4) DEFAULT '0',
  `cod_doctor` tinyint(4) DEFAULT '0',
  `doctor` varchar(100) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT '0',
  `cod_tipo_documento` tinyint(4) DEFAULT NULL,
  `hospital` varchar(100) DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT '0',
  `nombre_archivo` varchar(255) DEFAULT NULL,
  `ruta` varchar(100) DEFAULT '0',
  `fecha_creado` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documentos_pacientes`
--

LOCK TABLES `documentos_pacientes` WRITE;
/*!40000 ALTER TABLE `documentos_pacientes` DISABLE KEYS */;
INSERT INTO `documentos_pacientes` VALUES (1,4,NULL,'Hector Banda','Receta',1,'Los Angeles','Receta','cotizacion-bastion.pdf','media/4/cotizacion-bastion.pdf','2016-08-06 15:32:35'),(2,5,NULL,'LENIN GALLARDO','Receta muelas',1,'MEDICA DEL VALLE','RECETA EXTRACION MUELAS','Receta.pdf','media/5/Receta.pdf','2016-08-08 17:20:02'),(3,3,NULL,'JULIAN JASSO','RECETA COLESTEROL',1,'MEDICA DEL VALLE','RECETA TRATAMIENTO COLESTEROL','Receta.pdf','media/3/Receta.pdf','2016-08-08 17:43:44'),(4,5,NULL,'JULIAN JASSO','RADIOGRAFIA',3,'EL CHOPO','ESGUINSE','Radiografia.pdf','media/5/Radiografia.pdf','2016-08-08 17:45:16'),(5,7,NULL,'laboratorios chopo','radiografia ',3,'MEDICA DEL VALLE','Radiografia mano derecha','Radiografia_mano.pdf','media/7/Radiografia_mano.pdf','2016-08-08 20:06:14');
/*!40000 ALTER TABLE `documentos_pacientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `programacion_citas`
--

DROP TABLE IF EXISTS `programacion_citas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `programacion_citas` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(150) DEFAULT NULL,
  `body` mediumtext NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `class` varchar(45) NOT NULL DEFAULT 'info',
  `start` varchar(15) NOT NULL,
  `end` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `programacion_citas`
--

LOCK TABLES `programacion_citas` WRITE;
/*!40000 ALTER TABLE `programacion_citas` DISABLE KEYS */;
INSERT INTO `programacion_citas` VALUES (1,'<p>Cita con Dr(a): Hector Banda Salgado</p><p>Motivo: Revision General</p><p>Hora: 19:30</p>','Revision General',NULL,'event-warning','1470702600000','1470702600000'),(2,'<p>Cita con Dr(a): Hector Banda Salgado</p><p>Motivo: Prueba recordatorio</p><p>En: Los Angeles</p><p>Hora: 8:00</p>','Prueba recordatorio',NULL,'event-warning','1470747600000','1470747600000'),(3,'<p>Cita con Dr(a): JUAN BARRAGAN</p><p>Motivo: REVISIÓN DE PROSTATA</p><p>En: ARANDA DE LA PARRA</p><p>Hora: 17:30</p>','REVISIÓN DE PROSTATA',NULL,'event-warning','1471041000000','1471041000000'),(4,'<p>Cita con Dr(a): QFB LUISA MARTINEZ</p><p>Motivo: EXAMENES DE SANGRE</p><p>En: MEDICA CAMPESTRE</p><p>Hora: 9:45</p>','EXAMENES DE SANGRE',NULL,'event-warning','1471963500000','1471963500000'),(5,'<p>Cita con Dr(a): JUAN BARRAGAN</p><p>Motivo: REVISIÓN EXAMENES</p><p>En: MEDICA CAMPESTRE</p><p>Hora: 18:00</p>','REVISIÓN EXAMENES',NULL,'event-warning','1472598000000','1472598000000'),(6,'<p>Cita con Dr(a): JUAN VAZQUEZ</p><p>Motivo: REVISION DE RUTINA</p><p>En: HOSPITAL LA LUZ</p><p>Hora: 17:00</p>','REVISION DE RUTINA',NULL,'event-warning','1470780000000','1470780000000'),(7,'<p>Cita con Dr(a): ISAURO LLAMAS</p><p>Motivo: DOLOR ABDOMINAL</p><p>En: LOS ANGELES</p><p>Hora: 17:45</p>','DOLOR ABDOMINAL',NULL,'event-warning','1472165100000','1472165100000'),(8,'<p>Cita con Dr(a): JULIAN JASSO</p><p>Motivo: DOLOR ABDOMINAL</p><p>En: MEDICA DEL VALLE</p><p>Hora: 20:30</p>','DOLOR ABDOMINAL',NULL,'event-warning','1471051800000','1471051800000'),(9,'<p>Cita con Dr(a): julian alfonso</p><p>Motivo: REVISION DE RUTINA</p><p>En: MEDICA DEL VALLE</p><p>Hora: 17:30</p>','REVISION DE RUTINA',NULL,'event-warning','1471559400000','1471559400000'),(10,'<p>Cita con Dr(a): Hector Salgado</p><p>Motivo: Revision general</p><p>En: Los Angeles</p><p>Hora: 11:00</p>','Revision general',NULL,'event-warning','1471449600000','1471449600000');
/*!40000 ALTER TABLE `programacion_citas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `programacion_tratamientos`
--

DROP TABLE IF EXISTS `programacion_tratamientos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `programacion_tratamientos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(150) DEFAULT NULL,
  `body` mediumtext NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `class` varchar(45) NOT NULL DEFAULT 'info',
  `start` varchar(15) NOT NULL,
  `end` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `programacion_tratamientos`
--

LOCK TABLES `programacion_tratamientos` WRITE;
/*!40000 ALTER TABLE `programacion_tratamientos` DISABLE KEYS */;
/*!40000 ALTER TABLE `programacion_tratamientos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tratamientos_pacientes`
--

DROP TABLE IF EXISTS `tratamientos_pacientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tratamientos_pacientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_usuario` int(11) DEFAULT '0',
  `fecha_inicio` datetime DEFAULT '0000-00-00 00:00:00',
  `fecha_fin` datetime DEFAULT '0000-00-00 00:00:00',
  `hora` varchar(10) DEFAULT '0',
  `medicamento` varchar(255) DEFAULT '0',
  `observaciones` mediumtext,
  `fecha_creado` datetime DEFAULT '0000-00-00 00:00:00',
  `title` varchar(150) DEFAULT NULL,
  `body` mediumtext,
  `url` varchar(150) DEFAULT NULL,
  `class` varchar(45) DEFAULT NULL,
  `start` varchar(15) DEFAULT NULL,
  `end` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tratamientos_pacientes`
--

LOCK TABLES `tratamientos_pacientes` WRITE;
/*!40000 ALTER TABLE `tratamientos_pacientes` DISABLE KEYS */;
INSERT INTO `tratamientos_pacientes` VALUES (1,4,'2016-08-08 00:00:00','2016-08-12 00:00:00','6:00','Aspirina','Tomar una pastilla cada 8 hrs','2016-08-06 15:29:34','<p>Tratamiento de: Aspirina</p><p>Observaciones: Tomar una pastilla cada 8 hrs</p><p>Hora: 6:00</p>','<p>Tratamiento de: Aspirina</p><p>Observaciones: Tomar una pastilla cada 8 hrs</p><p>Hora: 6:00</p>',NULL,'event-special','1470654000000','1470999600000'),(2,3,'2016-08-16 00:00:00','2016-09-16 00:00:00','10:00','VITAMINAS','TOMAR DESPUÉS DEL DESAYUNO','2016-08-08 16:39:32','<p>Tratamiento de: VITAMINAS</p><p>Observaciones: TOMAR DESPUÉS DEL DESAYUNO</p><p>Hora: 10:00</p>','<p>Tratamiento de: VITAMINAS</p><p>Observaciones: TOMAR DESPUÉS DEL DESAYUNO</p><p>Hora: 10:00</p>',NULL,'event-special','1471359600000','1474038000000'),(3,3,'2016-08-13 00:00:00','2016-08-31 00:00:00','14:00','FLUVINESE','TOMAR ANTES DE LA COMIDAD','2016-08-08 16:41:04','<p>Tratamiento de: FLUVINESE</p><p>Observaciones: TOMAR ANTES DE LA COMIDAD</p><p>Hora: 14:00</p>','<p>Tratamiento de: FLUVINESE</p><p>Observaciones: TOMAR ANTES DE LA COMIDAD</p><p>Hora: 14:00</p>',NULL,'event-special','1471114800000','1472670000000'),(4,6,'2016-08-10 00:00:00','2016-09-10 00:00:00','6:00','PENICILINA','TOMAR CAPSULA CADA 8 HORAS','2016-08-08 19:35:15','<p>Tratamiento de: PENICILINA</p><p>Observaciones: TOMAR CAPSULA CADA 8 HORAS</p><p>Hora: 6:00</p>','<p>Tratamiento de: PENICILINA</p><p>Observaciones: TOMAR CAPSULA CADA 8 HORAS</p><p>Hora: 6:00</p>',NULL,'event-special','1470826800000','1473505200000'),(5,4,'2016-08-22 00:00:00','2016-08-26 00:00:00','15:00','Aspirina','Tomar una pildora','2016-08-17 15:07:18','<p>Tratamiento de: Aspirina</p><p>Observaciones: Tomar una pildora</p><p>Hora: 15:00</p>','<p>Tratamiento de: Aspirina</p><p>Observaciones: Tomar una pildora</p><p>Hora: 15:00</p>',NULL,'event-special','1471896000000','1472241600000');
/*!40000 ALTER TABLE `tratamientos_pacientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `vw_cita_paciente`
--

DROP TABLE IF EXISTS `vw_cita_paciente`;
/*!50001 DROP VIEW IF EXISTS `vw_cita_paciente`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_cita_paciente` AS SELECT 
 1 AS `id_cita`,
 1 AS `cod_usuario`,
 1 AS `cod_tipo`,
 1 AS `doctor`,
 1 AS `hospital`,
 1 AS `nombre`,
 1 AS `ape_paterno`,
 1 AS `ape_materno`,
 1 AS `correo`,
 1 AS `telefono`,
 1 AS `cod_doctor`,
 1 AS `nombre_doc`,
 1 AS `ape_paterno_doc`,
 1 AS `ape_materno_doc`,
 1 AS `correo_doc`,
 1 AS `telefono_doc`,
 1 AS `especialidad`,
 1 AS `fecha_cita`,
 1 AS `hora_cita`,
 1 AS `motivo_cita`,
 1 AS `confirmacion`,
 1 AS `nota`,
 1 AS `fecha_creado_cita`,
 1 AS `notificacion`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_citas_programadas`
--

DROP TABLE IF EXISTS `vw_citas_programadas`;
/*!50001 DROP VIEW IF EXISTS `vw_citas_programadas`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_citas_programadas` AS SELECT 
 1 AS `id_cita`,
 1 AS `cod_usuario`,
 1 AS `cod_tipo`,
 1 AS `cod_doctor`,
 1 AS `fecha_cita`,
 1 AS `hora_cita`,
 1 AS `motivo_cita`,
 1 AS `confirmacion`,
 1 AS `nota`,
 1 AS `fecha_creado`,
 1 AS `fecha_actualizado`,
 1 AS `nombre`,
 1 AS `ape_paterno`,
 1 AS `ape_materno`,
 1 AS `title`,
 1 AS `nombre_doc`,
 1 AS `ape_paterno_doc`,
 1 AS `ape_materno_doc`,
 1 AS `nombre_completo_paciente`,
 1 AS `nombre_completo_doc`,
 1 AS `title1`,
 1 AS `body`,
 1 AS `url`,
 1 AS `clase`,
 1 AS `start`,
 1 AS `end`,
 1 AS `class`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_comentarios`
--

DROP TABLE IF EXISTS `vw_comentarios`;
/*!50001 DROP VIEW IF EXISTS `vw_comentarios`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_comentarios` AS SELECT 
 1 AS `id`,
 1 AS `cod_usuario`,
 1 AS `cod_noticia`,
 1 AS `asunto`,
 1 AS `comentario`,
 1 AS `publicado`,
 1 AS `fecha_comentario`,
 1 AS `nombre`,
 1 AS `ape_paterno`,
 1 AS `ape_materno`,
 1 AS `foto`,
 1 AS `titulo_noticia`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_doctor`
--

DROP TABLE IF EXISTS `vw_doctor`;
/*!50001 DROP VIEW IF EXISTS `vw_doctor`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_doctor` AS SELECT 
 1 AS `id`,
 1 AS `usuario`,
 1 AS `correo`,
 1 AS `password`,
 1 AS `nombre`,
 1 AS `ape_paterno`,
 1 AS `ape_materno`,
 1 AS `domicilio`,
 1 AS `colonia`,
 1 AS `ciudad`,
 1 AS `telefono`,
 1 AS `activo`,
 1 AS `cod_tipo`,
 1 AS `fecha_creado`,
 1 AS `fecha_actualizado`,
 1 AS `id_tipo_usuario`,
 1 AS `tipo`,
 1 AS `descripcion`,
 1 AS `especialidad`,
 1 AS `cedula_profesional`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_documentos`
--

DROP TABLE IF EXISTS `vw_documentos`;
/*!50001 DROP VIEW IF EXISTS `vw_documentos`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_documentos` AS SELECT 
 1 AS `id`,
 1 AS `cod_usuario`,
 1 AS `cod_tipo`,
 1 AS `cod_doctor`,
 1 AS `doctor`,
 1 AS `nombre_documento`,
 1 AS `descripcion`,
 1 AS `ruta`,
 1 AS `nombre_archivo`,
 1 AS `fecha_creado`,
 1 AS `nombre`,
 1 AS `ape_paterno`,
 1 AS `ape_materno`,
 1 AS `nombre_paciente`,
 1 AS `ape_paterno_paciente`,
 1 AS `ape_materno_paciente`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_noticia`
--

DROP TABLE IF EXISTS `vw_noticia`;
/*!50001 DROP VIEW IF EXISTS `vw_noticia`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_noticia` AS SELECT 
 1 AS `id`,
 1 AS `titulo_noticia`,
 1 AS `noticia_corta`,
 1 AS `noticia_completa`,
 1 AS `caratula_noticia`,
 1 AS `publicada`,
 1 AS `cod_categoria`,
 1 AS `fecha_creado`,
 1 AS `categoria`,
 1 AS `descripcion`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_tratamientos`
--

DROP TABLE IF EXISTS `vw_tratamientos`;
/*!50001 DROP VIEW IF EXISTS `vw_tratamientos`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_tratamientos` AS SELECT 
 1 AS `id`,
 1 AS `cod_usuario`,
 1 AS `cod_tipo`,
 1 AS `fecha_inicio`,
 1 AS `fecha_fin`,
 1 AS `hora`,
 1 AS `medicamento`,
 1 AS `observaciones`,
 1 AS `fecha_creado`,
 1 AS `title`,
 1 AS `class`,
 1 AS `nombre`,
 1 AS `ape_paterno`,
 1 AS `ape_materno`,
 1 AS `descripcion`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_usuario`
--

DROP TABLE IF EXISTS `vw_usuario`;
/*!50001 DROP VIEW IF EXISTS `vw_usuario`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_usuario` AS SELECT 
 1 AS `id`,
 1 AS `usuario`,
 1 AS `correo`,
 1 AS `password`,
 1 AS `nombre`,
 1 AS `ape_paterno`,
 1 AS `ape_materno`,
 1 AS `domicilio`,
 1 AS `colonia`,
 1 AS `ciudad`,
 1 AS `telefono`,
 1 AS `activo`,
 1 AS `cod_tipo`,
 1 AS `fecha_creado`,
 1 AS `fecha_actualizado`,
 1 AS `id_tipo_usuario`,
 1 AS `tipo`,
 1 AS `descripcion`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `vw_cita_paciente`
--

/*!50001 DROP VIEW IF EXISTS `vw_cita_paciente`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_cita_paciente` AS (select `a`.`id` AS `id_cita`,`a`.`cod_usuario` AS `cod_usuario`,`b`.`cod_tipo` AS `cod_tipo`,`a`.`doctor` AS `doctor`,`a`.`hospital` AS `hospital`,`b`.`nombre` AS `nombre`,`b`.`ape_paterno` AS `ape_paterno`,`b`.`ape_materno` AS `ape_materno`,`b`.`correo` AS `correo`,`b`.`telefono` AS `telefono`,`a`.`cod_doctor` AS `cod_doctor`,`c`.`nombre` AS `nombre_doc`,`c`.`ape_paterno` AS `ape_paterno_doc`,`c`.`ape_materno` AS `ape_materno_doc`,`c`.`correo` AS `correo_doc`,`c`.`telefono` AS `telefono_doc`,`c`.`especialidad` AS `especialidad`,`a`.`fecha_cita` AS `fecha_cita`,`a`.`hora_cita` AS `hora_cita`,`a`.`motivo_cita` AS `motivo_cita`,`a`.`confirmacion` AS `confirmacion`,`a`.`nota` AS `nota`,`a`.`fecha_creado` AS `fecha_creado_cita`,`a`.`notificacion` AS `notificacion` from ((`citas_pacientes` `a` left join `vw_usuario` `b` on((`a`.`cod_usuario` = `b`.`id`))) left join `vw_doctor` `c` on((`a`.`cod_doctor` = `c`.`id`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_citas_programadas`
--

/*!50001 DROP VIEW IF EXISTS `vw_citas_programadas`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_citas_programadas` AS (select `a`.`id` AS `id_cita`,`a`.`cod_usuario` AS `cod_usuario`,`c`.`cod_tipo` AS `cod_tipo`,`a`.`cod_doctor` AS `cod_doctor`,`a`.`fecha_cita` AS `fecha_cita`,`a`.`hora_cita` AS `hora_cita`,`a`.`motivo_cita` AS `motivo_cita`,`a`.`confirmacion` AS `confirmacion`,`a`.`nota` AS `nota`,`a`.`fecha_creado` AS `fecha_creado`,`a`.`fecha_actualizado` AS `fecha_actualizado`,`c`.`nombre` AS `nombre`,`c`.`ape_paterno` AS `ape_paterno`,`c`.`ape_materno` AS `ape_materno`,`b`.`title` AS `title`,`d`.`nombre` AS `nombre_doc`,`d`.`ape_paterno` AS `ape_paterno_doc`,`d`.`ape_materno` AS `ape_materno_doc`,concat(`c`.`nombre`,' ',`c`.`ape_paterno`,' ',`c`.`ape_materno`) AS `nombre_completo_paciente`,concat(`d`.`nombre`,' ',`d`.`ape_paterno`,' ',`d`.`ape_materno`) AS `nombre_completo_doc`,concat(`b`.`title`,' ',concat(`d`.`nombre`,' ',`d`.`ape_paterno`,' ',`d`.`ape_materno`,' Motivo: ',`a`.`motivo_cita`,' Hora: ',`a`.`hora_cita`,' Nota: ',`a`.`nota`)) AS `title1`,concat(' Motivo: ',`a`.`motivo_cita`,' Fecha: ',`a`.`fecha_cita`,' Hora: ',`a`.`hora_cita`,' Nota: ',`a`.`nota`) AS `body`,`b`.`url` AS `url`,`b`.`class` AS `clase`,`b`.`start` AS `start`,`b`.`end` AS `end`,(case `a`.`confirmacion` when 1 then 'event-success' when 0 then 'event-warning' when 2 then 'event-important' end) AS `class` from (((`citas_pacientes` `a` left join `programacion_citas` `b` on((`a`.`id` = `b`.`id`))) left join `cat_usuarios` `c` on((`a`.`cod_usuario` = `c`.`id`))) left join `cat_usuarios` `d` on((`a`.`cod_doctor` = `d`.`id`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_comentarios`
--

/*!50001 DROP VIEW IF EXISTS `vw_comentarios`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_comentarios` AS (select `a`.`id` AS `id`,`a`.`cod_usuario` AS `cod_usuario`,`a`.`cod_noticia` AS `cod_noticia`,`a`.`asunto` AS `asunto`,`a`.`comentario` AS `comentario`,`a`.`publicado` AS `publicado`,`a`.`fecha_creado` AS `fecha_comentario`,`b`.`nombre` AS `nombre`,`b`.`ape_paterno` AS `ape_paterno`,`b`.`ape_materno` AS `ape_materno`,`b`.`foto` AS `foto`,`c`.`titulo_noticia` AS `titulo_noticia` from ((`blog_comentarios` `a` left join `cat_usuarios` `b` on((`a`.`cod_usuario` = `b`.`id`))) left join `blog_noticias` `c` on((`a`.`cod_noticia` = `c`.`id`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_doctor`
--

/*!50001 DROP VIEW IF EXISTS `vw_doctor`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_doctor` AS (select `a`.`id` AS `id`,`a`.`usuario` AS `usuario`,`a`.`correo` AS `correo`,`a`.`password` AS `password`,`a`.`nombre` AS `nombre`,`a`.`ape_paterno` AS `ape_paterno`,`a`.`ape_materno` AS `ape_materno`,`a`.`domicilio` AS `domicilio`,`a`.`colonia` AS `colonia`,`a`.`ciudad` AS `ciudad`,`a`.`telefono` AS `telefono`,`a`.`activo` AS `activo`,`a`.`cod_tipo` AS `cod_tipo`,`a`.`fecha_creado` AS `fecha_creado`,`a`.`fecha_actualizado` AS `fecha_actualizado`,`b`.`id` AS `id_tipo_usuario`,`b`.`tipo` AS `tipo`,`b`.`descripcion` AS `descripcion`,`c`.`especialidad` AS `especialidad`,`c`.`cedula_profesional` AS `cedula_profesional` from ((`cat_usuarios` `a` left join `cat_tipo_usuario` `b` on((`a`.`cod_tipo` = `b`.`id`))) left join `datos_doctores` `c` on((`a`.`id` = `c`.`cod_usuario`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_documentos`
--

/*!50001 DROP VIEW IF EXISTS `vw_documentos`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_documentos` AS (select `a`.`id` AS `id`,`a`.`cod_usuario` AS `cod_usuario`,`c`.`cod_tipo` AS `cod_tipo`,`a`.`cod_doctor` AS `cod_doctor`,`a`.`doctor` AS `doctor`,`a`.`nombre` AS `nombre_documento`,`a`.`descripcion` AS `descripcion`,`a`.`ruta` AS `ruta`,`a`.`nombre_archivo` AS `nombre_archivo`,`a`.`fecha_creado` AS `fecha_creado`,`b`.`nombre` AS `nombre`,`b`.`ape_paterno` AS `ape_paterno`,`b`.`ape_materno` AS `ape_materno`,`c`.`nombre` AS `nombre_paciente`,`c`.`ape_paterno` AS `ape_paterno_paciente`,`c`.`ape_materno` AS `ape_materno_paciente` from ((`documentos_pacientes` `a` left join `vw_doctor` `b` on((`a`.`cod_doctor` = `b`.`id`))) left join `vw_usuario` `c` on((`a`.`cod_usuario` = `c`.`id`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_noticia`
--

/*!50001 DROP VIEW IF EXISTS `vw_noticia`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_noticia` AS (select `a`.`id` AS `id`,`a`.`titulo_noticia` AS `titulo_noticia`,`a`.`noticia_corta` AS `noticia_corta`,`a`.`noticia_completa` AS `noticia_completa`,`a`.`caratula_noticia` AS `caratula_noticia`,`a`.`publicada` AS `publicada`,`a`.`cod_categoria` AS `cod_categoria`,`a`.`fecha_creado` AS `fecha_creado`,`b`.`categoria` AS `categoria`,`b`.`descripcion` AS `descripcion` from (`blog_noticias` `a` left join `blog_categorias` `b` on((`a`.`cod_categoria` = `b`.`id`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_tratamientos`
--

/*!50001 DROP VIEW IF EXISTS `vw_tratamientos`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_tratamientos` AS (select `a`.`id` AS `id`,`a`.`cod_usuario` AS `cod_usuario`,`b`.`cod_tipo` AS `cod_tipo`,`a`.`fecha_inicio` AS `fecha_inicio`,`a`.`fecha_fin` AS `fecha_fin`,`a`.`hora` AS `hora`,`a`.`medicamento` AS `medicamento`,`a`.`observaciones` AS `observaciones`,`a`.`fecha_creado` AS `fecha_creado`,`a`.`title` AS `title`,`a`.`class` AS `class`,`b`.`nombre` AS `nombre`,`b`.`ape_paterno` AS `ape_paterno`,`b`.`ape_materno` AS `ape_materno`,`b`.`descripcion` AS `descripcion` from (`tratamientos_pacientes` `a` left join `vw_usuario` `b` on((`a`.`cod_usuario` = `b`.`id`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_usuario`
--

/*!50001 DROP VIEW IF EXISTS `vw_usuario`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_usuario` AS (select `a`.`id` AS `id`,`a`.`usuario` AS `usuario`,`a`.`correo` AS `correo`,`a`.`password` AS `password`,`a`.`nombre` AS `nombre`,`a`.`ape_paterno` AS `ape_paterno`,`a`.`ape_materno` AS `ape_materno`,`a`.`domicilio` AS `domicilio`,`a`.`colonia` AS `colonia`,`a`.`ciudad` AS `ciudad`,`a`.`telefono` AS `telefono`,`a`.`activo` AS `activo`,`a`.`cod_tipo` AS `cod_tipo`,`a`.`fecha_creado` AS `fecha_creado`,`a`.`fecha_actualizado` AS `fecha_actualizado`,`b`.`id` AS `id_tipo_usuario`,`b`.`tipo` AS `tipo`,`b`.`descripcion` AS `descripcion` from (`cat_usuarios` `a` left join `cat_tipo_usuario` `b` on((`a`.`cod_tipo` = `b`.`id`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-17 15:23:14
