<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}
class Blog_model extends CI_Model {

  function __construct() {
    parent::__construct();
    $this->db = $this->load->database('default', TRUE);
    }

    public function actualizarComentario($serv = array(), $id)
    {
        $this->db->trans_begin();
        $this->db->where('id', $id);
        $this->db->update('blog_comentarios', $serv);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
          }
    }

    function obtenerComentario($id){
        $where = "id = ".$id."";
        $this->db->select('*');
        if($where != NULL){
            $this->db->where($where,NULL,FALSE);
        }
        $query = $this->db->get('blog_comentarios');
        return $query->row();
      }

    public function guardarComentario($serv = array())
    {
        $this->db->trans_begin();
        $this->db->insert('blog_comentarios', $serv);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return TRUE;
          }
        }

        function obtenerListaComentarios(){
          $where = "";
          $this->db->select('*');
          if($where != NULL){
              $this->db->where($where,NULL,FALSE);
          }
          $query = $this->db->get('vw_comentarios');
          return $query->result();
        }


    function obtenerComentarios($id){
      $where = "cod_noticia=".$id." AND publicado = 1";
      $this->db->select('*');
      if($where != NULL){
          $this->db->where($where,NULL,FALSE);
      }
      $query = $this->db->get('vw_comentarios');
      return $query->result();
    }

    function obtenerCategoria($id){
  			$where = "id = ".$id."";
  			$this->db->select('*');
  			if($where != NULL){
  					$this->db->where($where,NULL,FALSE);
  			}
  			$query = $this->db->get('blog_categorias');
  			return $query->row();
  		}

    public function actualizarCategoria($serv = array(), $id)
    {
        $this->db->trans_begin();
        $this->db->where('id', $id);
        $this->db->update('blog_categorias', $serv);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
          }
    }

    public function registrarCategoria($serv = array())
    {
        $this->db->trans_begin();
        $this->db->insert('blog_categorias', $serv);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return TRUE;
          }
        }

        function obtenerListaCategoriasActivas(){
          $where = "activo=1";
          $this->db->select('*');
          if($where != NULL){
              $this->db->where($where,NULL,FALSE);
          }
          $query = $this->db->get('blog_categorias');
          return $query->result();
        }

    function obtenerListaCategorias(){
      $where = "";
      $this->db->select('*');
      if($where != NULL){
          $this->db->where($where,NULL,FALSE);
      }
      $query = $this->db->get('blog_categorias');
      return $query->result();
    }

    public function actualizarNoticia($serv = array(), $id)
    {
        $this->db->trans_begin();
        $this->db->where('id', $id);
        $this->db->update('blog_noticias', $serv);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
          }
    }

    function obtenerNoticia($id){
  			$where = "id = ".$id."";
  			$this->db->select('*');
  			if($where != NULL){
  					$this->db->where($where,NULL,FALSE);
  			}
  			$query = $this->db->get('blog_noticias');
  			return $query->row();
  		}

    function obtenerNoticiasRecientes(){
      $where = "publicada = 1";
      $this->db->limit(3);
      $this->db->select('*');
      if($where != NULL){
          $this->db->where($where,NULL,FALSE);
      }
      $this->db->order_by("id","desc");
      $query = $this->db->get('vw_noticia');
      return $query->result();
    }

    function obtenerEtiquetasNoticias(){
      $where = "publicada = 1";
      $this->db->distinct();
      $this->db->select('cod_categoria, categoria');
      if($where != NULL){
          $this->db->where($where,NULL,FALSE);
      }
      $query = $this->db->get('vw_noticia');
      return $query->result();
    }

    function obtenerNoticiasBlog($cod_categoria){
      if ($cod_categoria !== null) {
          $where = "publicada = 1 AND cod_categoria = '".$cod_categoria."'";
      }else {
          $where = "publicada = 1";
      }
      $this->db->select('*');
      if($where != NULL){
          $this->db->where($where,NULL,FALSE);
      }
      $query = $this->db->get('blog_noticias');
      return $query->result();
    }

    public function guardarNoticia($serv = array())
    {
        $this->db->trans_begin();
        $this->db->insert('blog_noticias', $serv);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return TRUE;
          }
        }

        function obtenerNoticias(){
          $where = "publicada = 1";
          $this->db->select('*');
          if($where != NULL){
              $this->db->where($where,NULL,FALSE);
          }
          $query = $this->db->get('vw_noticia');
          return $query->result();
        }

    function obtenerListaNoticias(){
      $where = "";
      $this->db->select('*');
      if($where != NULL){
          $this->db->where($where,NULL,FALSE);
      }
      $query = $this->db->get('vw_noticia');
      return $query->result();
    }

}
