<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}
class Usuario_model extends CI_Model {

  function __construct() {
    parent::__construct();
    $this->db = $this->load->database('default', TRUE);
    }

    function obtenerTipoUsuarios(){
			$where = "activo = 1";
			$this->db->select('*');
			if($where != NULL){
					$this->db->where($where,NULL,FALSE);
			}
			$query = $this->db->get('cat_tipo_usuario');
			return $query->result();
		}


    public function registrarMensaje($serv = array()){
        $this->db->trans_begin();
        $this->db->insert('contacto_mensajes', $serv);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
      }

    function obtenerCalendarioCitas(){
			$where = "";
      $this->db->select('*');
			if($where != NULL){
					$this->db->where($where,NULL,FALSE);
			}
			$query = $this->db->get('vw_citas_programadas');
			return $query->result();
		}

    function obtenerUsuarioAdmin($email, $pass){
      $password = do_hash($pass, 'md5');
      $where = "correo ='".$email."' AND password = '".$password."'";
      $this->db->select('*');
      $this->db->from('cat_usuarios');
      if ($where != NULL) {
        $this->db->where($where, NULL, FALSE);
      }
      $query = $this->db->get();
      return $query->row();
    }

    function obtenerTratamientosUsuarios($id){
			$where = "cod_usuario = ".$id."";
			$this->db->select('*');
			if($where != NULL){
					$this->db->where($where,NULL,FALSE);
			}
			$query = $this->db->get('tratamientos_pacientes');
			return $query->result();
		}

    public function registrarTratamientoUsuario($serv = array()){
        $this->db->trans_begin();
        $this->db->insert('tratamientos_pacientes', $serv);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
      }

    function obtenerCitasUsuarios($id){
			$where = "cod_usuario = ".$id."";
			$this->db->select('*');
			if($where != NULL){
					$this->db->where($where,NULL,FALSE);
			}
			$query = $this->db->get('vw_citas_programadas');
			return $query->result();
		}


    public function registrarProgramacionCitaUsuario($serv = array()){
        $this->db->trans_begin();
        $this->db->insert('programacion_citas', $serv);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
      }

    function obtenerListaDocumentos($id){
			$where = "cod_usuario=".$id."";
			$this->db->select('*');
			if($where != NULL){
					$this->db->where($where,NULL,FALSE);
			}
			$query = $this->db->get('vw_documentos');
			return $query->result();
		}

    public function guardarDocumento($serv = array()){
        $this->db->trans_begin();
        $this->db->insert('documentos_pacientes', $serv);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
      }

    public function registrarCitaUsuario($serv = array()){
        $this->db->trans_begin();
        $this->db->insert('citas_pacientes', $serv);
          $id = $this->db->insert_id();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return (isset($id)) ? $id : FALSE;
        }
      }

    function actualizarUsuario($serv = array(),$id){
			$this->db->trans_begin();
      $this->db->where('id', $id);
      $this->db->update('cat_usuarios', $serv);
			if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return FALSE;
			 } else {
			  $this->db->trans_commit();
			   return TRUE;
		  }
	 }

    function obtenerDetalleUsuario($id){
      $where = "id = ".$id."";
      $this->db->select('*');
      if($where != NULL){
          $this->db->where($where,NULL,FALSE);
      }
      $query = $this->db->get('cat_usuarios');
      return $query->row();
    }

    function obtenerListaUsuarios(){
			$where = "";
			$this->db->select('*');
			if($where != NULL){
					$this->db->where($where,NULL,FALSE);
			}
			$query = $this->db->get('vw_usuario');
			return $query->result();
		}

    function obtenerUsuario($email, $pass){
      $password = do_hash($pass, 'md5');
      $where = "correo ='".$email."' AND password = '".$password."'";
      $this->db->select('*');
      $this->db->from('cat_usuarios');
      if ($where != NULL) {
        $this->db->where($where, NULL, FALSE);
      }
      $query = $this->db->get();
      return $query->row();
    }

}
