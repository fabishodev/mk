<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}
class Citas_model extends CI_Model {

  function __construct() {
    parent::__construct();
    $this->db = $this->load->database('default', TRUE);
    }

    function eliminarCitaPacienteProgramada($id) {
      //die(print($id));
        $this->db->trans_begin();
      $this->db->where('id', $id);
      $this->db->delete('programacion_citas');
        if ($this->db->trans_status() === FALSE) {
        $this->db->trans_rollback();
        return FALSE;
      } else {
        $this->db->trans_commit();
        return TRUE;
      }
    }

    function eliminarCitaPaciente($id) {
			//die(print($id));
				$this->db->trans_begin();
			$this->db->where('id', $id);
			$this->db->delete('citas_pacientes');
				if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				return FALSE;
			} else {
				$this->db->trans_commit();
				return TRUE;
			}
		}


    function obtenerProximasCitasRecordatorios(){
      $where = "fecha_cita >= CURDATE()";
      $this->db->select('id_cita, cod_usuario,nombre, ape_paterno, ape_materno, correo,doctor, hospital, fecha_cita, hora_cita,CONCAT( fecha_cita,"'.' '.'", hora_cita,  "'.':00'.'" ) AS fecha_hora_cita, motivo_cita, TIMESTAMPDIFF( HOUR , NOW( ) , CONCAT( fecha_cita,"'.' '.'", hora_cita,  "'.':00'.'" ) ) AS horas_restantes,notificacion');
      if($where != NULL){
          $this->db->where($where,NULL,FALSE);
      }
      $query = $this->db->get('vw_cita_paciente');
      return $query->result();
    }


    function obtenerProximasCitas($id){
      $where = "cod_usuario = ".$id." AND fecha_cita >= CURDATE()";
      $this->db->limit(5);
      $this->db->select('*');
      if($where != NULL){
          $this->db->where($where,NULL,FALSE);
      }
      $query = $this->db->get('vw_cita_paciente');
      return $query->result();
    }

    function obtenerNumCitasUsuario($id){
      $where = "cod_usuario = ".$id."";
      $this->db->select('COUNT(id_cita) AS num_citas , cod_tipo');
      if($where != NULL){
          $this->db->where($where,NULL,FALSE);
      }
      $query = $this->db->get('vw_cita_paciente');
      return $query->row();
    }

    function obtenerProgramacionCita($id){
      $where = "id = ".$id."";
      $this->db->select('*');
      if($where != NULL){
          $this->db->where($where,NULL,FALSE);
      }
      $query = $this->db->get('programacion_citas');
      return $query->row();
    }

    function actualizarProgramacionCita($serv = array(),$id){
      $this->db->trans_begin();
      $this->db->where('id', $id);
      $this->db->update('programacion_citas', $serv);
      if ($this->db->trans_status() === FALSE) {
      $this->db->trans_rollback();
      return FALSE;
       } else {
        $this->db->trans_commit();
         return TRUE;
      }
   }

    function actualizarCita($serv = array(),$id){
      $this->db->trans_begin();
      $this->db->where('id', $id);
      $this->db->update('citas_pacientes', $serv);
      if ($this->db->trans_status() === FALSE) {
      $this->db->trans_rollback();
      return FALSE;
       } else {
        $this->db->trans_commit();
         return TRUE;
      }
   }

    function obtenerDetalleCita($id){
      $where = "id_cita = ".$id."";
      $this->db->select('*');
      if($where != NULL){
          $this->db->where($where,NULL,FALSE);
      }
      $query = $this->db->get('vw_cita_paciente');
      return $query->row();
    }

    function obtenerListaCitas(){
			$where = "";
			$this->db->select('*');
			if($where != NULL){
					$this->db->where($where,NULL,FALSE);
			}
			$query = $this->db->get('vw_cita_paciente');
			return $query->result();
		}
}
