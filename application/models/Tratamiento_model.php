<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}
class Tratamiento_model extends CI_Model {

  function __construct() {
    parent::__construct();
    $this->db = $this->load->database('default', TRUE);
    }

    function obtenerHorasTratamientos(){
      $where = "fecha_inicio >= CURDATE()";
      $this->db->select('*');
      if($where != NULL){
          $this->db->where($where,NULL,FALSE);
      }
      $query = $this->db->get('vw_tratamientos');
      return $query->result();
    }


    function eliminarTratamientoPaciente($id) {
      //die(print($id));
        $this->db->trans_begin();
      $this->db->where('id', $id);
      $this->db->delete('tratamientos_pacientes');
        if ($this->db->trans_status() === FALSE) {
        $this->db->trans_rollback();
        return FALSE;
      } else {
        $this->db->trans_commit();
        return TRUE;
      }
    }

    function actualizarTratamiento($serv = array(),$id){
      $this->db->trans_begin();
      $this->db->where('id', $id);
      $this->db->update('tratamientos_pacientes', $serv);
      if ($this->db->trans_status() === FALSE) {
      $this->db->trans_rollback();
      return FALSE;
       } else {
        $this->db->trans_commit();
         return TRUE;
      }
   }

    function obtenerDetalleTratamiento($id){
      $where = "id = ".$id."";
      $this->db->select('*');
      if($where != NULL){
          $this->db->where($where,NULL,FALSE);
      }
      $query = $this->db->get('vw_tratamientos');
      return $query->row();
    }

    function obtenerListaTratamientosUsuario($id){
			$where = "cod_usuario = ".$id." AND fecha_inicio >= CURDATE()";
			$this->db->select('*');
      $this->db->order_by('fecha_inicio', 'ASC');
      $this->db->order_by('hora', 'ASC');
			if($where != NULL){
					$this->db->where($where,NULL,FALSE);
			}
			$query = $this->db->get('vw_tratamientos');
			return $query->result();
		}

    function obtenerNumTratamientosUsuario($id){
      $where = "cod_usuario = ".$id."";
      $this->db->select('COUNT(id) AS num_tratamientos , cod_tipo');
      if($where != NULL){
          $this->db->where($where,NULL,FALSE);
      }
      $query = $this->db->get('vw_tratamientos');
      return $query->row();
    }

    function obtenerListaTratamientos(){
			$where = "";
			$this->db->select('*');
			if($where != NULL){
					$this->db->where($where,NULL,FALSE);
			}
			$query = $this->db->get('vw_tratamientos');
			return $query->result();
		}
}
