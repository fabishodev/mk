<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}
class Documento_model extends CI_Model {

  function __construct() {
    parent::__construct();
    $this->db = $this->load->database('default', TRUE);
    }
    function obtenerDetalleDocumento($id){
			$where = "id = ".$id."";
			$this->db->select('*');
			if($where != NULL){
					$this->db->where($where,NULL,FALSE);
			}
			$query = $this->db->get('documentos_pacientes');
			return $query->row();
		}

    function eliminarDocumentoPaciente($id) {
			//die(print($id));
				$this->db->trans_begin();
			$this->db->where('id', $id);
			$this->db->delete('documentos_pacientes');
				if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				return FALSE;
			} else {
				$this->db->trans_commit();
				return TRUE;
			}
		}

    function obtenerNumDocumentosUsuario($id){
      $where = "cod_usuario = ".$id."";
      $this->db->select('COUNT(id) AS num_documentos , cod_tipo');
      if($where != NULL){
          $this->db->where($where,NULL,FALSE);
      }
      $query = $this->db->get('vw_documentos');
      return $query->row();
    }

    function obtenerTiposDocumento(){
			$where = "activo = 1";
			$this->db->select('*');
			if($where != NULL){
					$this->db->where($where,NULL,FALSE);
			}
			$query = $this->db->get('cat_tipos_documento');
			return $query->result();
		}

    function obtenerListaDocumentos(){
			$where = "";
			$this->db->select('*');
			if($where != NULL){
					$this->db->where($where,NULL,FALSE);
			}
			$query = $this->db->get('vw_documentos');
			return $query->result();
		}
}
