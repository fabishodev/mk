<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}
class Doctor_model extends CI_Model {

  function __construct() {
    parent::__construct();
    $this->db = $this->load->database('default', TRUE);
    }

    function obtenerDetalleDoctor($id){
      $where = "id = ".$id."";
      $this->db->select('*');
      if($where != NULL){
          $this->db->where($where,NULL,FALSE);
      }
      $query = $this->db->get('vw_doctor');
      return $query->row();
    }

    function actualizarDatosDoctor($serv = array(),$id){
			$this->db->trans_begin();
      $this->db->where('cod_usuario', $id);
      $this->db->update('datos_doctores', $serv);
			if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return FALSE;
			 } else {
			  $this->db->trans_commit();
			   return TRUE;
		  }
	 }

   public function registrarDatosDoctor($serv = array()){
       $this->db->trans_begin();
       $this->db->insert('datos_doctores', $serv);
       if ($this->db->trans_status() === false) {
           $this->db->trans_rollback();
           return false;
       } else {
           $this->db->trans_commit();
           return true;
       }
     }


    function obtenerDatosDoctor($id){
      $where = "cod_usuario = ".$id."";
      $this->db->select('*');
      if($where != NULL){
          $this->db->where($where,NULL,FALSE);
      }
      $query = $this->db->get('datos_doctores');
      return $query->row();
    }

    function obtenerListaDoctores(){
			$where = "cod_tipo = 4";
			$this->db->select('*');
			if($where != NULL){
					$this->db->where($where,NULL,FALSE);
			}
			$query = $this->db->get('vw_doctor');
			return $query->result();
		}
}
