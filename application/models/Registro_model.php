<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}
class Registro_model extends CI_Model {

  function __construct() {
    parent::__construct();
    $this->db = $this->load->database('default', TRUE);
    }

    public function registrarUsuario($serv = array()){
        $this->db->trans_begin();
        $this->db->insert('cat_usuarios', $serv);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
      }

    function existeCorreo($correo){
      $where = "correo ='".$correo."'";
      $this->db->select('*');
      $this->db->from('cat_usuarios');
      if ($where != NULL) {
        $this->db->where($where, NULL, FALSE);
      }
      $query = $this->db->get();
      return $query->row();
    }
}
