<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH.'libraries/swift_mailer/lib/swift_required.php';

class Notificaciones {

  public function EnviarNotificacionRecordatorioTratamiento($nombre, $email,$medicamento, $observacion, $fecha_inicio,$fecha_fin, $hora, $hora_fin, $lapso)
   {
    //echo "Enviando Correos";
    //ini_set('max_execution_time', 28800); //240 segundos = 4 minutos
     //Enviar correo electr�nico
          $url = base_url();
          $transport = Swift_SmtpTransport::newInstance()
                ->setHost('smtp.gmail.com')
                ->setPort(465)
                ->setEncryption('ssl')
                ->setUsername('indiesoftmx@gmail.com')
                ->setPassword('$i2n0d1i6e');

                //Create the Mailer using your created Transport
                $mailer = Swift_Mailer::newInstance($transport);
                //$this->load->model("Solicitud_model", "solicitud");
                //$query = $this->solicitud->getAlumnosCorreo();

                //Pass it as a parameter when you create the message
                $message = Swift_Message::newInstance();
                //Give the message a subject
                //Or set it after like this

                $message->setSubject('Recordatorio Tratamiento');
                //no_reply@ugto.mx
                $message->setFrom(array('indiesoftmx@gmail.com' => 'MedKeep'));
                $message->addTo($email);
                //$message->addTo('mgsnikips@gmail.com');

                //$message->addBcc('fabishodev@gmail.com');

                //Add alternative parts with addPart()
                $failedRecipients = array();
                $message->addPart("<h2>Estimado, </h2>".$nombre."
                <br>
                <h3>Le recordamos su tratamiento:</h3>
                <br>
                Medicamento: ".$medicamento."<br>
                Tomar cada: ".$lapso." (hrs)<br>
                Fecha inicio: ".$fecha_inicio."<br>
                Fecha fin: ".$fecha_fin."<br>
                Hora Inicio: ".$hora."<br>
                Hora Fin: ".$hora_fin."<br>
                Observación: ".$observacion."<br>
                ---<br>
                <img src='".$message->embed(Swift_Image::fromPath(base_url().'images/logo-medkeep.png'))."' />
                ", 'text/html');

                if (!$mailer->send($message)) {
                  return FALSE;
                }else {
                  return TRUE;
                }

    }

  public function EnviarCorreoTratamientoActualizado($nombre, $email,$medicamento, $observacion, $fecha_inicio,$fecha_fin, $hora,$hora_fin,$lapso)
   {
    //echo "Enviando Correos";
    //ini_set('max_execution_time', 28800); //240 segundos = 4 minutos
     //Enviar correo electr�nico
          $url = base_url();
          $transport = Swift_SmtpTransport::newInstance()
                ->setHost('smtp.gmail.com')
                ->setPort(465)
                ->setEncryption('ssl')
                ->setUsername('indiesoftmx@gmail.com')
                ->setPassword('$i2n0d1i6e');

                //Create the Mailer using your created Transport
                $mailer = Swift_Mailer::newInstance($transport);
                //$this->load->model("Solicitud_model", "solicitud");
                //$query = $this->solicitud->getAlumnosCorreo();

                //Pass it as a parameter when you create the message
                $message = Swift_Message::newInstance();
                //Give the message a subject
                //Or set it after like this

                $message->setSubject('Cambio de Tratamiento');
                //no_reply@ugto.mx
                $message->setFrom(array('indiesoftmx@gmail.com' => 'MedKeep'));
                $message->addTo($email);
                //$message->addTo('mgsnikips@gmail.com');

                //$message->addBcc('fabishodev@gmail.com');

                //Add alternative parts with addPart()
                $failedRecipients = array();
                $message->addPart("<h2>Gracias, </h2>".$nombre."
                <br>
                <h3>Su Tratamiento ha sido editado con éxito.</h3>
                <br>
                Medicamento: ".$medicamento."<br>
                Tomar cada: ".$lapso." (hrs)<br>
                Fecha inicio: ".$fecha_inicio."<br>
                Fecha fin: ".$fecha_fin."<br>
                Hora Inicio: ".$hora."<br>
                Hora Fin: ".$hora_fin."<br>
                Observación: ".$observacion."<br>
                ---<br>
                <img src='".$message->embed(Swift_Image::fromPath(base_url().'images/logo-medkeep.png'))."' />
                ", 'text/html');

                if (!$mailer->send($message)) {
                  return FALSE;
                }else {
                  return TRUE;
                }

    }

  public function EnviarCorreoTratamiento($nombre, $email,$medicamento, $observacion, $fecha_inicio,$fecha_fin, $hora, $hora_fin,$lapso)
   {
    //echo "Enviando Correos";
    //ini_set('max_execution_time', 28800); //240 segundos = 4 minutos
     //Enviar correo electr�nico
          $url = base_url();
          $transport = Swift_SmtpTransport::newInstance()
                ->setHost('smtp.gmail.com')
                ->setPort(465)
                ->setEncryption('ssl')
                ->setUsername('indiesoftmx@gmail.com')
                ->setPassword('$i2n0d1i6e');

                //Create the Mailer using your created Transport
                $mailer = Swift_Mailer::newInstance($transport);
                //$this->load->model("Solicitud_model", "solicitud");
                //$query = $this->solicitud->getAlumnosCorreo();

                //Pass it as a parameter when you create the message
                $message = Swift_Message::newInstance();
                //Give the message a subject
                //Or set it after like this

                $message->setSubject('Tratamiento Agregado');
                //no_reply@ugto.mx
                $message->setFrom(array('indiesoftmx@gmail.com' => 'MedKeep'));
                $message->addTo($email);
                //$message->addTo('mgsnikips@gmail.com');

                //$message->addBcc('fabishodev@gmail.com');

                //Add alternative parts with addPart()
                $failedRecipients = array();
                $message->addPart("<h2>Gracias, </h2>".$nombre."
                <br>
                <h3>Su Tratamiento ha sido agregado con éxito.</h3>
                <br>
                Medicamento: ".$medicamento."<br>
                Tomar cada: ".$lapso." (hrs)<br>
                Fecha inicio: ".$fecha_inicio."<br>
                Fecha fin: ".$fecha_fin."<br>
                Hora Inicio: ".$hora."<br>
                Hora Fin: ".$hora_fin."<br>
                Observación: ".$observacion."<br>
                ---<br>
                <img src='".$message->embed(Swift_Image::fromPath(base_url().'images/logo-medkeep.png'))."' />
                ", 'text/html');

                if (!$mailer->send($message)) {
                  return FALSE;
                }else {
                  return TRUE;
                }

    }

  public function EnviarCorreoEditarCita($nombre, $email, $doctor, $hospital, $motivo, $fecha, $hora)
   {
    //echo "Enviando Correos";
    //ini_set('max_execution_time', 28800); //240 segundos = 4 minutos
     //Enviar correo electr�nico
          $url = base_url();
          $transport = Swift_SmtpTransport::newInstance()
                ->setHost('smtp.gmail.com')
                ->setPort(465)
                ->setEncryption('ssl')
                ->setUsername('indiesoftmx@gmail.com')
                ->setPassword('$i2n0d1i6e');

                //Create the Mailer using your created Transport
                $mailer = Swift_Mailer::newInstance($transport);
                //$this->load->model("Solicitud_model", "solicitud");
                //$query = $this->solicitud->getAlumnosCorreo();

                //Pass it as a parameter when you create the message
                $message = Swift_Message::newInstance();
                //Give the message a subject
                //Or set it after like this

                $message->setSubject('Cambio de Cita Agendada');
                //no_reply@ugto.mx
                $message->setFrom(array('indiesoftmx@gmail.com' => 'MedKeep'));
                $message->addTo($email);
                //$message->addTo('mgsnikips@gmail.com');

                //$message->addBcc('fabishodev@gmail.com');

                //Add alternative parts with addPart()
                $failedRecipients = array();
                $message->addPart("<h2>Gracias, </h2>".$nombre."
                <br>
                <h3>Su Cita agendada ha sido editada con éxito.</h3>
                <br>
                Cita con Dr(a): ".$doctor."<br>
                Motivo: ".$motivo."<br>
                Hospital: ".$hospital."<br>
                Fecha: ".$fecha."<br>
                Hora: ".$hora."<br>
                ---<br>
                <img src='".$message->embed(Swift_Image::fromPath(base_url().'images/logo-medkeep.png'))."' />
                ", 'text/html');

                if (!$mailer->send($message)) {
                  return FALSE;
                }else {
                  return TRUE;
                }

    }

  public function EnviarCorreoAgendarCita($nombre, $email, $doctor, $hospital, $motivo, $fecha, $hora)
   {
    //echo "Enviando Correos";
    //ini_set('max_execution_time', 28800); //240 segundos = 4 minutos
     //Enviar correo electr�nico
          $url = base_url();
          $transport = Swift_SmtpTransport::newInstance()
                ->setHost('smtp.gmail.com')
                ->setPort(465)
                ->setEncryption('ssl')
                ->setUsername('indiesoftmx@gmail.com')
                ->setPassword('$i2n0d1i6e');

                //Create the Mailer using your created Transport
                $mailer = Swift_Mailer::newInstance($transport);
                //$this->load->model("Solicitud_model", "solicitud");
                //$query = $this->solicitud->getAlumnosCorreo();

                //Pass it as a parameter when you create the message
                $message = Swift_Message::newInstance();
                //Give the message a subject
                //Or set it after like this

                $message->setSubject('Cita Agendada');
                //no_reply@ugto.mx
                $message->setFrom(array('indiesoftmx@gmail.com' => 'MedKeep'));
                $message->addTo($email);
                //$message->addTo('mgsnikips@gmail.com');

                //$message->addBcc('fabishodev@gmail.com');

                //Add alternative parts with addPart()
                $failedRecipients = array();
                $message->addPart("<h2>Gracias, </h2>".$nombre."
                <br>
                <h3>Su Cita ha sido agendada con éxito.</h3>
                <br>
                Cita con Dr(a): ".$doctor."<br>
                Motivo: ".$motivo."<br>
                Hospital: ".$hospital."<br>
                Fecha: ".$fecha."<br>
                Hora: ".$hora."<br>
                ---<br>
                <img src='".$message->embed(Swift_Image::fromPath(base_url().'images/logo-medkeep.png'))."' />
                ", 'text/html');

                if (!$mailer->send($message)) {
                  return FALSE;
                }else {
                  return TRUE;
                }

    }

    public function EnviarCorreoRecordatorio($email, $doctor, $hospital, $motivo, $fecha, $hora)
     {
      //echo "Enviando Correos";
      //ini_set('max_execution_time', 28800); //240 segundos = 4 minutos
       //Enviar correo electr�nico
            $url = base_url();
            $transport = Swift_SmtpTransport::newInstance()
                  ->setHost('smtp.gmail.com')
                  ->setPort(465)
                  ->setEncryption('ssl')
                  ->setUsername('indiesoftmx@gmail.com')
                  ->setPassword('$i2n0d1i6e');

                  //Create the Mailer using your created Transport
                  $mailer = Swift_Mailer::newInstance($transport);
                  //$this->load->model("Solicitud_model", "solicitud");
                  //$query = $this->solicitud->getAlumnosCorreo();

                  //Pass it as a parameter when you create the message
                  $message = Swift_Message::newInstance();
                  //Give the message a subject
                  //Or set it after like this

                  $message->setSubject('Cron Job');
                  //no_reply@ugto.mx
                  $message->setFrom(array('indiesoftmx@gmail.com' => 'MedKeep'));
                  $message->addTo($email);
                  //$message->addTo('mgsnikips@gmail.com');

                  //$message->addBcc('fabishodev@gmail.com');

                  //Add alternative parts with addPart()
                  $failedRecipients = array();
                  $message->addPart("<h2>Recordatorio de cita, </h2>
                  <br>
                  Cita con Dr(a): ".$doctor."<br>
                  Motivo: ".$motivo."<br>
                  Hospital: ".$hospital."<br>
                  Fecha: ".$fecha."<br>
                  Hora: ".$hora."<br>
                  ---<br>
                  <img src='".$message->embed(Swift_Image::fromPath(base_url().'images/logo-medkeep.png'))."' />
                  ", 'text/html');

                  if (!$mailer->send($message)) {
                    return FALSE;
                  }else {
                    return TRUE;
                  }

      }


}
