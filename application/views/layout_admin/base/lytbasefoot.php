</div>
</div>
<footer>
     <div class="container">

        <div class="copy text-center">
           Copyright <?php echo date('Y'); ?> <a href='#'>Medkeep</a>
        </div>

     </div>
  </footer>


<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>bootstrap/js/custom.js"></script>
</body>
</html>
