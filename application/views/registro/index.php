<div class="sub-banner">
  <img class="banner-img" src="<?php echo base_url(); ?>images/registro1.jpg" alt="">
</div>
<!--Start Content-->
<div class="content">
  <div class="main-appointment-form">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="main-title text-center">
          <h2>Solicita una demostraci&oacute;n.</h2>
            <p>Descubra como MEDKEEP puede hacer m&aacute;s eficiente el manejo de su informaci&oacute;n m&eacute;dica.<br>
Solicita una cuenta de registro totalmente gratis, as&iacute; podr&aacute; descubrir todo lo que nuestra herramienta puede hacer por usted.</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="icon-center">
            <div class="appoint-icon">
              <i class="icon-clipboard"></i>
            </div>
          </div>
          <div class="appointment-form">
            <div class="form">
              <section class="bgcolor-a">
                <p class="error" id="error" style="display:none;"></p>
                <p class="success" id="success" style="display:none;"></p>
                <form name="appointment_form" id="appointment_form" method="post" action="#" onSubmit="return false">
                  <span class="input input--kohana">
                    <input class="input__field input__field--kohana" type="text" id="nombre" name="nombre" />
                    <label class="input__label input__label--kohana" for="nombre">
                      <i class="icon-user6 icon icon--kohana"></i>
                      <span class="input__label-content input__label-content--kohana">Tu nombre</span>
                    </label>
                  </span>
                  <span class="input input--kohana">
                    <input class="input__field input__field--kohana" type="text" id="ape-paterno" name="ape-paterno" />
                    <label class="input__label input__label--kohana" for="ape-paterno">
                      <i class="icon-user6 icon icon--kohana"></i>
                      <span class="input__label-content input__label-content--kohana">Tu apellido paterno</span>
                    </label>
                  </span>
                  <span class="input input--kohana">
                    <input class="input__field input__field--kohana" type="text" id="ape-materno" name="ape-materno" />
                    <label class="input__label input__label--kohana" for="ape-materno">
                      <i class="icon-user6 icon icon--kohana"></i>
                      <span class="input__label-content input__label-content--kohana">Tu apellido materno</span>
                    </label>
                  </span>
                  <span class="input input--kohana last">
                    <input class="input__field input__field--kohana" type="text" id="telefono" name="telefono"/>
                    <label class="input__label input__label--kohana" for="telefono">
                      <i class="icon-phone5 icon icon--kohana"></i>
                      <span class="input__label-content input__label-content--kohana">Teléfono</span>
                    </label>
                  </span>
                  <span class="input input--kohana last">
                    <input class="input__field input__field--kohana" type="text" id="correo" name="correo"/>
                    <label class="input__label input__label--kohana" for="correo">
                      <i class="icon-dollar icon icon--kohana"></i>
                      <span class="input__label-content input__label-content--kohana">Correo electrónico</span>
                    </label>
                  </span>
                  <span class="input input--kohana last">
                    <input class="input__field input__field--kohana" type="password" id="password" name="password"/>
                    <label class="input__label input__label--kohana" for="password">
                      <i class="icon-key icon icon--kohana"></i>
                      <span class="input__label-content input__label-content--kohana">Password</span>
                    </label>
                  </span>
                  <input name="submit_appointment" type="submit" value="Enviar" onClick="validateAppointment();">
                </form>
              </section>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--End Content-->
<script type="text/javascript">
  medkeep.funciones.menu_registro();
  medkeep.funciones.menu_registro_mobile();
</script>
