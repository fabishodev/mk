<!--Start Banner-->
<div class="sub-banner">
  <img class="banner-img" src="<?php echo base_url(); ?>images/sub-banner.jpg" alt="">
 </div>
<!--End Banner-->
<!--Start Content-->
<div class="content">
  <div class="contact-us">
    <div class="leave-msg dark-back">
      <div class="container">
        <div class="rox">
          <div class="col-md-7">
            <div class="main-title">
              <h2><span>Nos</span> gustar&iacute;a <span>conocer tu opini&oacute;n</span></h2>
              <p>Si quieres enviarnos tu opini&oacute;n, inquietudes o preguntas acerca de MEDKEEP, estas en el lugar indicado para que las conozcamos. </p>
            </div>
            <div class="form">
              <div class="row">
                <p class="success" id="success" style="display:none;"></p>
                <p class="error" id="error" style="display:none;"></p>
                <form name="contact_form" id="contact_form" method="post" action="#" onSubmit="return false">
                  <div class="col-md-4"><input type="text" data-delay="300" placeholder="Tu nombre" name="contact_name" id="contact_name" class="input"></div>
                  <div class="col-md-4"><input type="text" data-delay="300" placeholder="Correo electrónico" name="contact_email" id="contact_email" class="input"></div>
                  <div class="col-md-4"><input type="text" data-delay="300" placeholder="Asunto" name="contact_subject" id="contact_subject" class="input"></div>
                  <div class="col-md-12"><textarea data-delay="500" class="required valid" placeholder="Mensaje" name="message" id="message"></textarea></div>
                  <div class="col-md-3"><input name=" " type="submit" value="Enviar" onClick="validateContact();"></div>
                </form>
              </div>
            </div>
          </div>
          <div class="col-md-5">
            <div class="contact-get">
              <div class="main-title">
                <h2><span>Info</span> Medkeep</h2>
                <p>Env&iacute;anos un mensaje para as&iacute; poder entrar en contacto.</p>
              </div>
              <div class="get-in-touch">
                <div class="detail">
                  <span><b>Email:</b> <a href="#.">info@medkeep.mx</a></span>
                  <span><b>Web:</b> <a href="#.">www.medkeep.mx</a></span>
                </div>
                <div class="social-icons">
                  <a href="#." class="fb"><i class="icon-euro"></i></a>
                  <a href="#." class="tw"><i class="icon-yen"></i></a>
                  <a href="#." class="gp"><i class="icon-google-plus"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--End Content-->
<script type="text/javascript">
  medkeep.funciones.menu_contacto();
  medkeep.funciones.menu_contacto_mobile();
</script>
