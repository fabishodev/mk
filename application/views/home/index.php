<!--Start Banner-->
<div class="tp-banner-container">
  <div class="tp-banner">
    <ul>
      <!-- SLIDE  -->
      <li data-transition="fade" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="Intro Slide">
        <img src="<?php echo base_url(); ?>images/slides/banner-img8.jpg" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
        <div class="tp-caption customin customout rs-parallaxlevel-0" data-x="right" data-y="188" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
          data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="700" data-start="1200" data-easing="Power3.easeInOut" data-elementdelay="0.1"
          data-endelementdelay="0.1" style="z-index: 4;"><img class="transprint" src="<?php echo base_url(); ?>images/slides/transprint-bg.png" alt="">
        </div>
        <div class="tp-caption title-bold tp-resizeme rs-parallaxlevel-0 fade start" data-x="670" data-y="218" data-speed="1000" data-start="1800" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1"
          data-endspeed="300" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">Medkeep
        </div>
        <div class="tp-caption small-title tp-resizeme rs-parallaxlevel-0 fade start" data-x="670" data-y="255" data-speed="1000" data-start="1800" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1"
          data-endspeed="300" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">Su asistente médico en línea
        </div>
        <div class="tp-caption paragraph tp-resizeme rs-parallaxlevel-0 fade start" data-x="670" data-y="325" data-speed="1000" data-start="1800" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.05" data-endelementdelay="0.1"
          data-endspeed="300" style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">
          <div style="text-align:left;">Herramienta médica electrónica diseñada para <br>asistirlo a usted. </div>
        </div>
        <div class="tp-caption banner-button tp-resizeme rs-parallaxlevel-0  fade start" data-x="670" data-y="402" data-speed="1000" data-start="1800" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.05" data-endelementdelay="0.1"
          data-endspeed="300" style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap; border-radius: 5px;">
          <div style="text-align:left; background:#525866; border-radius: 5px;">

          </div>
        </div>
      </li>
      <li data-transition="fade" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="Intro Slide">
        <img src="<?php echo base_url(); ?>images/slides/banner-img9.jpg" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
        <div class="tp-caption customin customout rs-parallaxlevel-0" data-x="left" data-y="188" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
          data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="700" data-start="1200" data-easing="Power3.easeInOut" data-elementdelay="0.1"
          data-endelementdelay="0.1" style="z-index: 4;"><img style="border-bottom:solid 6px #02adc6;" src="<?php echo base_url(); ?>images/slides/transprint-bg.png" alt="">
        </div>
        <div class="tp-caption title-bold tp-resizeme rs-parallaxlevel-0 fade start" data-x="30" data-y="218" data-speed="1000" data-start="1800" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1"
          data-endspeed="300" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">Medkeep
        </div>
        <div class="tp-caption small-title tp-resizeme rs-parallaxlevel-0 fade start" data-x="30" data-y="255" data-speed="1000" data-start="1800" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1"
          data-endspeed="300" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">SU INFORMACIÓN DISPONIBLE Y SEGURA
        </div>
        <div class="tp-caption paragraph tp-resizeme rs-parallaxlevel-0 fade start" data-x="30" data-y="325" data-speed="1000" data-start="1800" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.05" data-endelementdelay="0.1"
          data-endspeed="300" style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">
          <div style="text-align:left;">Historial médico en línea y disponible las 24 horas, <br>los 365 días al año.</div>
        </div>
        <div class="tp-caption banner-button tp-resizeme rs-parallaxlevel-0  fade start" data-x="30" data-y="402" data-speed="1000" data-start="1800" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.05" data-endelementdelay="0.1"
          data-endspeed="300" style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap; border-radius: 5px;">
          <div style="text-align:left; background:#525866; border-radius: 5px;">

          </div>
        </div>
      </li>
      <li data-transition="fade" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="Intro Slide">
        <img src="<?php echo base_url(); ?>images/slides/banner-img10.jpg" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
        <div class="tp-caption customin customout rs-parallaxlevel-0" data-x="right" data-y="188" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
          data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="700" data-start="1200" data-easing="Power3.easeInOut" data-elementdelay="0.1"
          data-endelementdelay="0.1" style="z-index: 4;"><img style="border-bottom:solid 6px #02adc6;" src="<?php echo base_url(); ?>images/slides/transprint-bg.png" alt="">
        </div>
        <div class="tp-caption title-bold tp-resizeme rs-parallaxlevel-0 fade start" data-x="670" data-y="218" data-speed="1000" data-start="1800" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1"
          data-endspeed="300" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">Medkeep
        </div>
        <div class="tp-caption small-title tp-resizeme rs-parallaxlevel-0 fade start" data-x="670" data-y="255" data-speed="1000" data-start="1800" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1"
          data-endspeed="300" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">TODO EN UN SOLO LUGAR
        </div>
        <div class="tp-caption paragraph tp-resizeme rs-parallaxlevel-0 fade start" data-x="670" data-y="325" data-speed="1000" data-start="1800" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.05" data-endelementdelay="0.1"
          data-endspeed="300" style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">
          <div style="text-align:left;">Tenga siempre a la mano su historial médico, <br>antecedentes y sus documentos.</div>
        </div>
        <div class="tp-caption banner-button tp-resizeme rs-parallaxlevel-0  fade start" data-x="670" data-y="402" data-speed="1000" data-start="1800" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.05" data-endelementdelay="0.1"
          data-endspeed="300" style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap; border-radius: 5px;">
          <div style="text-align:left; background:#525866; border-radius: 5px;">

          </div>
        </div>
      </li>
      <li data-transition="fade" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="Intro Slide">
        <img src="<?php echo base_url(); ?>images/slides/banenr-img2.jpg" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
        <div class="tp-caption customin customout rs-parallaxlevel-0" data-x="left" data-y="188" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
          data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="700" data-start="1200" data-easing="Power3.easeInOut" data-elementdelay="0.1"
          data-endelementdelay="0.1" style="z-index: 4;"><img style="border-bottom:solid 6px #02adc6;" src="<?php echo base_url(); ?>images/slides/transprint-bg.png" alt="">
        </div>
        <div class="tp-caption title-bold tp-resizeme rs-parallaxlevel-0 fade start" data-x="30" data-y="218" data-speed="1000" data-start="1800" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1"
          data-endspeed="300" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">Medkeep
        </div>
        <div class="tp-caption small-title tp-resizeme rs-parallaxlevel-0 fade start" data-x="30" data-y="255" data-speed="1000" data-start="1800" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1"
          data-endspeed="300" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">REGISTRATE Y HAZ UNA PRUEBA GRATIS
        </div>
        <div class="tp-caption paragraph tp-resizeme rs-parallaxlevel-0 fade start" data-x="30" data-y="325" data-speed="1000" data-start="1800" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.05" data-endelementdelay="0.1"
          data-endspeed="300" style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">
          <div style="text-align:left;">Registra tu información e inicia una prueba totalmente gratis.</div>
        </div>
        <div class="tp-caption banner-button tp-resizeme rs-parallaxlevel-0  fade start" data-x="30" data-y="402" data-speed="1000" data-start="1800" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.05" data-endelementdelay="0.1"
          data-endspeed="300" style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap; border-radius: 5px;">
          <div style="text-align:left; background:#525866; border-radius: 5px;">
            <a href="<?php echo site_url('registro'); ?>" class="read-more" style=" line-height: initial; color: #fff; text-transform: uppercase; font-weight: 500; padding: 12px 36px; display: inline-block; font-size: 18px; ">Registro de usuarios</a>
          </div>
        </div>
      </li>
    </ul>
    <div class="tp-bannertimer"></div>
  </div>
</div>
<!--End Banner-->
<!--Start Make Appointment-->
<!--End Make Appointment-->
<!--Start Content-->
<div class="content">
  <!--Start Services-->
  <div class="services-one">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="service-sec">
            <div class="icon">
              <i class="icon-medical"></i>
            </div>
            <div class="detail">
              <h5>Control de su historial médico.</h5>
              <p>Almacene su información médica en la nube, siempre disponible en todo momento y en cualquier lugar.</p>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="service-sec">
            <div class="icon">
              <i class="icon-doctor"></i>
            </div>
            <div class="detail">
              <h5>Programe y administre sus citas médicas.</h5>
              <p>Capture y programe sus citas médicas en nuestra herramienta de una forma fácil y sencilla, su agenda médica en línea no le permitirá olvidar ninguna cita.</p>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="service-sec">
            <div class="icon">
              <i class="icon-Medicine-bottle"></i>
            </div>
            <div class="detail">
              <h5>Programe sus tratamientos médicos.</h5>
              <p>Agende y programe sus tratamientos médicos en nuestra herramienta, así recibirá alertas
de su tratamiento y podrá consultarlo en todo momento.</p>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="service-sec">
            <div class="icon">
              <i class="icon-prescription-pad"></i>
            </div>
            <div class="detail">
              <h5>Reciba Notificaciones y alertas.</h5>
              <p>Nuestros usuarios siempre estarán informados sobre citas o tratamientos programados, a
través de nuestras notificaciones y alertas.</p>
            </div>
          </div>
        </div>
        <div class="col-md-6">
              <div class="service-sec">

                <div class="icon">
                  <i class="icon-tablet6"></i>
                </div>

                <div class="detail">
                  <h5>Referencias médicas en línea.</h5>
                  <p>Descubre los servicios médicos más cercanos a tu ubicación a través de nuestro directorio electrónico de referencias médicas.</p>
                </div>

              </div>
            </div>

      </div>
    </div>
  </div>
  <!--End Services-->
<!--Start Doctor Quote-->
  <div class="dr-quote">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <span class="quote">"MEDKEEP es una herramienta electrónica amigable, que le permite almacenar su información médica en línea y facilita su consulta en cualquier momento, a cualquier hora y desde cualquier lugar."</span>
          <span class="name">-- Medkeep --</span>
        </div>
      </div>
    </div>
  </div>
  <!--End Doctor Quote-->
</div>
<!--End Content-->
<script type="text/javascript">
  medkeep.funciones.menu_home();
  medkeep.funciones.menu_home_mobile();
</script>
