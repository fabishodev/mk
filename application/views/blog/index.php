<div class="sub-banner">
  <img class="banner-img" src="<?php echo base_url(); ?>images/blog.jpg" alt="">
</div>
<br>
<!--Start Content-->
<div class="content">
  <div class="news-posts">
   <div class="container">
     <div class="row">
       <div class="col-md-8">
         <?php if ($noticias): ?>
           <?php foreach ($noticias as $fila): ?>
             <div class="col-md-6">
             <div class="news-sec">
               <img src="<?php echo base_url() ?>img/noticias/<?php echo $fila->caratula_noticia; ?>" alt="">
               <div class="detail">
                 <span><?php echo date("F j, Y, g:i a", strtotime($fila->fecha_creado)); ?></span>
                   <h3><?php echo $fila->titulo_noticia; ?></h3>
                   <p><?php echo $fila->noticia_corta; ?></p>
                   <a href="<?php echo site_url('blog/noticia/'.$fila->id); ?>" class="read-more">Leer más</a>
                 </div>
               </div>
           </div>
           <?php endforeach; ?>
         <?php endif; ?>
       </div>
       <div class="col-md-4">
         <div class="recent-posts">
           <h6 class="bar-title">Publicaciones recientes</h6>
           <?php if ($recientes): ?>
             <?php foreach ($recientes as $fila): ?>
               <div class="post-sec">
                <a href="<?php echo site_url('blog/noticia/'.$fila->id) ?>"><img src="<?php echo base_url(); ?>img/noticias/<?php echo $fila->caratula_noticia; ?>" alt=""></a>
                <a href="<?php echo site_url('blog/noticia/'.$fila->id) ?>" class="title"><?php echo $fila->titulo_noticia; ?></a>
                <span class="date"><?php echo date("F j, Y, g:i a", strtotime($fila->fecha_creado)); ?></span>
              </div>
             <?php endforeach; ?>
           <?php endif; ?>
        </div>
        <div class="clear"></div>
        <div class="categories">
          <h6 class="bar-title">Categorías</h6>
          <?php if ($etiquetas): ?>
            <ul>
              <?php foreach ($etiquetas as $e): ?>
                <li><a href="<?php echo site_url('blog/categoria/'.$e->cod_categoria) ?>"><i class="icon-chevron-small-right"></i><?php echo $e->categoria; ?></a></li>
              <?php endforeach; ?>
            </ul>
          <?php endif; ?>
        </div>
        <div class="clear"></div>
      </div>
     </div>
   </div>
 </div>
</div>
<!--End Content-->
<script type="text/javascript">
  medkeep.funciones.menu_blog();
  medkeep.funciones.menu_blog_mobile();
</script>
