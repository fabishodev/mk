<!--Start Content-->
<div class="content">
  <div class="news-posts">
   <div class="container">
     <div class="row">
       <?php if ($noticias): ?>
         <?php foreach ($noticias as $fila): ?>
           <div class="col-md-6">
           <div class="news-sec">
             <img src="<?php echo base_url() ?>img/noticias/<?php echo $fila->caratula_noticia; ?>" alt="">
             <div class="detail">
               <span><?php echo date("F j, Y, g:i a", strtotime($fila->fecha_creado)); ?></span>
                 <h3><?php echo $fila->titulo_noticia; ?></h3>
                 <p><?php echo $fila->noticia_corta; ?></p>
                 <a href="<?php echo site_url('blog/noticia/'.$fila->id); ?>" class="read-more">Leer más</a>
               </div>
             </div>
         </div>
         <?php endforeach; ?>
       <?php endif; ?>
     </div>
   </div>
 </div>
</div>
<!--End Content-->
