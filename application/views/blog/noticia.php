<!--Start Content-->
<div class="content">
  <div class="news-posts">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <div class="news-detail">
            <?php if ($noticia): ?>
              <img class="img-responsive" src="<?php echo base_url(); ?>img/noticias/<?php echo $noticia->caratula_noticia ?>" alt="">
              <div class="detail">
                <span class="date"><?php echo date("F j, Y, g:i a", strtotime($noticia->fecha_creado)); ?></span>
                <h3><?php  echo $noticia->titulo_noticia ?></h3>
                <?php echo $noticia->noticia_completa; ?>
              </div>
            <?php endif; ?>
          </div>
          <!-- <div class="share-post">
            <span>Comparte esta publicación</span>
            <div class="social-icons">
              <a href="#." class="fb"><i class="icon-euro"></i></a>
              <a href="#." class="tw"><i class="icon-yen"></i></a>
            </div>
          </div> -->
          <?php    $cod_usuario = $this->session->userdata('id_usuario'); ?>
          <div class="comments-sec">
            <div class="all-comments">
              <?php if ($comentarios): ?>
                <?php foreach ($comentarios as $fila): ?>
                  <div class="comment-box">
                    <?php if ($fila->foto !== NULL): ?>
                      <img src="<?php echo base_url(); ?>media/<?php   echo $fila->cod_usuario ; ?>/<?php echo $fila->foto ?>" alt="">
                      <?php else: ?>
                        <img src="<?php echo base_url(); ?>images/comment-img3.jpg" alt="">
                    <?php endif; ?>
                    <div class="detail">
                      <span class="name"><?php echo $fila->nombre.' '.$fila->ape_paterno.' '.$fila->ape_materno; ?></span>
                      <span class="date"><?php echo date("F j, Y, g:i a", strtotime($fila->fecha_comentario)); ?></span>
                      <p><strong><?php echo $fila->asunto ?></strong></p>
                      <p><?php echo $fila->comentario ?></p>
                    </div>
                    <div class="clear"></div>
                  </div>
                <?php endforeach; ?>
              <?php endif; ?>
            </div>
          </div>
          <?php $cod_tipo = $this->session->userdata('cod_tipo_usuario'); ?>
          <?php if ( $cod_tipo !== NULL && $cod_tipo !== 1 ): ?>
            <!--Notificaciones-->
            <?php if ($success != '') { ?>
                <p class="success" id="success"></p>
            <?php $this->session->set_userdata('success', '');} ?>
            <?php if ($danger != '') { ?>
                <p class="error" id="error"></p>
            <?php $this->session->set_userdata('danger', '');} ?>
            <!---->


          <?php $id_usuario = $this->session->userdata('id_usuario'); ?>
          <?php if ($id_usuario == NULL): ?>
          <?php   redirect('usuario'); ?>
          <?php endif; ?>
            <div class="leave-reply">
              <h3>Déjanos un comentario</h3>
              <form class="" action="<?php echo base_url(); ?>index.php/usuario/enviarComentario/<?php echo $noticia->id ?>" method="post">
                <div class="form">
                  <input type="hidden" data-delay="300" name="id-usuario" value="<?php echo $id_usuario ?>" required>
                  <input type="text" data-delay="300" placeholder="Asunto" name="asunto" class="input" required>
                  <textarea data-delay="500" class="required valid" placeholder="Mensaje" name="comentario" id="comentario" required></textarea>
                  <input name="" type="submit" value="Enviar">
                </div>
              </form>
            </div>
          <?php endif; ?>
        </div>
        <div class="col-md-4">
          <div class="recent-posts">
            <h6 class="bar-title">Publicaciones recientes</h6>
            <?php if ($recientes): ?>
              <?php foreach ($recientes as $fila): ?>
                <div class="post-sec">
                 <a href="<?php echo site_url('blog/noticia/'.$fila->id) ?>"><img src="<?php echo base_url(); ?>img/noticias/<?php echo $fila->caratula_noticia; ?>" alt=""></a>
                 <a href="<?php echo site_url('blog/noticia/'.$fila->id) ?>" class="title"><?php echo $fila->titulo_noticia; ?></a>
                 <span class="date"><?php echo date("F j, Y, g:i a", strtotime($fila->fecha_creado)); ?></span>
               </div>
              <?php endforeach; ?>
            <?php endif; ?>
         </div>
         <div class="clear"></div>
         <div class="categories">
           <h6 class="bar-title">Categorías</h6>
           <?php if ($etiquetas): ?>
             <ul>
               <?php foreach ($etiquetas as $e): ?>
                 <li><a href="<?php echo site_url('blog/categoria/'.$e->cod_categoria) ?>"><i class="icon-chevron-small-right"></i><?php echo $e->categoria; ?></a></li>
               <?php endforeach; ?>
             </ul>
           <?php endif; ?>
         </div>
         <div class="clear"></div>
       </div>
     </div>
   </div>
 </div>
</div>
<!--End Content-->
<script type="text/javascript">
  medkeep.funciones.menu_blog();
  medkeep.funciones.menu_blog_mobile();
</script>
