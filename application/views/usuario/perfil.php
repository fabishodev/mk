<div class="sub-banner">
  <img class="banner-img" src="<?php echo base_url(); ?>images/documentos.jpg" alt="">
</div>
<br>
<div class="row">
  <div class="col-md-12" style="padding:40px 10%; text-align:center;">
    <div class="main-title">
      <h2>Perfil de Usuario</h2>
        <p>Administre su perfil de usuario.<br></p>
      </div>
    </div>
  </div>
  <!--Start Content-->
  <div class="content">
    <div style="padding: 0px 0px 100px 0px;">
      <div class="container">
  <div class="row">
    <div class="col-md-12">
          <div class="row">
            <div class="col-md-6">
              <!--Notificaciones-->
              <?php if ($success != '') { ?>
                <div class="alert alert-success alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <strong><?php echo $success ?></strong>
              </div>
              <?php $this->session->set_userdata('success', '');} ?>

              <?php if ($danger != '') { ?>
                <div class="alert alert-danger alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <strong><?php echo $danger ?></strong>
                </div>
              <?php $this->session->set_userdata('danger', '');} ?>
              <!---->
            </div>
          </div>
        <?php if ($usuario): ?>
          <form id="form-user" class="form-horizontal" action="<?php echo base_url();?>index.php/usuario/editarUsuario/<?php echo $usuario->id;?>" method="post" enctype="multipart/form-data">
            <div class="row">
              <div class="col-md-6">

                <div class="form-group">
                  <label for="nombre" class="col-sm-2 control-label">Nombre</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" value="<?php echo $usuario->nombre ?>" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="ape-paterno" class="col-sm-2 control-label">Apellido Paterno</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="ape-paterno" name="ape-paterno" placeholder="Apellido Paterno" value="<?php echo $usuario->ape_paterno ?>" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="ape-aterno" class="col-sm-2 control-label">Apellido Materno</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="ape-materno" name="ape-materno" placeholder="Apellido Materno" value="<?php echo $usuario->ape_materno ?>" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="correo" class="col-sm-2 control-label">Correo</label>
                  <div class="col-sm-10">
                    <input type="email" class="form-control" id="correo" name="correo" placeholder="Apellido Materno" value="<?php echo $usuario->correo ?>" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="password" class="col-sm-2 control-label">Password</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                  </div>
                </div>
                <div class="form-group">
                  <label for="telefono" class="col-sm-2 control-label">Télefono</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Telefono" value="<?php echo $usuario->telefono ?>" required>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                    <label for="caratula" class="col-sm-2 control-label">Imagen</label>
                  <div class="col-sm-10">
                  <?php if ($usuario->foto): ?>
                    <img class="img-thumbnail img-responsive" src="<?php echo base_url() ?>media/<?php echo $usuario->id; ?>/<?php echo $usuario->foto; ?>" alt="...">
                  <?php else: ?>
                    <img src="<?php echo base_url(); ?>images/comment-img3.jpg" alt="">
                <?php endif; ?>
                  <p class="help-block">Imagen de usuario.</p>
                    </div>
                </div>
                <div class="form-group">
                  <label for="upl" class="col-sm-2 control-label">Cambiar Imagen</label>
                  <div class="col-sm-10">
                  <input type="file" id="upl" name="upl" >
                  <p class="help-block">Imagen de usuario.</p>
                    </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-warning">Editar</button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        <?php endif; ?>
    </div>
  </div>
</div>
</div>
</div>
<script type="text/javascript">
  medkeep.funciones.menu_usuario();
  medkeep.funciones.menu_usuario_perfil();
  medkeep.funciones.menu_usuario_mobile();
  medkeep.funciones.menu_perfil_mobile();
</script>
