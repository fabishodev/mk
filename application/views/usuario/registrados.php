<div class="sub-banner">
  <img class="banner-img" src="<?php echo base_url(); ?>images/slides/registrados.jpg" alt="">
</div>
<!--Start Content-->
<div class="row">
  <div class="col-md-12" style="padding:40px 10%; text-align:center;">
    <div class="main-title">
      <h2><span>¿Quieres probar cómo funciona nuestro servicio?</h2>
        <p></p>
      </div>
    </div>
  </div>
  <div class="content">
    <div class="row">
      <div class="col-md-12" style="padding: 0 0 50px 0;">
        <div class="col-md-4">
          <a href="<?php echo site_url('usuario/cita') ?>">
            <div style=" padding-bottom:10px;">
            <img class="img-rounded"  src="<?php echo base_url(); ?>images/slides/cita.jpg" alt="" >
          </div>
        </a>
        </div>
        <div class="col-md-4">
          <a href="<?php echo site_url('usuario/tratamiento') ?>">
            <div style=" padding-bottom:10px;">
              <img class="img-rounded" src="<?php echo base_url(); ?>images/slides/tratamiento.jpg" alt="" >
            </div>
          </a>
        </div>
        <div class="col-md-4">
          <a href="<?php echo site_url('usuario/documentos') ?>">
            <div style=" padding-bottom:10px;">
              <img class="img-rounded"  src="<?php echo base_url(); ?>images/slides/documentos.jpg" alt="" >
            </div>
          </a>
        </div>
      </div>
    </div>
</div>
<!--End Content-->
<script type="text/javascript">
  medkeep.funciones.menu_usuario();
  medkeep.funciones.menu_usuario_mobile();
</script>
