<?php echo link_tag('css/date_time_picker.css'); ?>
<?php echo link_tag('calendario/css/calendar.css'); ?>
<?php $id_usuario = $this->session->userdata('id_usuario'); ?>
<!-- Modal -->
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detalle Tratamiento</h4>
      </div>
      <div class="modal-body">
        <div id="detalle-tratamiento">

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="sub-banner">
  <img class="banner-img" src="<?php echo base_url(); ?>images/tratamientos.jpg" alt="">
</div>
<br>
<!--Start Content-->
<div class="row">
  <div class="col-md-12" style="padding:40px 10%; text-align:center;">
    <div class="main-title">
      <h2>Tratamiento</h2>
        <p>Programe sus tratamientos y permítanos administrar en nuestro calendario el seguimiento a cada uno de ellos, nuestra aplicación se encargará de que no vuelva a olvidar ningún tratamiento médico desde ahora</p>
      </div>
    </div>
  </div>
<div class="page">
  <section class="page-title">
    <div class="grid-row clearfix">
      <nav class="bread-crumbs">

      </nav>
     </div>
   </section>
   <!--/ page title -->

   <!-- page content -->
   <main class="page-content">
     <div class="grid-row">
       <!-- timetable -->
       <section class="timetable">
        <!-- <div class="widget-title" style="text-align:center; padding-top: 50px; font-size: 36px; font-weight: bold;"><h2>Citas</h2></div> -->
         <div style="padding-top:100px;">
           <div class="make-appointment">
             <div class="container">
               <ul id="accordion" class="accordion">
                 <li>
                   <div class="link"><i class="fa fa-database"></i><span class="appointment-title" style="font-size:19px;">Programar Tratamiento Médico</span> <i class="icon-chevron-down"></i> </div>
                   <section class="bgcolor-3">
                     <p class="error" id="error" style="display:none;"></p>
                     <p class="success" id="success" style="display:none;"></p>
                     <form name="appointment_form" id="appointment_form" method="post" action="#" onsubmit="return false;">
                       <span class="input input--kohana">
                         <input class="input__field input__field--kohana date-pick" type="text" id="fecha-inicio"  placeholder="Fecha Inicio" onClick="" name="fecha-inicio" required readonly />
                         <label class="input__label input__label--kohana" for="fecha-inicio">
                           <i class="icon-new-message icon icon--kohana"></i>
                           <span class="input__label-content input__label-content--kohana"></span>
                         </label>
                       </span>
                       <span class="input input--kohana">
                         <input class="input__field input__field--kohana date-pick" type="text" id="fecha-fin"  placeholder="Fecha Fin" onClick="" name="fecha-fin" required readonly />
                         <label class="input__label input__label--kohana" for="fecha-fin">
                           <i class="icon-new-message icon icon--kohana"></i>
                           <span class="input__label-content input__label-content--kohana"></span>
                         </label>
                       </span>
                       <span class="input input--kohana">
                          <input class="input__field input__field--kohana time-pick" type="text" id="hora-tratamiento" name="hora-tratamiento" placeholder="Hora Inicio" required readonly />
                          <label class="input__label input__label--kohana" for="hora-tratamiento">
                            <i class="icon-new-message icon icon--kohana"></i>
                            <span class="input__label-content input__label-content--kohana"></span>
                          </label>
                     </span>
                     <span class="input input--kohana">
                        <input class="input__field input__field--kohana time-pick" type="text" id="hora-fin" name="hora-fin" placeholder="Hora Fin" readonly />
                        <label class="input__label input__label--kohana" for="hora-fin">
                          <i class="icon-new-message icon icon--kohana"></i>
                          <span class="input__label-content input__label-content--kohana"></span>
                        </label>
                   </span>
                   <span class="input input--kohana">
                     <input class="input__field input__field--kohana" type="number" min="1" max="24" id="lapso" name="lapso" required/>
                     <label class="input__label input__label--kohana" for="lapso">
                       <i class="icon-new-message icon icon--kohana"></i>
                       <span class="input__label-content input__label-content--kohana">Tomar cada...</span>
                    </label>
                  </span>
                         <span class="input input--kohana">
                           <input class="input__field input__field--kohana" type="text" id="medicamento" name="medicamento" required/>
                           <label class="input__label input__label--kohana" for="medicamento">
                             <i class="icon-new-message icon icon--kohana"></i>
                             <span class="input__label-content input__label-content--kohana">Medicamento</span>
                          </label>
                        </span>
                        <span class="input input--kohana message">
                          <input class="input__field input__field--kohana" type="text" id="observaciones" name="observaciones" required >
                          <label class="input__label input__label--kohana" for="observaciones">
                            <i class="icon-new-message icon icon--kohana"></i>
                            <span class="input__label-content input__label-content--kohana">Observaciones</span>
                          </label>
                        </span>
                        <input name="submit" type="submit" value="Guardar" onClick="validateFormTratamiento()">
                   </form>
                 </section>
               </li>
             </ul>
           </div>
         </div>
       </div>

       </section>
       <!--/ timetable -->
     </div>


   </main>
   <div class="content">
     <div style="padding: 0px 0px 0px 0px;">
       <div class="container">
         <div class="row">
           <div class="col-md-12">
             <div class="">
               <div class="">
                 <section>
                   <!--Notificaciones-->
                   <?php if ($success != '') { ?>
                     <div class="alert alert-success alert-dismissible" role="alert">
                       <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       <strong><?php echo $success ?></strong>
                   </div>
                   <?php $this->session->set_userdata('success', '');} ?>

                   <?php if ($danger != '') { ?>
                     <div class="alert alert-danger alert-dismissible" role="alert">
                       <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       <strong><?php echo $danger ?></strong>
                     </div>
                   <?php $this->session->set_userdata('danger', '');} ?>
                   <!---->

                   <div class="row">
                                        <div class="page-header"><h2 style="font-size: 36px; color: #555;"></h2></div>
                                               <div class="pull-left form-inline"><br>
                                                 <div class="btn-group">
                                                    <button class="btn btn-info" data-calendar-nav="prev">Anterior</button>
                                                    <button class="btn btn-default active" data-calendar-nav="today">Hoy</button>
                                                    <button class="btn btn-info" data-calendar-nav="next">Siguiente</button>
                                                  </div>
                                                       <div class="btn-group">
                                                          <button class="btn btn-default active" data-calendar-view="month">Mes</button>
                                                           <button class="btn btn-default" data-calendar-view="year">Año</button>
                                                           <button class="btn btn-default" data-calendar-view="week">Semana</button>
                                                           <button class="btn btn-default" data-calendar-view="day">Dia</button>



                                                       </div>

                                               </div>
                                                   <div class="pull-right form-inline"><br>

                                                   </div>

                               </div>

                               <br>
                               <div class="row">
                                 <div class="col-md-4">
                                   <br>
                                   <?php if ($lista_tratamientos): ?>
                                     <?php foreach ($lista_tratamientos as $l): ?>
                                       <div class="list-group">
                                         <a style="background: #acc6d2;" href="#" class="list-group-item link-obtener-tratamiento" data-toggle="modal" data-target="#myModal">
                                            <input class="id-tratamiento" type="hidden" name="id-tratamiento" value="<?php echo $l->id; ?>">
                                           <h4 class="list-group-item-heading"><?php echo $l->medicamento; ?></h4>
                                            <p class="list-group-item-text">Tomar cada: <?php echo $l->lapso; ?></p>
                                           <p class="list-group-item-text">Inicio: <?php echo date("d-m-Y", strtotime($l->fecha_inicio)) ; ?></p>
                                           <p class="list-group-item-text">Fin: <?php echo date("d-m-Y", strtotime($l->fecha_fin)) ; ?></p>
                                            <p class="list-group-item-text">Hora inicio: <?php echo $l->hora; ?></p>
                                              <p class="list-group-item-text">Hora fin: <?php echo $l->hora_fin; ?></p>
                                             <p class="list-group-item-text"><?php echo $l->observaciones; ?></p>


                                         </a>
                                        </div>
                                     <?php endforeach; ?>
                                   <?php endif; ?>


                                 </div>
                                 <div class="col-md-8">
                                   <div id="calendar"></div> <!-- Aqui se mostrara nuestro calendario -->
                                   <!--ventana modal para el calendario-->
                                 </div>




                                           </div>

               </section>
               </div>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
   <!--End Content-->
 </div>
 <div class="col-md-12" style="padding: 50px 0 50px 0; background-color:#fff;">
   <div class="col-md-6">
     <a href="<?php echo site_url('usuario/cita') ?>">
       <div style=" padding-bottom:10px;">
         <img class="img-rounded" src="<?php echo base_url() ?>images/slides/cita.jpg" alt="" >
       </div>
     </a>
   </div>
   <div class="col-md-6">
     <a href="<?php echo site_url('usuario/documentos') ?>">
       <div style=" padding-bottom:10px;">
         <img class="img-rounded" src="<?php echo base_url() ?>images/slides/documentos.jpg" alt="" >
       </div>
     </a>
   </div>
</div><script type="text/javascript" src="<?php echo base_url();?>calendario/js/es-ES.js"></script>
<script src="<?php echo base_url();?>calendario/js/moment.js"></script>
<script src="<?php echo base_url();?>js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>js/bootstrap-timepicker.js"></script>
<script src="<?php echo base_url();?>js/bootstrap-datepicker.es.js"></script>
<script src="<?php echo base_url();?>vendors/fullcalendar/fullcalendar.min.js"></script>
<script src="<?php echo base_url();?>calendario/js/underscore-min.js"></script>
<script src="<?php echo base_url();?>calendario/js/calendar.js"></script>
<script type="text/javascript">
      (function($){
              //creamos la fecha actual
              var date = new Date();
              var yyyy = date.getFullYear().toString();
              var mm = (date.getMonth()+1).toString().length == 1 ? "0"+(date.getMonth()+1).toString() : (date.getMonth()+1).toString();
              var dd  = (date.getDate()).toString().length == 1 ? "0"+(date.getDate()).toString() : (date.getDate()).toString();

              //establecemos los valores del calendario
              var options = {

                  // definimos que los eventos se mostraran en ventana modal
                      modal: '#events-modal',

                      // dentro de un iframe
                      modal_type:'iframe',

                      //obtenemos los eventos de la base de datos
                      events_source: '<?php echo base_url();?>index.php/usuario/obtenerTratamientos/<?php echo $id_usuario ?>',

                      // mostramos el calendario en el mes
                      view: 'month',

                      // y dia actual
                      day: yyyy+"-"+mm+"-"+dd,


                      // definimos el idioma por defecto
                      language: 'es-ES',

                      //Template de nuestro calendario
                      tmpl_path: '<?php echo base_url();?>calendario/tmpls/',
                      tmpl_cache: false,


                      // Hora de inicio
                      time_start: '00:00',

                      // y Hora final de cada dia
                      time_end: '24:00',

                      // intervalo de tiempo entre las hora, en este caso son 30 minutos
                      time_split: '30',

                      // Definimos un ancho del 100% a nuestro calendario
                      width: '100%',

                      onAfterEventsLoad: function(events)
                      {
                              if(!events)
                              {
                                      return;
                              }
                              var list = $('#eventlist');
                              list.html('');

                              $.each(events, function(key, val)
                              {
                                      $(document.createElement('li'))
                                              .html('<a href="' + val.url + '">' + val.title + '</a>')
                                              .appendTo(list);
                              });
                      },
                      onAfterViewLoad: function(view)
                      {
                              $('.page-header h2').text(this.getTitle());
                              $('.btn-group button').removeClass('active');
                              $('button[data-calendar-view="' + view + '"]').addClass('active');
                      },
                      classes: {
                              months: {
                                      general: 'label'
                              }
                      }
              };


              // id del div donde se mostrara el calendario
              var calendar = $('#calendar').calendar(options);

              $('.btn-group button[data-calendar-nav]').each(function()
              {
                      var $this = $(this);
                      $this.click(function()
                      {
                              calendar.navigate($this.data('calendar-nav'));
                      });
              });

              $('.btn-group button[data-calendar-view]').each(function()
              {
                      var $this = $(this);
                      $this.click(function()
                      {
                              calendar.view($this.data('calendar-view'));
                      });
              });

              $('#first_day').change(function()
              {
                      var value = $(this).val();
                      value = value.length ? parseInt(value) : null;
                      calendar.setOptions({first_day: value});
                      calendar.view();
              });
      }(jQuery));
  </script>

<script>
$('input.date-pick').datepicker({
  format: 'yyyy/mm/dd',
  startDate: '0',
  language: 'es',
});
$('input.time-pick').timepicker({
  showInpunts: false,
});
</script>
<script type="text/javascript">
  medkeep.funciones.menu_usuario();
  medkeep.funciones.menu_usuario_mobile();
  medkeep.funciones.menu_usuario_tratamiento();
  medkeep.funciones.menu_usuario_tratamiento_mobile();
  medkeep.funciones.obtener_detalle_tratamiento();
</script>
