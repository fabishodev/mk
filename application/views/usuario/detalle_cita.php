
<form id="form-user" class="form-horizontal" action="<?php echo base_url();?>index.php/usuario/editarCita/<?php echo $cita->id_cita;?>" method="post" >
      <div class="form-group">
          <label for="doctor-modal" class="col-sm-2 control-label">Doctor</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="doctor-modal" name="doctor-modal" placeholder="" value="<?php echo  $cita->doctor;?>">

          </div>
        </div>
        <div class="form-group">
            <label for="hospital-modal" class="col-sm-2 control-label">Hospital</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="hospital-modal" name="hospital-modal" placeholder="" value="<?php echo  $cita->hospital;?>">

            </div>
          </div>
        <div class="form-group">
          <label for="fecha-cita-modal" class="col-sm-2 control-label">Fecha Cita</label>
          <div class="col-sm-10">
            <input type="text" class="date-pick form-control" id="fecha-cita-modal" name="fecha-cita-modal" placeholder="Fecha Cita" value="<?php echo  $cita->fecha_cita; ?>" readonly required>
          </div>
        </div>
        <div class="form-group">
          <label for="hora-cita-modal" class="col-sm-2 control-label">Hora Cita</label>
          <div class="col-sm-10">
            <input type="text" class="time-pick form-control" id="hora-cita-modal" name="hora-cita-modal" placeholder="Hora Cita" value="<?php echo  $cita->hora_cita; ?>" readonly required>
          </div>
        </div>
        <div class="form-group">
          <label for="motivo-cita-modal" class="col-sm-2 control-label">Motivo Cita</label>
          <div class="col-sm-10">
            <textarea class="form-control" id="motivo-cita-modal" name="motivo-cita-modal" rows="5" cols="40"><?php echo  $cita->motivo_cita; ?></textarea>
          </div>
        </div>
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-warning">Editar</button>
              <?php echo anchor('usuario/eliminarCita/'.$cita->id_cita,'Eliminar', array('class'=>'btn btn-danger  eliminar-cita')) ?>
            </div>
          </div>
    </form>
    <script src="<?php echo base_url();?>js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap-timepicker.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap-datepicker.es.js"></script>
    <script>
    medkeep.funciones.alerta_eliminar_cita();
    $('#fecha-cita-modal').datepicker({
      format: 'yyyy/mm/dd',
      startDate: '0',
      language: 'es',
    });
    $('#hora-cita-modal').timepicker({
      showInpunts: false,
      showMeridian: false,
    });
    </script>
