<div class="sub-banner">
  <img class="banner-img" src="<?php echo base_url(); ?>images/ingreso.jpg" alt="">
</div>
<!--Start Content-->
<div class="content">
  <div class="main-appointment-form">
    <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="main-title text-center">
          <h2>Ingrese <span>sus datos para acceder a MEDKEEP.</span></h2>
          <p>Descubra como MEDKEEP en l&iacute;nea puede hacer m&aacute;s f&aacute;cil el cuidado de su salud.</p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="icon-center">
          <div class="appoint-icon">
            <i class="icon-clipboard"></i>
          </div>
        </div>
        <div class="appointment-form">
          <div class="form">
            <section class="bgcolor-a">
              <p class="error" id="error" style="display:none;"></p>
              <p class="success" id="success" style="display:none;"></p>
              <form name="appointment_form" id="appointment_form" method="post" action="#" onSubmit="return false">
                <span class="input input--kohana">
                  <input class="input__field input__field--kohana" type="text" id="correo" name="correo" />
                  <label class="input__label input__label--kohana" for="correo">
                    <i class="icon-user6 icon icon--kohana"></i>
                    <span class="input__label-content input__label-content--kohana">Correo</span>
                  </label>
                </span>
                <span class="input input--kohana last">
                  <input class="input__field input__field--kohana" type="password" id="password" name="password"/>
                  <label class="input__label input__label--kohana" for="password">
                    <i class="icon-key icon icon--kohana"></i>
                    <span class="input__label-content input__label-content--kohana">Contraseña</span>
                  </label>
                </span>
                <input name="submit_appointment" type="submit" value="Ingresar" onClick="validateUserRegister();">
              </form>
            </section>
          </div>
        </div>
      </div>
      <div class="row">
      <div class="col-md-12">
        <div class="main-title text-center">
        <br>
          <p>M&aacute;s informaci&oacute;n en info@medkeep.mx</p>
        </div>
      </div>
    </div>
    </div>
  </div>
</div>
</div>
<!--End Content-->
<script type="text/javascript">
  medkeep.funciones.menu_usuario();
  medkeep.funciones.menu_usuario_mobile();
</script>
