<div class="sub-banner">
  <img class="banner-img" src="<?php echo base_url(); ?>images/documentos.jpg" alt="">
</div>
<br>
<div class="row">
  <div class="col-md-12" style="padding:40px 10%; text-align:center;">
    <div class="main-title">
      <h2>Historial Médico</h2>
        <p>Almacene todos sus documentos médicos para acceder y consultarlos fácilmente, desde cualquier lugar.
<br>De seguimiento a su salud de una forma muy sencilla y segura en el momento que lo necesite.
</p>
      </div>
    </div>
  </div>
<!--End Content-->
<div class="page">
<!-- page content -->
   <main class="page-content">
     <div class="grid-row">
       <!-- timetable -->
       <section class="timetable">
         <div class="widget-title" style="text-align:center;"></div>
         <div style="padding-top:100px;">
           <div class="make-appointment">
             <div class="container">
               <ul id="accordion" class="accordion">
                 <li>
                   <div class="link"><i class="fa fa-database"></i><span class="appointment-title">Guardar documento médico</span> <i class="icon-chevron-down"></i> </div>
                   <section class="bgcolor-3">
                     <p class="error" id="error" style="display:none;"></p>
                     <p class="success" id="success" style="display:none;"></p>
                     <?php $id_usuario = $this->session->userdata('id_usuario'); ?>
                     <?php if ($id_usuario == NULL): ?>
                     <?php   redirect('usuario'); ?>
                     <?php endif; ?>
                     <form name="document_form" id="document_form" method="post" action="<?php echo base_url();?>index.php/usuario/agregarDocumento/<?php echo $id_usuario?>"  method="post" enctype="multipart/form-data" >
                       <span class="input input--kohana">
                         <input class="input__field input__field--kohana" type="text" id="nombre-documento" name="nombre-documento" required/>
                         <label class="input__label input__label--kohana" for="nombre-documento">
                           <i class="icon-new-message icon icon--kohana"></i>
                           <span class="input__label-content input__label-content--kohana">Nombre</span>
                         </label>
                       </span>
                       <span class="input input--kohana">
                           <br>
                           <select class="form-control" id="sel-tipo" name="sel-tipo" required>
                            <option value="">Seleccione Tipo Documento</option>
                            <?php foreach ($tipos_documento as $t) { ?>
                              <option value="<?php echo $t->id;?>"><?php echo $t->tipo;?></option>
                            <?php }?>
                          </select>
                         </span>
                         <span class="input input--kohana last">
                           <input class="input__field input__field--kohana" type="text" id="doctor" name="doctor" required/>
                           <label class="input__label input__label--kohana" for="doctor">
                             <i class="icon-new-message icon icon--kohana"></i>
                             <span class="input__label-content input__label-content--kohana">Doctor</span>
                           </label>
                         </span>
                         <span class="input input--kohana">
                           <input class="input__field input__field--kohana" type="text" id="hospital" name="hospital" required/>
                           <label class="input__label input__label--kohana" for="hospital">
                             <i class="icon-new-message icon icon--kohana"></i>
                             <span class="input__label-content input__label-content--kohana">Hospital</span>
                           </label>

                         </span>
                       <span class="input input--kohana">
                         <input class="input__field input__field--kohana" type="text" id="descripcion" name="descripcion" required/>
                         <label class="input__label input__label--kohana" for="descripcion">
                           <i class="icon-new-message icon icon--kohana"></i>
                           <span class="input__label-content input__label-content--kohana">Descripcion</span>
                         </label>
                       </span>


                       <span class="input input--kohana ">
                         <input  class="input__field input__field--kohana" type="file" name="upl" required/>
                         <label class="input__label input__label--kohana" for="upl">
                           <i class="icon-new-message icon icon--kohana"></i>
                           <span class="input__label-content input__label-content--kohana"></span>
                         </label>
                     <label for="" style="color: #fff;">Tamaño máximo 3 MB<br>Formatos permitidos (JPG,JPEG, GIF, PDF y PNG)</label>

                     </span>
                  <br>
                     <input name="submit" type="submit" value="Guardar" onClick="">
                   </form>
                 </section>
               </li>
             </ul>
           </div>
         </div>
       </div>
     </section>
     <!--/ timetable -->
   </div>
 </main>
</div>
<!--Start Content-->
<div class="content">
  <div style="padding: 100px 0px 100px 0px;">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="">
            <div class="">
              <section>
                <!--Notificaciones-->
                <?php if ($success != '') { ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong><?php echo $success ?></strong>
                </div>
                <?php $this->session->set_userdata('success', '');} ?>

                <?php if ($danger != '') { ?>
                  <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong><?php echo $danger ?></strong>
                  </div>
                <?php $this->session->set_userdata('danger', '');} ?>
                <!---->

                <table class="table">
                  <thead>
                    <tr>
                      <th>Nombre</th>
                      <th>Descripcion</th>
                      <th>Doctor</th>
                      <th>Fecha</th>
                      <th>Archivo</th>
                          <th>Opciones</th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php if ($lista_documentos): ?>
                        <?php foreach ($lista_documentos as $fila): ?>
                          <tr>
                            <td class="event">
                              <div class="event">
                              <div><?php echo $fila->nombre_documento; ?></div>
                              </div>
                            </td>
                            <td class="event">
                              <div class="event">
                              <div><?php echo $fila->descripcion; ?></div>
                              </div>
                            </td>
                            <td class="event">
                              <div class="event">
                                <div><?php echo $fila->doctor; ?></div>
                              </div>
                            </td>
                            <td class="event">
                              <div class="event">
                                <div><?php echo $fila->fecha_creado; ?></div>
                              </div>
                            </td>
                            <td>
                              <div class="event">
                                <a target="_blank" href="<?php echo base_url(); ?><?php echo $fila->ruta; ?>"><?php echo $fila->nombre_archivo; ?></a>
                              </div>
                            </td>
                            <td>
                                <?php echo anchor('usuario/eliminarDocumento/'.$fila->id,'Eliminar', array('class'=>'btn btn-danger  btn-xs eliminar-documento')) ?>
                            </td>
                          </tr>
                        <?php endforeach; ?>
                      <?php endif; ?>
                    </tbody>
                </table>

            </section>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--End Content-->


 <div class="content">
   <div class="row">
     <div class="col-md-12" style="padding: 0 0 50px 0;">
       <div class="col-md-6">
         <a href="<?php echo site_url('usuario/cita') ?>">
           <div style=" padding-bottom:10px;">
           <img class="img-rounded" src="<?php echo base_url(); ?>images/slides/cita.jpg" alt="" >
         </div>
       </a>
       </div>
       <div class="col-md-6">
         <a href="<?php echo site_url('usuario/tratamiento') ?>">
           <div style=" padding-bottom:10px;">
             <img class="img-rounded" src="<?php echo base_url(); ?>images/slides/tratamiento.jpg" alt="" >
           </div>
         </a>
       </div>

     </div>
   </div>
</div>
<script type="text/javascript">
  medkeep.funciones.menu_usuario();
  medkeep.funciones.menu_usuario_documento();
  medkeep.funciones.menu_usuario_mobile();
  medkeep.funciones.menu_usuario_documento_mobile();
  medkeep.funciones.alerta_eliminar_documento();
</script>
