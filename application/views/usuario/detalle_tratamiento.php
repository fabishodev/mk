<form id="form-tratameinto" class="form-horizontal" action="<?php echo base_url();?>index.php/usuario/editarTratamiento/<?php echo $tratamiento->id;?>" method="post" >
  <div class="form-group">
      <label for="medicamento-modal" class="col-sm-2 control-label">Tratamiento</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="medicamento-modal" name="medicamento-modal" placeholder="" value="<?php echo  $tratamiento->medicamento;?>">
      </div>
    </div>
        <div class="form-group">
          <label for="fecha-inicio-tratamiento-modal" class="col-sm-2 control-label">Fecha Inicio</label>
          <div class="col-sm-10">
            <input type="text" class="date-pick form-control" id="fecha-inicio-tratamiento-modal" name="fecha-inicio-tratamiento-modal" placeholder="Fecha inicio" value="<?php echo  $tratamiento->fecha_inicio; ?>" readonly required>
          </div>
        </div>
        <div class="form-group">
          <label for="fecha-fin-tratamiento-modal" class="col-sm-2 control-label">Fecha Fin</label>
          <div class="col-sm-10">
            <input type="text" class="date-pick form-control" id="fecha-fin-tratamiento-modal" name="fecha-fin-tratamiento-modal" placeholder="Fecha fin" value="<?php echo  $tratamiento->fecha_fin; ?>" readonly required>
          </div>
        </div>
        <div class="form-group">
          <label for="hora-tratamiento-modal" class="col-sm-2 control-label">Hora Inicio</label>
          <div class="col-sm-10">
            <input type="text" class="time-pick form-control" id="hora-tratamiento-modal" name="hora-tratamiento-modal" placeholder="" value="<?php echo  $tratamiento->hora; ?>" readonly required>
          </div>
        </div>
        <div class="form-group">
          <label for="hora-fin-tratamiento-modal" class="col-sm-2 control-label">Hora Fin</label>
          <div class="col-sm-10">
            <input type="text" class="time-pick form-control" id="hora-fin-tratamiento-modal" name="hora-fin-tratamiento-modal" placeholder="" value="<?php echo  $tratamiento->hora_fin; ?>" readonly required>
          </div>
        </div>
        <div class="form-group">
            <label for="lapso-modal" class="col-sm-2 control-label">Tomar cada...</label>
            <div class="col-sm-10">
              <input type="number" min="1" max="24" class="form-control" id="lapso-modal" name="lapso-modal" placeholder="" value="<?php echo  $tratamiento->lapso;?>">
            </div>
          </div>
        <div class="form-group">
          <label for="observacion-modal" class="col-sm-2 control-label">Observación</label>
          <div class="col-sm-10">
            <textarea class="form-control" id="observacion-modal" name="observacion-modal" rows="5" cols="40"><?php echo  $tratamiento->observaciones; ?></textarea>
          </div>
        </div>
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-warning">Editar</button>
              <?php echo anchor('usuario/eliminarTratamiento/'.$tratamiento->id,'Eliminar', array('class'=>'btn btn-danger  eliminar-tratamiento')) ?>
            </div>
          </div>
    </form>
    <script src="<?php echo base_url();?>js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap-timepicker.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap-datepicker.es.js"></script>
    <script>
    medkeep.funciones.alerta_eliminar_tratamiento();
    $('#fecha-inicio-tratamiento-modal').datepicker({
      format: 'yyyy/mm/dd',
      startDate: '0',
      language: 'es',
    });
    $('#fecha-fin-tratamiento-modal').datepicker({
      format: 'yyyy/mm/dd',
      startDate: '0',
      language: 'es',
    });
    $('#hora-tratamiento-modal').timepicker({
      showInpunts: false,
      showMeridian: false,
    });
    $('#hora-fin-tratamiento-modal').timepicker({
      showInpunts: false,
      showMeridian: false,
    });
    </script>
