<!--Start Footer-->
<footer class="footer" id="footer">
   <div class="container">
     <div class="main-footer">
       <div class="row">
         <div class="col-md-12">
           <div class="get-touch">
             <div class="title">
               <div class="col-md-4">
                 <h6 style="color:#fff; text-align:left;"><span>Medkeep Contacto</span></h6>
               </div>
               <div class="col-md-4">
                 <!--<h6 style="color:#fff; text-align:center;"><i class="icon-phone4"></i> <span>477-777-7777</span></h6>-->
               </div>
               <div class="col-md-4">
                 <h6 style="color:#fff; text-align:right;"><i class="icon-dollar"></i> <span>info@medkeep.mx</span></h6>
               </div>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
   <div class="footer-bottom">
     <div class="container">
       <div class="row">
         <div class="col-md-6">
           <span class="copyrights">Copyright &copy; 2016 Medkeep. Todos los derechos reservados.</span>
         </div>
         <div class="col-md-6">
           <div class="social-icons">
             <a href="#." class="fb"><i class="icon-euro"></i></a>
             <a href="#." class="tw"><i class="icon-yen"></i></a>
             <a href="#." class="gp"><i class="icon-google-plus"></i></a>
           </div>
         </div>
       </div>
     </div>
   </div>
 </footer>
<!--End Footer-->
<a href="#0" class="cd-top"></a>
</div>


<!-- SMOOTH SCROLL -->
<script type="text/javascript" src="<?php echo base_url(); ?>js/scroll-desktop.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/scroll-desktop-smooth.js"></script>

<!-- START REVOLUTION SLIDER -->
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.themepunch.tools.min.js"></script>


<!-- Date Picker and input hover -->
<script type="text/javascript" src="<?php echo base_url(); ?>js/classie.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui-1.10.3.custom.js"></script>

<!-- Fun Facts Counter -->
<script type="text/javascript" src="<?php echo base_url(); ?>js/counter.js"></script>


<!-- Welcome Tabs -->
<script type="text/javascript" src="<?php echo base_url(); ?>js/tabs.js"></script>


<!-- All Carousel -->
<script type="text/javascript" src="<?php echo base_url(); ?>js/owl.carousel.js"></script>

<!-- Mobile Menu -->
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.mmenu.min.all.js"></script>

<!-- All Scripts -->
<script type="text/javascript" src="<?php echo base_url(); ?>js/custom.js"></script>

<!-- Revolution Slider -->
<script type="text/javascript">
jQuery('.tp-banner').show().revolution({
    dottedOverlay:"none",
    delay:16000,
    startwidth:1170,
    startheight:520,
    hideThumbs:200,

    thumbWidth:100,
    thumbHeight:50,
    thumbAmount:5,

    navigationType:"nexttobullets",
    navigationArrows:"solo",
    navigationStyle:"preview",

    touchenabled:"on",
    onHoverStop:"on",

    swipe_velocity: 0.7,
    swipe_min_touches: 1,
    swipe_max_touches: 1,
    drag_block_vertical: false,

    parallax:"mouse",
    parallaxBgFreeze:"on",
    parallaxLevels:[7,4,3,2,5,4,3,2,1,0],

    keyboardNavigation:"off",

    navigationHAlign:"center",
    navigationVAlign:"bottom",
    navigationHOffset:0,
    navigationVOffset:20,

    soloArrowLeftHalign:"left",
    soloArrowLeftValign:"center",
    soloArrowLeftHOffset:20,
    soloArrowLeftVOffset:0,

    soloArrowRightHalign:"right",
    soloArrowRightValign:"center",
    soloArrowRightHOffset:20,
    soloArrowRightVOffset:0,

    shadow:0,
    fullWidth:"on",
    fullScreen:"off",

    spinner:"spinner4",

    stopLoop:"off",
    stopAfterLoops:-1,
    stopAtSlide:-1,

    shuffle:"off",

    autoHeight:"off",
    forceFullWidth:"off",

    hideThumbsOnMobile:"off",
    hideNavDelayOnMobile:1500,
    hideBulletsOnMobile:"off",
    hideArrowsOnMobile:"off",
    hideThumbsUnderResolution:0,

    hideSliderAtLimit:0,
    hideCaptionAtLimit:0,
    hideAllCaptionAtLilmit:0,
    startWithSlide:0,
    videoJsPath:"<?php echo base_url(); ?>rs-plugin/videojs/",
    fullScreenOffsetContainer: ""
});
</script><!-- Revolution Slider -->
<script type="text/javascript">
    jQuery('.tp-banner').show().revolution({
    dottedOverlay:"none",
    delay:16000,
    startwidth:1170,
    startheight:520,
    hideThumbs:200,

    thumbWidth:100,
    thumbHeight:50,
    thumbAmount:5,

    navigationType:"nexttobullets",
    navigationArrows:"solo",
    navigationStyle:"preview",

    touchenabled:"on",
    onHoverStop:"on",

    swipe_velocity: 0.7,
    swipe_min_touches: 1,
    swipe_max_touches: 1,
    drag_block_vertical: false,

    parallax:"mouse",
    parallaxBgFreeze:"on",
    parallaxLevels:[7,4,3,2,5,4,3,2,1,0],

    keyboardNavigation:"off",

    navigationHAlign:"center",
    navigationVAlign:"bottom",
    navigationHOffset:0,
    navigationVOffset:20,

    soloArrowLeftHalign:"left",
    soloArrowLeftValign:"center",
    soloArrowLeftHOffset:20,
    soloArrowLeftVOffset:0,

    soloArrowRightHalign:"right",
    soloArrowRightValign:"center",
    soloArrowRightHOffset:20,
    soloArrowRightVOffset:0,

    shadow:0,
    fullWidth:"on",
    fullScreen:"off",

    spinner:"spinner4",

    stopLoop:"off",
    stopAfterLoops:-1,
    stopAtSlide:-1,

    shuffle:"off",

    autoHeight:"off",
    forceFullWidth:"off",

    hideThumbsOnMobile:"off",
    hideNavDelayOnMobile:1500,
    hideBulletsOnMobile:"off",
    hideArrowsOnMobile:"off",
    hideThumbsUnderResolution:0,

    hideSliderAtLimit:0,
    hideCaptionAtLimit:0,
    hideAllCaptionAtLilmit:0,
    startWithSlide:0,
    videoJsPath:"rs-plugin/videojs/",
    fullScreenOffsetContainer: ""
});
</script>
</body>
</html>
<!-- Localized -->
