<!DOCTYPE html>
<html>
  <head>
    <title>Medkeep</title>
    <meta name="keywords" content="medkeep, citas medicas, historial medico, tratamientos, consultas, doctor, hospital, asistente medico en linea">
    <meta name="description" content="Medkeep: Su asistente medico en linea.">
    <meta name="author" content="Fabricio Magaña Ruiz, indiesoftmx@gmail.com">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <link rel="icon" type="image/png" href="<?php echo base_url(); ?>images/favicon-medical.png">
  <!--main file-->
	<link href="<?php echo base_url(); ?>css/medical-guide.css" rel="stylesheet" type="text/css">
  <!--Medical Guide Icons-->
	<link href="<?php echo base_url(); ?>fonts/medical-guide-icons.css" rel="stylesheet" type="text/css">
  <!-- Default Color-->
	<link href="<?php echo base_url(); ?>css/default-color.css" rel="stylesheet" id="color"  type="text/css">
  <!--bootstrap-->
	<link href="<?php echo base_url(); ?>css/bootstrap.css" rel="stylesheet" type="text/css">
  <!--Dropmenu-->
	<link href="<?php echo base_url(); ?>css/dropmenu.css" rel="stylesheet" type="text/css">
  <!--Sticky Header-->
	<link href="<?php echo base_url(); ?>css/sticky-header.css" rel="stylesheet" type="text/css">
  <!--revolution-->
	<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>css/settings.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>css/extralayers.css" rel="stylesheet" type="text/css">
  <!--Accordion-->
  <link href="<?php echo base_url(); ?>css/accordion.css" rel="stylesheet" type="text/css">
  <!--tabs-->
	<link href="<?php echo base_url(); ?>css/tabs.css" rel="stylesheet" type="text/css">
  <!--Owl Carousel-->
	<link href="<?php echo base_url(); ?>css/owl.carousel.css" rel="stylesheet" type="text/css">
  <!-- Mobile Menu -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/jquery.mmenu.all.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/demo.css" />
  <!--PreLoader-->
	<link href="<?php echo base_url(); ?>css/loader.css" rel="stylesheet" type="text/css">
  <!--switcher-->
	<link href="<?php echo base_url(); ?>css/switcher.css" rel="stylesheet" type="text/css">
  <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
  <script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
  <?php if (isset($scripts)): ?>
    <?php foreach ($scripts as $js): ?>
        <script src="<?php echo base_url()."js/custom/{$js}.js";?>" type="text/javascript"></script>
    <?php endforeach; ?>
  <?php endif; ?>
</head>
  <body>
    <div id="wrap">
      <!--Start PreLoader-->
      <div id="preloader">
        <div id="status">&nbsp;</div>
        <div class="loader">
          <h1>Cargando...</h1>
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>
      <!--End PreLoader-->
      <div id="header-2" style="display:none">
        <header class="header header2">
          <div class="container">
            <div class="row">
              <div class="col-md-3">
                <a href="<?php echo site_url('home'); ?>" class="logo"><img src="<?php echo base_url(); ?>images/logo-medkeep.png" alt=""></a>
              </div>
              <div class="col-md-9">
                <nav class="menu-2">
                  <ul class="nav wtf-menu">
                    <li class="parent"><a href="<?php echo site_url('home') ?>">Inicio</a></li>
                    <li class="parent"><a href="<?php echo site_url('servicios') ?>">Servicios</a></li>
                    <li class="parent"><a href="<?php echo site_url('registro') ?>">Registro</a></li>
                    <li class="parent"><a href="<?php echo site_url('blog') ?>">Blog</a></li>
                    <li class="parent"><a href="<?php echo site_url('contacto') ?>">Contáctanos</a></li>
                    <?php if ($id_usuario != NULL): ?>
                      <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Usuario<span class="caret"></span></a>
                        <ul class="submenu">
                          <li class="parent"><a href="<?php echo site_url('usuario/perfil') ?>">Perfil</a></li>
                          <li class="parent"><a href="<?php echo site_url('usuario/cita') ?>">Agenda una Cita</a></li>
                          <li class="parent"><a href="<?php echo site_url('usuario/tratamiento') ?>">Programa un Tratamiento</a></li>
                          <li class="parent"><a href="<?php echo site_url('usuario/documentos') ?>">Historial médico</a></li>
                          <?php if ($cod_tipo_usuario == 1): ?>
                              <li id="suscribirse" class="parent"><a href="<?php echo site_url('usuario/suscribirse') ?>">Suscribirse</a></li>
                          <?php endif; ?>
                          <li role="separator" class="divider"></li>
                          <li class="parent"><a href="<?php echo site_url('usuario/salir') ?>">Salir</a></li>
                        </ul>
                      </li>
                      <?php else: ?>
                        <li id="usuario" class="parent"><a href="<?php echo site_url('usuario') ?>">Usuarios Registrados</a></li>
                    <?php endif; ?>

                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </header>
      </div>
      <!--Start Header-->
      <div id="header-1">
        <!--Start Top Bar-->
        <div class="top-bar">
          <div class="container">
            <div class="row">
              <div class="col-md-5">
                <span>Su asistente medico en linea.</span>
              </div>
              <div class="col-md-7">
                <div class="get-touch">
                  <ul>
                    <!--<li><a><i class="icon-phone4"></i> 477-777-7777</a></li>-->
                    <li><a href="#."><i class="icon-mail"></i> info@medkeep.mx</a></li>
                  </ul>
                  <ul class="social-icons">
                    <li><a href="#." class="fb"><i class="icon-euro"></i> </a></li>
                    <li><a href="#." class="tw"><i class="icon-yen"></i> </a></li>
                    <li><a href="#." class="gp"><i class="icon-caddieshoppingstreamline"></i> </a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--Top Bar End-->
        <!--Start Header-->
        <?php $id_usuario = $this->session->userdata('id_usuario'); ?>
        <?php $cod_tipo_usuario = $this->session->userdata('cod_tipo_usuario'); ?>
        <header class="header">
          <div class="container">
            <div class="row">
              <div class="col-md-3">
                <a href="<?php echo site_url('home'); ?>" class="logo"><img src="<?php echo base_url(); ?>images/logo-medkeep.png" alt=""></a>
              </div>
              <div class="col-md-9">
                <nav class="menu-2">
                  <ul class="nav wtf-menu">
                    <li id="home" class="parent"><a href="<?php echo site_url('home') ?>">Inicio</a></li>
                    <li id="servicios" class="parent"><a href="<?php echo site_url('servicios') ?>">Servicios</a></li>
                    <li id="registro" class="parent"><a href="<?php echo site_url('registro') ?>">Registro</a></li>
                    <li id="blog" class="parent"><a href="<?php echo site_url('blog') ?>">Blog</a></li>
                    <li id="contacto" class="parent"><a href="<?php echo site_url('contacto') ?>">Contáctanos</a></li>
                    <?php if ($id_usuario != NULL): ?>
                      <li id="usuario">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Usuario<span class="caret"></span></a>
                        <ul class="submenu">
                          <li id="perfil" class="parent"><a href="<?php echo site_url('usuario/perfil') ?>">Perfil</a></li>
                          <li id="cita" class="parent"><a href="<?php echo site_url('usuario/cita') ?>">Agenda una Cita</a></li>
                          <li id="tratamiento" class="parent"><a href="<?php echo site_url('usuario/tratamiento') ?>">Programa un Tratamiento</a></li>
                          <li id="documento" class="parent"><a href="<?php echo site_url('usuario/documentos') ?>">Historial médico</a></li>
                          <?php if ($cod_tipo_usuario == 1): ?>
                              <li id="suscribirse" class="parent"><a href="<?php echo site_url('usuario/suscribirse') ?>">Suscribirse</a></li>
                          <?php endif; ?>
                          <li role="separator" class="divider"></li>
                          <li class="parent"><a href="<?php echo site_url('usuario/salir') ?>">Salir</a></li>
                        </ul>
                      </li>
                      <?php else: ?>
                        <li id="usuario" class="parent"><a href="<?php echo site_url('usuario') ?>">Usuarios Registrados</a></li>
                    <?php endif; ?>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </header>
      </div>
      <!--End Header-->
      <!-- Mobile Menu Start -->
      <div class="container">
        <div id="page">
          <header class="header">
            <a href="#menu"></a>
          </header>
          <nav id="menu">
            <ul>
              <li id="home-mobile"><a href="<?php echo site_url('home') ?>">Inicio</a></li>
              <li id="servicios-mobile"><a href="<?php echo site_url('servicios') ?>">Servicios</a></li>
              <li id="registro-mobile"><a href="<?php echo site_url('registro') ?>">Registro</a></li>
              <li id="blog-mobile"><a href="<?php echo site_url('blog') ?>">Blog</a></li>
              <li id="contacto-mobile"><a href="<?php echo site_url('contacto') ?>">Contáctanos</a></li>
              <?php if ($id_usuario != NULL): ?>
              <li id="usuario">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Usuario</a>
                  <ul class="submenu">
                    <li id="perfil-mobile" class="parent"><a href="<?php echo site_url('usuario/perfil') ?>">Perfil</a></li>
                    <li id="cita-mobile" class="parent"><a href="<?php echo site_url('usuario/cita') ?>">Agenda una Cita</a></li>
                    <li id="tratamiento-mobile" class="parent"><a href="<?php echo site_url('usuario/tratamiento') ?>">Programa un Tratamiento</a></li>
                    <li id="documento-mobile" class="parent"><a href="<?php echo site_url('usuario/documentos') ?>">Historial médico</a></li>
                    <?php if ($cod_tipo_usuario == 1): ?>
                        <li id="suscribirse-mobile" class="parent"><a href="<?php echo site_url('usuario/suscribirse') ?>">Suscribirse</a></li>
                    <?php endif; ?>
                    <li role="separator" class="divider"></li>
                    <li class="parent"><a href="<?php echo site_url('usuario/salir') ?>">Salir</a></li>
                  </ul>
                </li>
                <?php else: ?>
                  <li id="usuario" class="parent"><a href="<?php echo site_url('usuario') ?>">Usuarios Registrados</a></li>
              <?php endif; ?>
            </ul>
          </nav>
        </div>
      </div>
      <!-- Mobile Menu End -->
