<div class="sub-banner">
  <img class="banner-img" src="<?php echo base_url(); ?>images/servicios.jpeg" alt="">
</div>
<!--Start Content-->
<div class="content">




<!--Start Welcome-->
 <div class="welcome">
  <div class="container">

    <div class="row">
      <div class="col-md-12">
        <div class="main-title text-center">
        <h2>Conoce nuestros servicios</h2>
          <p></p>
        </div>
      </div>
    </div>

 <div class="hospital">
<div class="container">



  <div class="row">
  <div class="col-md-6">

   <div id="hospital">


   <div class="span12">
   <div id="services-slide" class="owl-carousel">

   <div class="item"><img src="<?php echo base_url(); ?>images/app_desktop.jpg" alt=""></div>
   <div class="item"><img src="<?php echo base_url(); ?>images/app_mobile.jpg" alt=""></div>

   </div>
   </div>


   </div>

  </div>

  <div class="col-md-6">
  <div class="why-choose-us">


   <ul id="why-choose" class="why-choose">
<li class="open">
 <div class="link"><i class="icon-first-aid-box"></i>Historial médico<i class="icon-chevron-down"></i></div>
 <ul class="submenu" style=" display:block;">
   <li>
 <p align="justify">Facilite el manejo de su información médica, registre citas, eventos y cualquier información referente a su salud.
<br/><br/>
Acceda a su información fácilmente en cualquier momento o lugar, permitiéndole tener su información disponible y segura. </p>
 </li>
 </ul>
</li>

<li>
 <div class="link"><i class="icon-heart-beat"></i>Citas médicas<i class="icon-chevron-down"></i></div>
 <ul class="submenu">
   <li>
 <p align="justify">Mejoré el manejo y el control de su calendario de citas médicas. Cada vez que agende sus futuras citas, MEDKEEP hará que no las olvide con alertas o recordatorios automáticos que le permitirá recordar en todo momento su cita, horario, doctor y hospital.

<br/><br/>
Descubra más y agende una cita gratis.</p>
 </li>
 </ul>
</li>
<li>
 <div class="link"><i class="icon-Medicine-bottle"></i>Tratamientos médicos<i class="fa icon-chevron-down"></i></div>
 <ul class="submenu">
   <li>
 <p align="justify">Mejoré el control del consumo de medicamentos y la programación de sus tratamientos médicos. Cada vez que programe un tratamiento, MEDKEEP hará que no olvide su seguimiento con alertas o recordatorios automáticos que le permitirá recordar en todo momento sus tratamientos.
<br/><br/>
Descubra más y programe un tratamiento de prueba. </p>
 </li>
 </ul>
</li>

<li>
 <div class="link"><i class="icon-mobile2"></i>Notificaciones y alertas<i class="fa icon-chevron-down"></i></div>
 <ul class="submenu">
   <li>
 <p align="justify">MEDKEEP facilita el manejo de su historial médico, agenda y tratamientos. A través de recordatorios, alertas y notificaciones.
<br/><br/>
Descubra todo lo que nuestra herramienta en línea puede hacer por usted. </p>
 </li>
 </ul>
</li>

<li>
 <div class="link"><i class="icon-tablet6"></i>Referencias médicas<i class="fa icon-chevron-down"></i></div>
 <ul class="submenu">
   <li>
 <p align="justify">MEDKEEP facilita la ubicación de servicios médicos a través de un directorio electrónico de referencias médicas, así podrá ubicar de una manera rápida y fácil aquellos servicios médicos que son de su interés.

<br/><br/>
Más información en info@medkeep.mx. </p>
 </li>
 </ul>
</li>

</ul>
  </div>
  </div>

 </div>
</div>
</div>


  </div>
</div>
<!--End Welcome-->


<!--Start Services-->

<!--End Services-->





</div>
<!--End Content-->
<script type="text/javascript">
  medkeep.funciones.menu_servicios();
  medkeep.funciones.menu_servicios_mobile();
</script>
