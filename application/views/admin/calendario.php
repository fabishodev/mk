

      <link href="<?php echo base_url();?>vendors/bootstrap-calendar/css/calendar.min.css" rel="stylesheet">
      <div class="col-md-10">
        <div class="row">
          <div class="col-md-12">
    <div id="calendar"></div>
    </div>
    </div>
    </div>



    <script type="text/javascript" src="<?php echo base_url();?>vendors/underscore/underscore-min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>vendors/bootstrap-calendar/js/calendar.js"></script>
      <script type="text/javascript" src="<?php echo base_url();?>vendors/bootstrap-calendar/js/language/es-MX.js"></script>
    <script type="text/javascript">
        var calendar = $("#calendar").calendar(
            {
                //events_source: function () { return [];},
                //events_source: '<?php echo base_url() ?>events/getAll',
			          language: 'es-MX',
                tmpl_path: "<?php echo base_url();?>vendors/bootstrap-calendar/tmpls/",
            });
    </script>
