<?php $cod_tipo_usuario = $this->session->userdata('cod_tipo_usuario'); ?>
<?php if ($cod_tipo_usuario != 3): ?>
<?php   redirect('admin/login'); ?>
<?php endif; ?>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Agregar Nueva Categoria</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" action="<?php echo base_url(); ?>index.php/admin/agregarCategoria" method="post">
          <div class="form-group">
            <label for="categoria" class="col-sm-2 control-label">Categoría</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="categoria" name="categoria" placeholder="Categoria" value="" required>
            </div>
          </div>
          <div class="form-group">
            <label for="descripcion-categoria" class="col-sm-2 control-label">Descripción Categoria</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="descripcion-categoria" name="descripcion-categoria" placeholder="Descripcion categoria" value="" required>
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
          </div>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

      </div>
    </div>
  </div>
</div><!-- /.modal -->
<div class="col-md-10">
  <div class="row">
    <div class="col-md-6">
      <!--Notificaciones-->
      <?php if ($success != '') { ?>
        <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <strong><?php echo $success ?></strong>
      </div>
      <?php $this->session->set_userdata('success', '');} ?>

      <?php if ($danger != '') { ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <strong><?php echo $danger ?></strong>
        </div>
      <?php $this->session->set_userdata('danger', '');} ?>
      <!---->
    </div>
  </div>
  <div class="content-box-header panel-heading">
    <div class="panel-title ">Lista Noticias</div>

  <div class="panel-options">
    <a href="<?php echo site_url('admin/nuevaNoticia') ?>" data-rel="collapse"><i class="glyphicon glyphicon-plus"></i> Nueva Noticia</a>

  </div>
  </div>
  <div class="content-box-large box-with-header">
      <table cellpadding="0" cellspacing="0" border="0" class="table table-striped" id="example">
      <thead>
        <tr>
          <th>Id</th>
          <th>Titulo</th>
          <th>Resumen</th>
          <th>Categoria</th>
          <th>Publicada</th>
          <th>Fecha Creado</th>
          <th>Opciones</th>
        </tr>
      </thead>
      <tbody>
        <?php if ($lista_noticias  !== FALSE): ?>
          <?php foreach ($lista_noticias as $fila): ?>
            <tr>
              <td>  <?php echo  $fila->id; ?></td>
              <td>  <?php echo  $fila->titulo_noticia; ?></td>
              <td><?php echo  $fila->noticia_corta; ?></td>
              <td><?php echo  $fila->categoria; ?></td>
              <td>
                <?php if ( $fila->publicada  == 1){ ?>
                  <span class="label label-success"> Publicada </span>
                  <?php }else { ?>
                    <span class="label label-danger"> No Publicada</span>
                    <?php }?>
              </td>
              <td><?php echo  $fila->fecha_creado; ?></td>
              <td>
                <?php echo anchor('admin/detalleNoticia/'.$fila->id,'Detalle', array('class'=>'btn btn-info  btn-xs')) ?>
              </td>
            </tr>
          <?php endforeach; ?>
        <?php endif; ?>
        </tbody>
    </table>
    </div>
    <div class="row">
      <div class="col-md-8">
        <div class="content-box-header">
          <div class="panel-title">Lista Categorias</div>

        <div class="panel-options">
          <a href="#" data-toggle="modal" data-target="#myModal" data-rel="collapse"><i class="glyphicon glyphicon-plus"></i> Nueva Categoría </a>

        </div>
        </div>
        <div class="content-box-large box-with-header">
          <table cellpadding="0" cellspacing="0" border="0" class="table table-striped data-table" id="">
          <thead>
            <tr>
              <th>Id</th>
              <th>Categoria</th>
              <th>Descripcion</th>
              <th>Estatus</th>
              <th>Opciones</th>
            </tr>
          </thead>
          <tbody>
            <?php if ($lista_categorias  !== FALSE): ?>
              <?php foreach ($lista_categorias as $fila): ?>
                <tr>
                  <td>  <?php echo  $fila->id; ?></td>
                  <td>  <?php echo  $fila->categoria; ?></td>
                  <td><?php echo  $fila->descripcion; ?></td>
                  <td>
                    <?php if ( $fila->activo  == 1){ ?>
                      <span class="label label-success"> Activa </span>
                      <?php }else { ?>
                        <span class="label label-danger"> No Activa</span>
                    <?php }?>
                  </td>
                  <td>
                    <?php if ( $fila->activo  == 1){ ?>
                      <?php echo anchor('admin/cambiarEstatusCategoria/'.$fila->id,'Desactivar', array('class'=>'btn btn-danger  btn-xs')) ?>
                      <?php }else { ?>
                        <?php echo anchor('admin/cambiarEstatusCategoria/'.$fila->id,'Activar', array('class'=>'btn btn-success  btn-xs')) ?>
                    <?php }?>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php endif; ?>
            </tbody>
        </table>
      </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8">
        <div class="content-box-header">
          <div class="panel-title">Lista Comentarios</div>


        </div>
        <div class="content-box-large box-with-header">
          <table cellpadding="0" cellspacing="0" border="0" class="table table-striped data-table" id="">
          <thead>
            <tr>
              <th>Id</th>
              <th>Usuario</th>
              <th>Noticia</th>
              <th>Comentario</th>
              <th>Estatus</th>
              <th>Fecha</th>
              <th>Opciones</th>
            </tr>
          </thead>
          <tbody>
            <?php if ($lista_comentarios  !== FALSE): ?>
              <?php foreach ($lista_comentarios as $fila): ?>
                <tr>
                  <td><?php echo  $fila->id; ?></td>
                    <td><?php echo  $fila->nombre.' '.$fila->ape_paterno.' '.$fila->ape_materno; ?></td>
                  <td><?php echo  $fila->titulo_noticia; ?></td>
                  <td><?php echo  $fila->comentario; ?></td>
                  <td>
                    <?php if ( $fila->publicado  == 1){ ?>
                      <span class="label label-success"> Publicado </span>
                      <?php }else { ?>
                        <span class="label label-danger"> No Publicado</span>
                    <?php }?>
                  </td>
                    <td><?php echo  $fila->fecha_comentario; ?></td>
                  <td>
                    <?php if ( $fila->publicado  == 1){ ?>
                      <?php echo anchor('admin/cambiarEstatusComentario/'.$fila->id,'No Publicar', array('class'=>'btn btn-danger  btn-xs')) ?>
                      <?php }else { ?>
                        <?php echo anchor('admin/cambiarEstatusComentario/'.$fila->id,'Publicar', array('class'=>'btn btn-success  btn-xs')) ?>
                    <?php }?>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php endif; ?>
            </tbody>
        </table>
      </div>
      </div>
    </div>
  </div>

<link href="<?php echo base_url(); ?>vendors/datatables/dataTables.bootstrap.css" rel="stylesheet" media="screen">
<!-- jQuery UI -->
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>vendors/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>vendors/datatables/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url(); ?>js/tables.js"></script>
