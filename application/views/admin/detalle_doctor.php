<?php $cod_tipo_usuario = $this->session->userdata('cod_tipo_usuario'); ?>
<?php if ($cod_tipo_usuario != 3): ?>
<?php   redirect('admin/login'); ?>
<?php endif; ?>
<div class="col-md-10">
  <div class="row">
    <div class="col-md-12">
      <div class="content-box-large">
        <div class="panel-heading">
          <div class="panel-title">Detalle Doctor</div>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-6">
              <!--Notificaciones-->
              <?php if ($success != '') { ?>
                <div class="alert alert-success alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <strong><?php echo $success ?></strong>
              </div>
              <?php $this->session->set_userdata('success', '');} ?>

              <?php if ($danger != '') { ?>
                <div class="alert alert-danger alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <strong><?php echo $danger ?></strong>
                </div>
              <?php $this->session->set_userdata('danger', '');} ?>
              <!---->
            </div>
          </div>
        <?php if ($usuario): ?>
          <form id="form-user" class="form-horizontal" action="<?php echo base_url();?>index.php/admin/editarDoctor/<?php echo $usuario->id;?>" method="post" >
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                <label for="estatus" class="col-sm-2 control-label">Estatus</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="estatus" name="estatus" required>
                      <?php if ($usuario->activo == '1'): ?>
                        <option selected value="1">Activo</option>
                        <option value="0">No Activo</option>
                        <?php else: ?>
                          <option value="1">Activo</option>
                          <option selected value="0">No Activo</option>
                        <?php endif; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="nombre" class="col-sm-2 control-label">Nombre</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="Nombre" name="nombre" placeholder="Nombre" value="<?php echo $usuario->nombre ?>" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="ape-paterno" class="col-sm-2 control-label">Apellido Paterno</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="ape-paterno" name="ape-paterno" placeholder="Apellido Paterno" value="<?php echo $usuario->ape_paterno ?>" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="ape-aterno" class="col-sm-2 control-label">Apellido Materno</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="ape-materno" name="ape-materno" placeholder="Apellido Materno" value="<?php echo $usuario->ape_materno ?>" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="correo" class="col-sm-2 control-label">Correo</label>
                  <div class="col-sm-10">
                    <input type="email" class="form-control" id="correo" name="correo" placeholder="Apellido Materno" value="<?php echo $usuario->correo ?>" required>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="telefono" class="col-sm-2 control-label">Télefono</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Telefono" value="<?php echo $usuario->telefono ?>" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="domicilio" class="col-sm-2 control-label">Domicilio</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="domicilio" name="domicilio" placeholder="Domicilio" value="<?php echo $usuario->domicilio ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="colonia" class="col-sm-2 control-label">Colonia</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="colonia" name="colonia" placeholder="Colonia" value="<?php echo $usuario->colonia ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="ciudad" class="col-sm-2 control-label">Ciudad</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="ciudad" name="ciudad" placeholder="Ciudad" value="<?php echo $usuario->ciudad ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="especialidad" class="col-sm-2 control-label">Especialidad</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="especialidad" name="especialidad" placeholder="Especialidad" value="<?php if (isset($datos_doctor)) {echo $datos_doctor->especialidad; } ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="ced-prof" class="col-sm-2 control-label">Cédula</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="ced-prof" name="ced-prof" placeholder="Cédula profesional" value="<?php if (isset($datos_doctor)) {echo $datos_doctor->cedula_profesional; } ?>">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-warning">Editar</button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</div>
