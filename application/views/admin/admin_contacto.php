<?php $cod_tipo_usuario = $this->session->userdata('cod_tipo_usuario'); ?>
<?php if ($cod_tipo_usuario != 3): ?>
<?php   redirect('admin/login'); ?>
<?php endif; ?>
<div class="col-md-10">
  <div class="row">
    <div class="col-md-12">
      <div class="content-box-header panel-heading">
        <div class="panel-title ">Lista Mensajes</div>

      <div class="panel-options">

      </div>
      </div>
      <div class="content-box-large box-with-header">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped" id="example">
        <thead>
          <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Correo</th>
            <th>Asunto</th>
            <th>Mensaje</th>
            <th>Fecha</th>
          </tr>
        </thead>
        <tbody>
          <?php if ($lista_mensajes  !== FALSE): ?>
            <?php foreach ($lista_mensajes as $fila): ?>
              <tr>
                <td> <?php echo  $fila->id; ?></td>
                <td><?php echo  $fila->nombre; ?></td>
                  <td><?php echo  $fila->correo ?></td>
                  <td> <?php echo  $fila->asunto; ?></td>
                  <td> <?php echo  $fila->mensaje; ?></td>
              <td> <?php echo  $fila->fecha_creado; ?></td>
              </tr>
            <?php endforeach; ?>
          <?php endif; ?>
          </tbody>
      </table>
    </div>
    </div>
  </div>
</div>
<link href="<?php echo base_url(); ?>vendors/datatables/dataTables.bootstrap.css" rel="stylesheet" media="screen">
<!-- jQuery UI -->
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>vendors/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>vendors/datatables/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url(); ?>js/tables.js"></script>
