<?php echo link_tag('calendario/css/calendar.css'); ?>
<?php $cod_tipo_usuario = $this->session->userdata('cod_tipo_usuario'); ?>
<?php if ($cod_tipo_usuario != 3): ?>
<?php   redirect('admin/login'); ?>
<?php endif; ?>
<div class="col-md-10">
  <div class="row">
    <div class="col-md-12">
      <div class="content-box-header panel-heading">
        <div class="panel-title ">Lista Citas</div>

      <!-- <div class="panel-options">
        <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
        <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
      </div> -->
      </div>
      <div class="content-box-large box-with-header">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped" id="example">
        <thead>
          <tr>
            <th>Id</th>
            <th>Paciente</th>
            <th>Doctor</th>
            <th>Fecha</th>
            <th>Hora</th>
            <th>Confirmacion</th>
            <th>Opciones</th>
          </tr>
        </thead>
        <tbody>
          <?php if ($lista_citas  !== false): ?>
            <?php foreach ($lista_citas as $fila): ?>
              <tr>
                <td> <?php echo  $fila->id_cita; ?></td>
                  <td><?php echo  $fila->nombre.' '.$fila->ape_paterno.' '.$fila->ape_materno; ?></td>
                <td><?php echo  $fila->doctor; ?></td>
                <td> <?php echo  $fila->fecha_cita; ?></td>
                <td>
                 <?php echo  $fila->hora_cita; ?>
              </td>

                <td>
                  <?php if ($fila->confirmacion  == 0) {?>
                    <span class="label label-warning">Sin Confirmar</span>
                        <?php } elseif ($fila->confirmacion  == 1) {?>
                      <span class="label label-success">Confirmada</span>
                    <?php } else {?>
                      <span class="label label-danger">Cancelada</span>
                      <?php }?>
                </td>
                <td>
                  <?php echo anchor('admin/detalleCita/'.$fila->id_cita, 'Detalle', array('class' => 'btn btn-info  btn-xs')) ?>
                </td>
              </tr>
            <?php endforeach; ?>
          <?php endif; ?>
          </tbody>
      </table>
    </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="content-box-header panel-heading">
        <div class="panel-title ">Calendario Citas</div>

      <!-- <div class="panel-options">
        <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
        <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
      </div> -->
      </div>
      <div class="content-box-large box-with-header">
        <div class="row">
          <div class="page-header"><h2></h2></div>
            <div class="pull-left form-inline"><br>
              <div class="btn-group">
                <button class="btn btn-info" data-calendar-nav="prev">Mes Anterior</button>
                <button class="btn btn-default active" data-calendar-nav="today">Hoy</button>
                <button class="btn btn-info" data-calendar-nav="next">Mes Siguiente</button>
              </div>
              <div class="btn-group">
                <button class="btn btn-default active" data-calendar-view="month">Mes</button>
                <button class="btn btn-default" data-calendar-view="year">Año</button>
              </div>
            </div>
            <div class="pull-right form-inline"><br>
            </div>
          </div>
          <br>
          <hr>
          <br>
          <div class="row">
            <div id="calendar"></div>
          </div>
    </div>
    </div>
  </div>
</div>
<link href="<?php echo base_url(); ?>vendors/datatables/dataTables.bootstrap.css" rel="stylesheet" media="screen">
<!-- jQuery UI -->
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>vendors/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>vendors/datatables/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url(); ?>js/tables.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>calendario/js/es-ES.js"></script>
<script src="<?php echo base_url();?>calendario/js/moment.js"></script>
<script src="<?php echo base_url();?>calendario/js/underscore-min.js"></script>
<script src="<?php echo base_url();?>calendario/js/calendar.js"></script>
<script type="text/javascript">
      (function($){
              //creamos la fecha actual
              var date = new Date();
              var yyyy = date.getFullYear().toString();
              var mm = (date.getMonth()+1).toString().length == 1 ? "0"+(date.getMonth()+1).toString() : (date.getMonth()+1).toString();
              var dd  = (date.getDate()).toString().length == 1 ? "0"+(date.getDate()).toString() : (date.getDate()).toString();

              //establecemos los valores del calendario
              var options = {

                  // definimos que los eventos se mostraran en ventana modal
                      modal: '#events-modal',

                      // dentro de un iframe
                      modal_type:'iframe',

                      //obtenemos los eventos de la base de datos
                      events_source: '<?php echo base_url();?>index.php/usuario/obtenerCitasCalendario',

                      // mostramos el calendario en el mes
                      view: 'month',

                      // y dia actual
                      day: yyyy+"-"+mm+"-"+dd,


                      // definimos el idioma por defecto
                      language: 'es-ES',

                      //Template de nuestro calendario
                      tmpl_path: '<?php echo base_url();?>calendario/tmpls/',
                      tmpl_cache: false,


                      // Hora de inicio
                      time_start: '08:00',

                      // y Hora final de cada dia
                      time_end: '22:00',

                      // intervalo de tiempo entre las hora, en este caso son 30 minutos
                      time_split: '30',

                      // Definimos un ancho del 100% a nuestro calendario
                      width: '100%',

                      onAfterEventsLoad: function(events)
                      {
                              if(!events)
                              {
                                      return;
                              }
                              var list = $('#eventlist');
                              list.html('');

                              $.each(events, function(key, val)
                              {
                                      $(document.createElement('li'))
                                              .html('<a href="' + val.url + '">' + val.title + '</a>')
                                              .appendTo(list);
                              });
                      },
                      onAfterViewLoad: function(view)
                      {
                              $('.page-header h2').text(this.getTitle());
                              $('.btn-group button').removeClass('active');
                              $('button[data-calendar-view="' + view + '"]').addClass('active');
                      },
                      classes: {
                              months: {
                                      general: 'label'
                              }
                      }
              };


              // id del div donde se mostrara el calendario
              var calendar = $('#calendar').calendar(options);

              $('.btn-group button[data-calendar-nav]').each(function()
              {
                      var $this = $(this);
                      $this.click(function()
                      {
                              calendar.navigate($this.data('calendar-nav'));
                      });
              });

              $('.btn-group button[data-calendar-view]').each(function()
              {
                      var $this = $(this);
                      $this.click(function()
                      {
                              calendar.view($this.data('calendar-view'));
                      });
              });

              $('#first_day').change(function()
              {
                      var value = $(this).val();
                      value = value.length ? parseInt(value) : null;
                      calendar.setOptions({first_day: value});
                      calendar.view();
              });
      }(jQuery));
  </script>
