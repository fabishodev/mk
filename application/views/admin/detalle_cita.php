<?php echo link_tag('css/date_time_picker.css'); ?>
<?php $cod_tipo_usuario = $this->session->userdata('cod_tipo_usuario'); ?>
<?php if ($cod_tipo_usuario != 3): ?>
<?php   redirect('admin/login'); ?>
<?php endif; ?>
<div class="col-md-10">
  <div class="row">
    <div class="col-md-12">
      <div class="content-box-large">
        <?php if ($cita !== NULL): ?>
        <div class="page-header">
          <h2>Detalle Cita <small><?php echo  $cita->especialidad; ?></small></h2>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-6">
              <!--Notificaciones-->
              <?php if ($success != '') { ?>
                <div class="alert alert-success alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <strong><?php echo $success ?></strong>
              </div>
              <?php $this->session->set_userdata('success', '');} ?>

              <?php if ($danger != '') { ?>
                <div class="alert alert-danger alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <strong><?php echo $danger ?></strong>
                </div>
              <?php $this->session->set_userdata('danger', '');} ?>
              <!---->
            </div>
          </div>
          <div class="row">
            <form id="form-user" class="form-horizontal" action="<?php echo base_url();?>index.php/admin/editarCita/<?php echo $cita->id_cita;?>" method="post" >
            <div class="col-md-6">
              <div class="form-group">
                <label for="paciente" class="col-sm-2 control-label">Paciente</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="paciente" name="paciente" placeholder="Paciente" value="<?php echo  $cita->nombre.' '.$cita->ape_paterno.' '.$cita->ape_materno; ?>" readonly>
                </div>
              </div>
              <div class="form-group">
                  <label for="correo-paciente" class="col-sm-2 control-label">Email Paciente</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="correo-paciente" name="correo-paciente" placeholder="Correo Paciente" value="<?php echo  $cita->correo;?>" readonly>

                  </div>
                </div>
                <div class="form-group">
                    <label for="telefono" class="col-sm-2 control-label">Telefono</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Telefono" value="<?php echo  $cita->telefono;?>" readonly>

                    </div>
                  </div>
                  <div class="form-group">
                      <label for="doctor" class="col-sm-2 control-label">Doctor</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="doctor" name="doctor" placeholder="" value="<?php echo  $cita->doctor;?>">

                      </div>
                    </div>
                  <hr>
                  <div class="form-group">
                  <label for="sel-categoria" class="col-sm-2 control-label">Doctor</label>
                  <div class="col-sm-10">
                      <select class="form-control" id="sel-doctor" name="sel-doctor" >
                        <option value="">Seleccione</option>
                        <?php foreach ($lista_doctores as $l) { ?>
                          <?php if ($l->id == $cita->cod_doctor): ?>
                            <?php $seleccionado = "selected" ?>
                            <?php else: ?>
                              <?php $seleccionado = "" ?>
                          <?php endif; ?>
                          <option <?php echo $seleccionado ?> value="<?php echo $l->id;?>"><?php echo $l->nombre.' '.$l->ape_paterno.' '.$l->ape_paterno;?></option>
                        <?php }?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="correo-doctor" class="col-sm-2 control-label">Correo Doctor</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="correo-doctor" name="correo-doctor" placeholder="Correo Doctor" value="<?php echo  $cita->correo_doc; ?>" readonly>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="telefono-doc" class="col-sm-2 control-label">Teléfono Doctor</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="telefono-doc" name="telefono-doc" placeholder="Teléfono Doctor" value="<?php echo  $cita->telefono_doc; ?>" readonly>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="especialidad" class="col-sm-2 control-label">Especialidad</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="especialidad" name="especialidad" placeholder="Especialidad" value="<?php echo  $cita->especialidad; ?>" readonly>
                    </div>
                  </div>

              </div>
                  <div class="col-md-6">

                    <div class="form-group">
                      <label for="fecha-cita" class="col-sm-2 control-label">Fecha Cita</label>
                      <div class="col-sm-10">
                        <input type="text" class="date-pick form-control" id="fecha-cita" name="fecha-cita" placeholder="Fecha Cita" value="<?php echo  $cita->fecha_cita; ?>" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="hora-cita" class="col-sm-2 control-label">Hora Cita</label>
                      <div class="col-sm-10">
                        <input type="text" class="time-pick form-control" id="hora-cita" name="hora-cita" placeholder="Hora Cita" value="<?php echo  $cita->hora_cita; ?>" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="motivo-cita" class="col-sm-2 control-label">Motivo Cita</label>
                      <div class="col-sm-10">
                        <textarea class="form-control" id="motivo-cita" name="motivo-cita" rows="5" cols="40" readonly><?php echo  $cita->motivo_cita; ?></textarea>
                      </div>
                    </div>

                      <div class="form-group">
                        <label for="nota" class="col-sm-2 control-label">Nota</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" name="nota-cita" id="nota-cita" rows="5" cols="40"><?php echo  $cita->nota; ?></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                      <label for="confirmacion" class="col-sm-2 control-label">Estatus</label>
                        <div class="col-sm-10">
                          <select class="form-control" id="confirmacion" name="confirmacion" required>
                            <?php if ($cita->confirmacion == '1'): ?>
                              <option value="0">Sin Confirmar</option>
                              <option selected value="1">Confirmada</option>
                              <option value="2">Cancelar</option>
                            <?php elseif($cita->confirmacion == '0'): ?>
                                <option selected value="0">Sin Confirmar</option>
                                <option value="1">Confirmada</option>
                                <option value="2">Cancelar</option>
                              <?php elseif($cita->confirmacion == '2'): ?>
                                <option value="0">Sin Confirmar</option>
                                <option value="1">Confirmada</option>
                                <option selected value="2">Cancelar</option>
                              <?php endif; ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-success">Guardar</button>
                        </div>
                      </div>
                    </div>
                </form>
      </div>
      </div>
              <?php endif; ?>
    </div>
  </div>
</div>
<script src="<?php echo base_url();?>js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>js/bootstrap-timepicker.js"></script>
<script src="<?php echo base_url();?>js/bootstrap-datepicker.es.js"></script>
<script>
$('input.date-pick').datepicker({
  format: 'yyyy/mm/dd',
  startDate: '0',
  language: 'es',
});
$('input.time-pick').timepicker({
  showInpunts: false
});

</script>
