<?php $cod_tipo_usuario = $this->session->userdata('cod_tipo_usuario'); ?>
<?php if ($cod_tipo_usuario != 3): ?>
<?php   redirect('admin/login'); ?>
<?php endif; ?>
<div class="col-md-10">
  <div class="content-box-header panel-heading">
    <div class="panel-title ">Lista Doctores</div>
  </div>
  <div class="content-box-large box-with-header">
      <table cellpadding="0" cellspacing="0" border="0" class="table table-striped" id="example">
      <thead>
        <tr>
          <th>Id</th>
          <th>Especialidad</th>
          <th>Nombre Completo</th>
          <th>Télefono</th>
          <th>Estatus</th>
          <th>Fecha Creado</th>
          <th>Opciones</th>
        </tr>
      </thead>
      <tbody>
        <?php if ($lista_doctores  !== FALSE): ?>
          <?php foreach ($lista_doctores as $fila): ?>
            <tr>
                <td>  <?php echo  $fila->id; ?></td>
              <td>  <?php echo  $fila->especialidad; ?></td>
              <td><?php echo  $fila->nombre.' '.$fila->ape_paterno.' '.$fila->ape_materno; ?></td>
              <td><?php echo  $fila->telefono; ?></td>
              <td>
                <?php if ( $fila->activo  == 1){ ?>
                  <span class="label label-success"> Activo </span>
                  <?php }else { ?>
                    <span class="label label-danger"> No Activo</span>
                    <?php }?>
              </td>
              <td><?php echo  $fila->fecha_creado; ?></td>
              <td>
                <?php echo anchor('admin/detalleDoctor/'.$fila->id,'Detalle', array('class'=>'btn btn-info  btn-xs')) ?>
              </td>
            </tr>
          <?php endforeach; ?>
        <?php endif; ?>
        </tbody>
    </table>
    </div>
  </div>
</div>
<link href="<?php echo base_url(); ?>vendors/datatables/dataTables.bootstrap.css" rel="stylesheet" media="screen">
<!-- jQuery UI -->
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>vendors/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>vendors/datatables/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url(); ?>js/tables.js"></script>
