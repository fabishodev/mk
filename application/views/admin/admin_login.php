<!DOCTYPE html>
<html>
  <head>
    <title>Medkeep Administrador</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="icon" type="image/png" href="<?php echo base_url(); ?>images/favicon-medical.png">
    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="<?php echo base_url(); ?>css/styles-admin.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-bg">
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-12">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1><a href="<?php echo site_url('admin/login'); ?>">Medkeep Administrador</a></h1>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>

	<div class="page-content container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="login-wrapper">
			        <div class="box">
			            <div class="content-wrap">
			                <h6>Ingresar</h6>
                      <?php if ($danger != '') { ?>
                        <div class="alert alert-danger alert-dismissible" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <strong><?php echo $danger ?></strong>
                        </div>
                      <?php $this->session->set_userdata('danger', '');} ?>
			                <form class="" action="<?php echo base_url(); ?>index.php/admin/ingresar" method="post">
                        <input class="form-control" type="text" id="correo" name="correo" placeholder="Dirección email" required>
  			                <input class="form-control" type="password" id="password" name="password" placeholder="Contraseña" required>
  			                <div class="action">
  			                  <button class="btn btn-primary signup" type="submit" name="button">Accesar</button>
  			                </div>
			                </form>
			            </div>
			        </div>
              <div class="already">
			             Copyright <?php echo date('Y'); ?> <a href='<?php echo site_url('home'); ?>'>Medkeep</a>

			        </div>
			    </div>
			</div>
		</div>
	</div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>bootstrap/js/custom.js"></script>
  </body>
</html>
