<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>tinymce.init({ selector:'textarea' });</script>
<?php $cod_tipo_usuario = $this->session->userdata('cod_tipo_usuario'); ?>
<?php if ($cod_tipo_usuario != 3): ?>
<?php   redirect('admin/login'); ?>
<?php endif; ?>
<div class="col-md-10">
  <div class="row">
    <div class="col-md-12">
      <div class="content-box-large">
        <div class="panel-heading">
          <div class="panel-title">Nueva Noticia</div>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-6">
              <!--Notificaciones-->
              <?php if ($success != '') { ?>
                <div class="alert alert-success alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <strong><?php echo $success ?></strong>
              </div>
              <?php $this->session->set_userdata('success', '');} ?>

              <?php if ($danger != '') { ?>
                <div class="alert alert-danger alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <strong><?php echo $danger ?></strong>
                </div>
              <?php $this->session->set_userdata('danger', '');} ?>
              <!---->
            </div>
          </div>
          <form id="form-user" class="form-horizontal" action="<?php echo base_url();?>index.php/admin/agregarNoticia" method="post" enctype="multipart/form-data" >
            <div class="row">
              <div class="col-md-6">
              <div class="form-group">
                <label for="sel-categoria" class="col-sm-2 control-label">Categoria</label>
                <div class="col-sm-10">
                    <select class="form-control" id="sel-categoria" name="sel-categoria" required>
                      <option value="">Seleccione</option>
                      <?php foreach ($lista_categorias as $c): ?>
                          <option value="<?php echo $c->id;?>"><?php echo $c->categoria;?></option>
                      <?php endforeach; ?>

                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="titulo" class="col-sm-2 control-label">Título Noticia</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="titulo" name="titulo" placeholder="Título noticia" value="" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="noticia-corta" class="col-sm-2 control-label">Resumen Noticia</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" name="noticia-corta" id="noticia-corta" rows="8" cols="40"></textarea>
                  </div>
                </div>


              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="noticia" class="col-sm-2 control-label">Noticia</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" name="noticia" id="noticia" rows="8" cols="40"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="upl" class="col-sm-2 control-label">Caratula</label>
                  <div class="col-sm-10">
                  <input type="file" id="upl" name="upl">
                  <p class="help-block">Caratula de la entrada de blog.</p>
                    </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-success">Guardar</button>
                  <a class="btn btn-info" href="<?php echo site_url('admin/adminBlog') ?>">Regresar</a>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
