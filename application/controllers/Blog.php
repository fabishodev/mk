<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 		function __construct() {
	         parent::__construct();
					 $this->load->model('Blog_model', 'blog_m');
	     }

			 private $defaultData = array(
				 'title'			=> 'Medkeep',
				 //'layout' 		=> 'layout/lytDefault',
				 'layout' 		=> 'layout/lytdefault',
				 'contentView' 	=> 'vUndefined',
				 'stylecss'		=> '',
				 );

			private function _renderView($data = array())
			{
				$data = array_merge($this->defaultData, $data);
				$this->load->view($data['layout'], $data);
			}

			public function categoria($cod = null)
			{
				$data = array();
		    $data['contentView'] = 'blog/categoria';
				$data['noticias'] = $this->blog_m->obtenerNoticiasBlog($cod);
	    	$data['scripts'] = array('medkeep');
		  	$this->_renderView($data);
			}

      public function noticia($id)
  		{
  			$data = array();
  	    $data['contentView'] = 'blog/noticia';
				$data['noticia'] = $this->blog_m->obtenerNoticia($id);
				$data['comentarios'] = $this->blog_m->obtenerComentarios($id);
				$data['recientes'] = $this->blog_m->obtenerNoticiasRecientes();
				$data['etiquetas'] = $this->blog_m->obtenerEtiquetasNoticias();
      	$data['scripts'] = array('medkeep');
				$data['success'] = '';
				if ($this->session->userdata('success')) {
						$success = $this->session->userdata('success');
						$data['success'] = $success;
				}
				$data['danger'] = '';
				if ($this->session->userdata('danger')) {
						$danger = $this->session->userdata('danger');
						$data['danger'] = $danger;
				}
  	  	$this->_renderView($data);
  		}


		public function index()
		{
			$data = array();
	    $data['contentView'] = 'blog/index';
			$data['noticias'] = $this->blog_m->obtenerNoticiasBlog($cod= null);
			$data['recientes'] = $this->blog_m->obtenerNoticiasRecientes();
			$data['etiquetas'] = $this->blog_m->obtenerEtiquetasNoticias();
    	$data['scripts'] = array('medkeep');
	  	$this->_renderView($data);
		}



}
