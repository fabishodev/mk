<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
    /**
             * Index Page for this controller.
             *
             * Maps to the following URL
             * 		http://example.com/index.php/welcome
             *	- or -
             * 		http://example.com/index.php/welcome/index
             *	- or -
             * Since this controller is set as the default controller in
             * config/routes.php, it's displayed at http://example.com/
             *
             * So any other public methods not prefixed with an underscore will
             * map to /index.php/welcome/<method_name>
             *
             * @see https://codeigniter.com/user_guide/general/urls.html
             */
            public function __construct()
            {
                parent::__construct();
                date_default_timezone_set('America/Mexico_City');
                $this->load->model('Usuario_model', 'usuario_m');
                $this->load->model('Doctor_model', 'doctor_m');
                $this->load->model('Citas_model', 'citas_m');
                $this->load->model('Blog_model', 'blog_m');
                $this->load->model('Tratamiento_model', 'tratamiento_m');
                $this->load->model('Documento_model', 'documento_m');
                $this->load->model('Contacto_model', 'contacto_m');
                  $this->load->library('Notificaciones');
            }

    private $defaultData = array(
      'title' => 'Medkeep',
      //'layout' 		=> 'layout/lytDefault',
      'layout' => 'layout_admin/lytdefault',
      'contentView' => 'vUndefined',
      'stylecss' => '',
    );

    private function _renderView($data = array())
    {
        $data = array_merge($this->defaultData, $data);
        $this->load->view($data['layout'], $data);
    }

    public function enviarRecordatorioTratamiento()
    {
      $horas = $this->tratamiento_m->obtenerHorasTratamientos();
      foreach ($horas as $h) {
        $strStart = $h->fecha_hora_inicio;
        $strEnd   = $h->fecha_hora_fin;
        $strNow   = date("Y-m-d H:i:s");
        $dteStart = new DateTime($strStart);
        $dteEnd   = new DateTime($strEnd);
        $dteNow   = new DateTime($strNow);
        $dteDiff  = $dteStart->diff($dteEnd);
        $hours = $dteDiff->h;
        $hours = $hours + ($dteDiff->days*24);
        $dteDiff2  = $dteNow->diff($dteEnd);
        $hours2 = $dteDiff2->h;
        $hours2 = $hours + ($dteDiff2->days*24);
        $hour_to_add = $h->lapso;
        echo 'Fecha Hora Inicio: ';var_dump($h->fecha_hora_inicio);
        echo '<br>Fecha Hora Fin: ';var_dump($h->fecha_hora_fin);
        echo '<br>Fecha Hora Ahora: ';var_dump($dteNow->format('Y-m-d H:i'));
        echo '<br>Tomar cada: ';var_dump($h->lapso);
        echo '<br>Horas totales: ';var_dump( $hours);
        echo '<br>Horas restantes: ';var_dump( $hours2);
        echo '<br>Tiempo restante: ';var_dump( $dteDiff->format("%H:%I:%S"));

        $time = new DateTime($h->fecha_hora_inicio);
        for ($i=0; $i <= $hours/$hour_to_add; $i++) {
          if ($i == 0) {
            $time->add(new DateInterval('PT' . $i . 'H'));
          }else {
            $time->add(new DateInterval('PT' . $hour_to_add . 'H'));
          }
          $stamp = $time->format('Y-m-d H:i');

          echo '<br>+'.$i.' Hora: ';var_dump( $stamp);
          echo '<br>---<br>';

            if ($stamp == $dteNow->format('Y-m-d H:i')) {
              $this->notificaciones->EnviarNotificacionRecordatorioTratamiento($h->nombre, $h->correo,$h->medicamento, $h->observaciones,  date("d-m-Y", strtotime($h->fecha_inicio)),date("d-m-Y", strtotime($h->fecha_fin)), date("G:i", strtotime($h->hora)),date("G:i", strtotime($h->hora_fin)),$h->lapso);
            }

        }
      }  

    }

     public function enviarProximasCitasRecordatorios()
    {
      $citas = $this->citas_m->obtenerProximasCitasRecordatorios();
      //$strStart = '2013-06-19 18:25';
      $strEnd   = date("Y-m-d H:i:s");
      //$dteStart = new DateTime($strStart);
      $dteEnd   = new DateTime($strEnd);
      //$dteDiff  = $dteStart->diff($dteEnd);

        foreach ($citas as $c) {
          $strStart = $c->fecha_hora_cita;
          $dteStart = new DateTime($strStart);
          $dteDiff  = $dteStart->diff($dteEnd);
          $hours = $dteDiff->h;
          $hours = $hours + ($dteDiff->days*24);
          echo '<pre>';var_dump( $hours);
          echo '<pre>';var_dump( $dteDiff->format("%H:%I:%S"));
          echo '<pre>';var_dump( $dteDiff->format("%H:%I"));

          if ($c->horas_restantes > 1) {
            if ($hours == 12 AND $c->notificacion == 0) {
              if ($dteDiff->format("%H:%I") >= "12:00" && $dteDiff->format("%H:%I") <= "12:10") {
                $this->notificaciones->EnviarCorreoRecordatorio($c->correo, $c->doctor, $c->hospital, $c->motivo_cita, $c->fecha_cita, $c->hora_cita);
                $datos = array('notificacion' => 1);
                $this->citas_m->actualizarCita($datos, $c->id_cita);
              }

            }elseif ($hours == 3 AND $c->notificacion == 1) {
              if ($dteDiff->format("%H:%I") >= "03:00" && $dteDiff->format("%H:%I") <= "03:10") {
                $this->notificaciones->EnviarCorreoRecordatorio($c->correo, $c->doctor, $c->hospital, $c->motivo_cita, $c->fecha_cita, $c->hora_cita);
                $datos = array('notificacion' => 2);
                $this->citas_m->actualizarCita($datos, $c->id_cita);
              }
            }
          }
        }
    }

    public function adminContacto()
    {
      if (!$this->session->userdata('id_usuario') && $this->session->userdata('cod_tipo_usuario') == 3) {
            redirect('admin/login');
        }
        $data = array();
        $data['contentView'] = 'admin/admin_contacto';
        $data['lista_mensajes'] = $this->contacto_m->obtenerListaMensajes();
        $data['scripts'] = array('medkeep');
        $this->_renderView($data);
    }

    public function adminDocumentos()
    {
      if (!$this->session->userdata('id_usuario') && $this->session->userdata('cod_tipo_usuario') == 3) {
            redirect('admin/login');
        }else {
          redirect('admin/adminUsuarios');
        }
        $data = array();
        $data['contentView'] = 'admin/admin_documentos';
        $data['lista_documentos'] = $this->documento_m->obtenerListaDocumentos();
        $data['scripts'] = array('medkeep');
        $this->_renderView($data);
    }

    public function adminTratamientos()
    {
      if (!$this->session->userdata('id_usuario') && $this->session->userdata('cod_tipo_usuario') == 3) {
            redirect('admin/login');
        }else {
          redirect('admin/adminUsuarios');
        }
        $data = array();
        $data['contentView'] = 'admin/admin_tratamientos';
        $data['lista_tratamientos'] = $this->tratamiento_m->obtenerListaTratamientos();
        $data['scripts'] = array('medkeep');
        $this->_renderView($data);
    }

    public function ingresar()
    {
      $correo = $this->input->post('correo');
      $password = $this->input->post('password');
      $usuario = $this->usuario_m->obtenerUsuarioAdmin($correo, $password);
      //echo '<pre>'; var_dump($usuario);
      //die();
         if ($usuario) {
        //   if ($usuario->activo == 0) {
        //     $this->session->set_userdata('danger', 'Usuario inactivo, su cuenta no ha sido activada, contacte al administrador.');
        //     redirect('admin/login');
        //   }else{

              //
              if($usuario->cod_tipo != 3){

                $this->session->set_userdata('danger', 'Su cuenta no tiene privelegios para usar el sistema, contacte al administrador.');
                redirect('admin/login');


              }else{


      						$this->session->set_userdata('cod_tipo_usuario', $usuario->cod_tipo);
      						$this->session->set_userdata('id_usuario', $usuario->id);
      						$this->session->set_userdata('correo', $usuario->correo);
      						$this->session->set_userdata('nombre_completo', $usuario->nombre.' '.$usuario->ape_paterno.' '.$usuario->ape_materno);
      						$this->session->set_userdata('danger', '');
      						redirect('admin/adminUsuarios');

                }

        //     $this->session->set_userdata('danger', 'Correo electrónico y/o contraseña incorrectos, intentelo de nuevo.');
        //     redirect('admin/login');
        //   }
        //
        //
        } else {
            $this->session->set_userdata('danger', 'Correo electrónico y/o contraseña incorrectos, intentelo de nuevo.');
            redirect('admin/login');
				}

    }

    public function login()
    {
        $data = array();
        $data['contentView'] = 'admin/admin_login';
        $data['layout'] = 'layout_admin/lytvacio';
        $data['scripts'] = array('medkeep');
        $data['danger'] = '';
        if ($this->session->userdata('danger')) {
            $danger = $this->session->userdata('danger');
            $data['danger'] = $danger;
        }
        $this->_renderView($data);
    }

    // Formatear una fecha a microtime para añadir al evento, tipo 1401517498985.
  function _formatear($fecha)
  {
    //return strtotime(substr($fecha, 0, 4)."-".substr($fecha, 6, 2)."-".substr($fecha, 9, 2)." " .substr($fecha, 10, 6)) * 1000;
    return strtotime(substr($fecha, 6, 4)."-".substr($fecha, 3, 2)."-".substr($fecha, 0, 2)." " .substr($fecha, 10, 6)) * 1000;
  }

    public function calendario()
    {
        $data = array();
        $data['contentView'] = 'admin/calendario';
        $data['scripts'] = array('medkeep');
        $this->_renderView($data);
    }

    //Blog

    public function cambiarEstatusComentario($id)
    {
      if (!$this->session->userdata('id_usuario') && $this->session->userdata('cod_tipo_usuario') == 3) {
            redirect('admin/login');
        }

      $comentario = $this->blog_m->obtenerComentario($id);
      if ($comentario->publicado == 1) {
        $datos = array('publicado' => 0 );
      }else {
        $datos = array('publicado' => 1 );
      }
      if ($this->blog_m->actualizarComentario($datos,$id)) {
          $this->session->set_userdata('success', 'Comentario editada correctamente.');
      } else {
          $this->session->set_userdata('danger', 'No se pudo editar el comentario, intentelo de nuevo.');
      }
      redirect('admin/adminBlog');
    }

    public function editarNoticia($id)
  {
    if (!$this->session->userdata('id_usuario') && $this->session->userdata('cod_tipo_usuario') == 3) {
          redirect('admin/login');
      }
      $allowed = array('png', 'jpg');
      $nombre_achivo = $_FILES['upl']['name'];
      $extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);
      $detalle = $this->blog_m->obtenerNoticia($id);

      if (isset($_FILES['upl']) && $_FILES['upl']['error'] == 0) {
          if (!in_array(strtolower($extension), $allowed)) {
              $this->session->set_userdata('danger', 'Extension no permitida.');
              redirect('/admin/detalleNoticia/'.$id);
          }

          $archivo = 'img/noticias/'.$nombre_achivo;

          if (file_exists($archivo)) {
              unlink($archivo);
          }

          if (move_uploaded_file($_FILES['upl']['tmp_name'], 'img/noticias/'.$_FILES['upl']['name'])) {
              $config['image_library'] = 'gd2';
              $config['source_image'] = 'img/noticias/'.$nombre_achivo;
              $config['create_thumb'] = false;
              $config['maintain_ratio'] = true;
              $config['width'] = 1280;
              $config['height'] = 440;
              $this->load->library('image_lib', $config);
              $this->image_lib->resize();
              $caratula = $nombre_achivo;
          } else {
              $this->session->set_userdata('danger', 'No se pudo agregar la imagen, intentelo de nuevo.');
              redirect('/admin/detalleNoticias/'.$id);
          }
      } else {
          $caratula = $detalle->caratula_noticia;
      }

      $titulo_noticia = $this->input->post('titulo');
      $noticia_corta = $this->input->post('noticia-corta');
      $noticia_completa = $this->input->post('noticia');
      $cod_categoria = $this->input->post('sel-categoria');
      $publicada = $this->input->post('publicada');


      $datos = array(
        'titulo_noticia' => $titulo_noticia,
        'noticia_corta' => $noticia_corta,
        'noticia_completa' => $noticia_completa,
        'caratula_noticia' => $caratula,
        'cod_categoria' => $cod_categoria,
        'publicada' => $publicada,
        'fecha_creado' => date('Y-m-d H:i:s'),
      );
    //  die;var_dump($datos);
      if ($this->blog_m->actualizarNoticia($datos, $id)) {
          $this->session->set_userdata('success', 'Noticia actualizada correctamente.');
      }else {
          $this->session->set_userdata('danger', 'No se pudo actualizar la noticia, intentelo de nuevo.');
      }
      redirect('admin/detalleNoticia/'.$id);
  }

    public function detalleNoticia($id)
    {
      if (!$this->session->userdata('id_usuario') && $this->session->userdata('cod_tipo_usuario') == 3) {
            redirect('admin/login');
        }
        $data = array();
        $data['contentView'] = 'admin/detalle_noticia';
        $data['noticia'] = $this->blog_m->obtenerNoticia($id);
        $data['lista_categorias'] = $this->blog_m->obtenerListaCategorias();
        $data['scripts'] = array('medkeep');
        $data['success'] = '';
        if ($this->session->userdata('success')) {
            $success = $this->session->userdata('success');
            $data['success'] = $success;
        }
        $data['danger'] = '';
        if ($this->session->userdata('danger')) {
            $danger = $this->session->userdata('danger');
            $data['danger'] = $danger;
        }
        $this->_renderView($data);
    }

    public function cambiarEstatusCategoria($id)
    {
      if (!$this->session->userdata('id_usuario') && $this->session->userdata('cod_tipo_usuario') == 3) {
            redirect('admin/login');
        }
      $categoria = $this->blog_m->obtenerCategoria($id);
      if ($categoria->activo == 1) {
        $datos = array('activo' => 0 );
      }else {
        $datos = array('activo' => 1 );
      }
      if ($this->blog_m->actualizarCategoria($datos,$id)) {
          $this->session->set_userdata('success', 'Categoria editada correctamente.');
      } else {
          $this->session->set_userdata('danger', 'No se pudo editar categoria, intentelo de nuevo.');
      }
      redirect('admin/adminBlog');
    }

    public function agregarNoticia()
    {
      if (!$this->session->userdata('id_usuario') && $this->session->userdata('cod_tipo_usuario') == 3) {
            redirect('admin/login');
        }
        $allowed = array('png', 'jpg');
        $nombre_achivo = $_FILES['upl']['name'];
        $extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);
        $archivo = 'img/noticias/'.$nombre_achivo;
        if (isset($_FILES['upl']) && $_FILES['upl']['error'] == 0) {
            if (!in_array(strtolower($extension), $allowed)) {
                $this->session->set_userdata('danger', 'Extension no permitida.');
                redirect('/admin/nuevaNoticia/');
            }
            if (!file_exists($archivo)) {
                if (move_uploaded_file($_FILES['upl']['tmp_name'], 'img/noticias/'.$_FILES['upl']['name'])) {
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = 'img/noticias/'.$nombre_achivo;
                    $config['create_thumb'] = false;
                    $config['maintain_ratio'] = true;
                    $config['width'] = 1280;
                    $config['height'] = 440;
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();
                    $caratula_noticia = $nombre_achivo;
                } else {
                    $this->session->set_userdata('danger', 'No se pudo agregar la imagen, intentelo de nuevo.');
                    redirect('/admin/nuevaEntrada/');
                }
            } else {
                $this->session->set_userdata('danger', 'Ya existe un archivo con ese nombre, cambie el nombre al archivo e intentelo de nuevo.');
                redirect('/admin/nuevaEntrada/');
            }
        }else {
            $this->session->set_userdata('danger', 'No se pudo guardar la noticia, intentelo de nuevo.');
            redirect('admin/nuevaNoticia');
        }


        $titulo_noticia = $this->input->post('titulo');
        $noticia_corta = $this->input->post('noticia-corta');
        $noticia_completa = $this->input->post('noticia');
        $cod_categoria = $this->input->post('sel-categoria');

        $datos = array(
          'titulo_noticia' => $titulo_noticia,
          'noticia_corta' => $noticia_corta,
          'noticia_completa' => $noticia_completa,
          'caratula_noticia' => $caratula_noticia,
          'cod_categoria' => $cod_categoria,
          'publicada' => 1,
          'fecha_creado' => date('Y-m-d H:i:s'),
        );
        if ($this->blog_m->guardarNoticia($datos)) {
            $this->session->set_userdata('success', 'Entrada de blog guardada correctamente.');
        } else {
            $this->session->set_userdata('danger', 'No se pudo guardar la entrada de blog, intentelo de nuevo.');
        }
        redirect('admin/adminBlog');
    }

    public function nuevaNoticia()
    {
      if (!$this->session->userdata('id_usuario') && $this->session->userdata('cod_tipo_usuario') == 3) {
            redirect('admin/login');
        }
        $data = array();
        $data['contentView'] = 'admin/nueva_noticia';
        $data['lista_categorias'] = $this->blog_m->obtenerListaCategoriasActivas();
        $data['scripts'] = array('medkeep');
        $data['success'] = '';
        if ($this->session->userdata('success')) {
            $success = $this->session->userdata('success');
            $data['success'] = $success;
        }
        $data['danger'] = '';
        if ($this->session->userdata('danger')) {
            $danger = $this->session->userdata('danger');
            $data['danger'] = $danger;
        }
        $this->_renderView($data);
    }

    public function agregarCategoria()
    {
      if (!$this->session->userdata('id_usuario') && $this->session->userdata('cod_tipo_usuario') == 3) {
            redirect('admin/login');
        }
        $categoria = $this->input->post('categoria');
        $descripcion = $this->input->post('descripcion-categoria');
        $datos = array(
        'categoria' => $categoria,
        'descripcion' => $descripcion,
        'activo' => 1,
        'fecha_creado' => date('Y-m-d H:i:s'),
      );
        if ($this->blog_m->registrarCategoria($datos)) {
            $this->session->set_userdata('success', 'Se registro la categoria correctamente.');
        } else {
            $this->session->set_userdata('danger', 'No se pudo registro la categoria, intentelo de nuevo.');
        }
        redirect('admin/adminBlog');
    }

    public function adminBlog()
    {
      if (!$this->session->userdata('id_usuario') && $this->session->userdata('cod_tipo_usuario') == 3) {
            redirect('admin/login');
        }
        $data = array();
        $data['contentView'] = 'admin/admin_blog';
        $data['lista_noticias'] = $this->blog_m->obtenerListaNoticias();
        $data['lista_categorias'] = $this->blog_m->obtenerListacategorias();
        $data['lista_comentarios'] = $this->blog_m->obtenerListaComentarios();
        $data['scripts'] = array('medkeep');
        $data['success'] = '';
        if ($this->session->userdata('success')) {
            $success = $this->session->userdata('success');
            $data['success'] = $success;
        }
        $data['danger'] = '';
        if ($this->session->userdata('danger')) {
            $danger = $this->session->userdata('danger');
            $data['danger'] = $danger;
        }
        $this->_renderView($data);
    }

    //Citas
    public function datosDoctor()
    {
      if (!$this->session->userdata('id_usuario') && $this->session->userdata('cod_tipo_usuario') == 3) {
            redirect('admin/login');
        }
        $id = $this->input->post('id');
        $doctor = $this->doctor_m->obtenerDetalleDoctor($id);
        $respuesta = array(
        'telefono' => $doctor->telefono,
        'correo' => $doctor->correo,
        'especialidad' => $doctor->especialidad,
      );
        echo json_encode($respuesta);
    }

    public function editarCita($id)
    {
      if (!$this->session->userdata('id_usuario') && $this->session->userdata('cod_tipo_usuario') == 3) {
            redirect('admin/login');
        }else {
          redirect('admin/adminUsuarios');
        }
        $fecha_cita = $this->input->post('fecha-cita');
        $hora_cita = $this->input->post('hora-cita');
        $motivo_cita = $this->input->post('motivo-cita');
        $confirmacion = $this->input->post('confirmacion');
        $nota_cita = $this->input->post('nota-cita');
        $cod_doctor = $this->input->post('sel-doctor');
        $doctor = $this->input->post('doctor');

        // $doctor = $this->doctor_m->obtenerDetalleDoctor($cod_doctor);
        // $nombre_doctor = $doctor->nombre.' '.$doctor->ape_paterno.' '.$doctor->ape_materno;
        $nombre_doctor  = $doctor;

        $fecha_hora = date("d-m-Y", strtotime($fecha_cita)).' '.date("G:i", strtotime($hora_cita));
        $start =  $this->_formatear($fecha_hora);
        $end = $this->_formatear($fecha_hora);
        if ( $nota_cita) {
          $title = '<p>Cita con Dr(a): '.$nombre_doctor.'</p><p>Motivo: '.  $motivo_cita.'</p><p>Hora: '.date("G:i", strtotime($hora_cita)).'</p><p>Nota: '.$nota_cita;
        }else {
            $title = '<p>Cita con Dr(a): '.$nombre_doctor.'</p><p>Motivo: '.  $motivo_cita.'</p><p>Hora: '.date("G:i", strtotime($hora_cita)).'</p>';
        }
        $body = $motivo_cita;
        $class = 'event-warning';

        $datos = array(
        'doctor' => $doctor,
          'fecha_cita' => $fecha_cita,
          'hora_cita' => $hora_cita,
          'confirmacion' => $confirmacion,
          'nota' => $nota_cita,
          'fecha_actualizado' => date('Y-m-d H:i:s'),
        );

        $programacion = array(
          'title' => $title,
          'body' => $body,
          'url' => null,
          'class' => $class,
          'start' => $start,
          'end' => $end,
        );

       //var_dump($datos);
        if ($this->citas_m->actualizarCita($datos, $id)) {
            if ($this->citas_m->actualizarProgramacionCita($programacion, $id)) {
                $this->session->set_userdata('success', 'Se actualizado la cita correctamente.');
            }else {
                $this->session->set_userdata('danger', 'No se pudo actualizar el doctor, intentelo de nuevo.');
            }

        } else {
            $this->session->set_userdata('danger', 'No se pudo actualizar el doctor, intentelo de nuevo.');
        }
        redirect('admin/detalleCita/'.$id);
    }

    public function detalleCita($id)
    {
      if (!$this->session->userdata('id_usuario') && $this->session->userdata('cod_tipo_usuario') == 3) {
            redirect('admin/login');
        }
        $data = array();
        $data['contentView'] = 'admin/detalle_cita';
        $data['cita'] = $this->citas_m->obtenerDetalleCita($id);
        $data['lista_doctores'] = $this->doctor_m->obtenerListaDoctores();
        $data['scripts'] = array('medkeep');
        $data['success'] = '';
        if ($this->session->userdata('success')) {
            $success = $this->session->userdata('success');
            $data['success'] = $success;
        }
        $data['danger'] = '';
        if ($this->session->userdata('danger')) {
            $danger = $this->session->userdata('danger');
            $data['danger'] = $danger;
        }
        $this->_renderView($data);
    }

    public function adminCitas()
    {
      if (!$this->session->userdata('id_usuario') && $this->session->userdata('cod_tipo_usuario') == 3) {
            redirect('admin/login');
        }else {
          redirect('admin/adminUsuarios');
        }
        $data = array();
        $data['contentView'] = 'admin/admin_citas';
        $data['lista_citas'] = $this->citas_m->obtenerListaCitas();
        $data['scripts'] = array('medkeep');
        $this->_renderView($data);
    }

    //Doctores

    public function editarDoctor($id)
    {
      if (!$this->session->userdata('id_usuario') && $this->session->userdata('cod_tipo_usuario') == 3) {
            redirect('admin/login');
        }else {
          redirect('admin/adminUsuarios');
        }
        $doctor = $this->doctor_m->obtenerDatosDoctor($id);
        $activo = $this->input->post('estatus');
        $nombre = $this->input->post('nombre');
        $ape_paterno = $this->input->post('ape-paterno');
        $ape_materno = $this->input->post('ape-materno');
        $correo = $this->input->post('correo');
        $domicilio = $this->input->post('domicilio');
        $colonia = $this->input->post('colonia');
        $ciudad = $this->input->post('ciudad');
        $telefono = $this->input->post('telefono');
        $especialidad = $this->input->post('especialidad');
        $cedula = $this->input->post('ced-prof');

        $datos = array(
          'correo' => $correo,
          'nombre' => $nombre,
          'ape_paterno' => $ape_paterno,
          'ape_materno' => $ape_materno,
          'domicilio' => $domicilio,
          'colonia' => $colonia,
          'ciudad' => $ciudad,
          'telefono' => $telefono,
          'activo' => $activo,
          'fecha_actualizado' => date('Y-m-d H:i:s'),
        );

       //var_dump($datos);
        if ($this->usuario_m->actualizarUsuario($datos, $id)) {
            if ($doctor) {
                $datos_doctor = array(
                'cod_usuario' => $id,
                'especialidad' => $especialidad,
                'cedula_profesional' => $cedula,
                'fecha_actualizado' => date('Y-m-d H:i:s'),
              );
                if ($this->doctor_m->actualizarDatosDoctor($datos_doctor, $id)) {
                    $this->session->set_userdata('success', 'Doctor actualizado correctamente.');
                } else {
                    $this->session->set_userdata('danger', 'No se pudo actualizar los datos del doctor, intentelo de nuevo.');
                }
            } else {
                $datos_doctor = array(
                'cod_usuario' => $id,
                'especialidad' => $especialidad,
                'cedula_profesional' => $cedula,
                'fecha_creado' => date('Y-m-d H:i:s'),
              );
                $this->doctor_m->registrarDatosDoctor($datos_doctor, $id);
                $this->session->set_userdata('success', 'Datos del doctor guardados correctamente.');
            }
        } else {
            $this->session->set_userdata('danger', 'No se pudo actualizar el doctor, intentelo de nuevo.');
        }
        redirect('admin/detalleDoctor/'.$id);
    }

    public function detalleDoctor($id)
    {
      if (!$this->session->userdata('id_usuario') && $this->session->userdata('cod_tipo_usuario') == 3) {
            redirect('admin/login');
        }else {
          redirect('admin/adminUsuarios');
        }
        $data = array();
        $data['contentView'] = 'admin/detalle_doctor';
        $data['usuario'] = $this->usuario_m->obtenerDetalleUsuario($id);
        $data['datos_doctor'] = $this->doctor_m->obtenerDatosDoctor($id);
        $data['scripts'] = array('medkeep');
        $data['success'] = '';
        if ($this->session->userdata('success')) {
            $success = $this->session->userdata('success');
            $data['success'] = $success;
        }
        $data['danger'] = '';
        if ($this->session->userdata('danger')) {
            $danger = $this->session->userdata('danger');
            $data['danger'] = $danger;
        }
        $this->_renderView($data);
    }

    public function adminDoctores()
    {
      if (!$this->session->userdata('id_usuario') && $this->session->userdata('cod_tipo_usuario') == 3) {
            redirect('admin/login');
        }else {
          redirect('admin/adminUsuarios');
        }
        $data = array();
        $data['contentView'] = 'admin/admin_doctores';
        $data['lista_doctores'] = $this->doctor_m->obtenerListaDoctores();
        $data['scripts'] = array('medkeep');
        $this->_renderView($data);
    }

    //Usuarios

    public function editarUsuario($id)
    {
      if (!$this->session->userdata('id_usuario') && $this->session->userdata('cod_tipo_usuario') == 3) {
            redirect('admin/login');
        }
        $usuario = $this->usuario_m->obtenerDetalleUsuario($id);
        $activo = $this->input->post('estatus');
        $cod_tipo = $this->input->post('tipo');
        $nombre = $this->input->post('nombre');
        $ape_paterno = $this->input->post('ape-paterno');
        $ape_materno = $this->input->post('ape-materno');
        $pass = $this->input->post('password');
        $correo = $this->input->post('correo');
        $domicilio = $this->input->post('domicilio');
        $colonia = $this->input->post('colonia');
        $ciudad = $this->input->post('ciudad');
        $telefono = $this->input->post('telefono');
        if ($pass !== '') {
            $password = do_hash($pass, 'md5');
        } else {
            $password = $usuario->password;
        }

        $datos = array(
          'correo' => $correo,
          'password' => $password,
          'nombre' => $nombre,
          'ape_paterno' => $ape_paterno,
          'ape_materno' => $ape_materno,
          'domicilio' => $domicilio,
          'colonia' => $colonia,
          'ciudad' => $ciudad,
          'telefono' => $telefono,
          'activo' => $activo,
          'cod_tipo' => $cod_tipo,
          'fecha_actualizado' => date('Y-m-d H:i:s'),
        );
       //var_dump($datos);
        if ($this->usuario_m->actualizarUsuario($datos, $id)) {
            $this->session->set_userdata('success', 'Usuario actualizado correctamente.');
        } else {
            $this->session->set_userdata('danger', 'No se pudo actualizar el usuario, intentelo de nuevo.');
        }
        redirect('admin/detalleUsuario/'.$id);
    }

    public function detalleUsuario($id)
    {
      if (!$this->session->userdata('id_usuario') && $this->session->userdata('cod_tipo_usuario') == 3) {
            redirect('admin/login');
        }
        $data = array();
        $data['contentView'] = 'admin/detalle_usuario';
        $data['usuario'] = $this->usuario_m->obtenerDetalleUsuario($id);
        $data['tipos_usuario'] = $this->usuario_m->obtenerTipoUsuarios();
        $data['scripts'] = array('medkeep');
        $data['success'] = '';
        if ($this->session->userdata('success')) {
            $success = $this->session->userdata('success');
            $data['success'] = $success;
        }
        $data['danger'] = '';
        if ($this->session->userdata('danger')) {
            $danger = $this->session->userdata('danger');
            $data['danger'] = $danger;
        }
        $this->_renderView($data);
    }

    public function adminUsuarios()
    {
      //var_dump($this->session->userdata('cod_tipo_usuario'));
      //die();
      if (!$this->session->userdata('id_usuario') && $this->session->userdata('cod_tipo_usuario') == 3) {
            redirect('admin/login');
        }
        $data = array();
        $data['contentView'] = 'admin/admin_usuarios';
        $data['lista_usuarios'] = $this->usuario_m->obtenerListaUsuarios();
        $data['scripts'] = array('medkeep');
        $this->_renderView($data);
    }

    public function salir()
    {
        $this->session->sess_destroy();
        redirect('admin/login');
    }

    public function index()
    {

      if (!$this->session->userdata('id_usuario') && $this->session->userdata('cod_tipo_usuario') == 3) {
            redirect('admin/login');
        }
        $data = array();
        $data['contentView'] = 'admin/admin_usuarios';
        $data['scripts'] = array('medkeep');
        $this->_renderView($data);
    }
}
