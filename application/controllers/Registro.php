<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registro extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 		function __construct() {
	         parent::__construct();
					 date_default_timezone_set('America/Mexico_City');
					 $this->load->model('Registro_model', 'registro_m');
	     }

			 private $defaultData = array(
				 'title'			=> 'Medkeep',
				 //'layout' 		=> 'layout/lytDefault',
				 'layout' 		=> 'layout/lytdefault',
				 'contentView' 	=> 'vUndefined',
				 'stylecss'		=> '',
				 );

			private function _renderView($data = array())
			{
				$data = array_merge($this->defaultData, $data);
				$this->load->view($data['layout'], $data);
			}

			public function existe()
			{
				$correo = $this->input->post('correo');
				$existe = $this->registro_m->existeCorreo($correo);
				if ($existe) {
					$respuesta = array('msg' => 'failed');
				}else {
					$respuesta = array('msg' => 'success');
				}
				echo json_encode($respuesta);
			}

			public function solicitud()
			{
				$nombre = $this->input->post('nombre');
				$ape_paterno = $this->input->post('ape-paterno');
				$ape_materno = $this->input->post('ape-materno');
				$correo = $this->input->post('correo');
				$pass = $this->input->post('password');
				$telefono = $this->input->post('telefono');
				 $data = array(
					 'nombre' => $nombre,
					 'ape_paterno' => $ape_paterno,
					 'ape_materno' => $ape_materno,
					 'correo' => $correo,
					 'password' => do_hash($pass, 'md5'),
					 'telefono' => $telefono,
					 'activo' => 1,
					 'cod_tipo' => 1,
					 'fecha_creado' => date('Y-m-d H:i:s'),
				 );

				if ($this->registro_m->registrarUsuario($data)) {
					$respuesta = array('msg' => 'success');
				}else {
						$respuesta = array('msg' => 'failed');
				}

				echo json_encode($respuesta);
			}

		public function index()
		{
			$data = array();
	    $data['contentView'] = 'registro/index';
    	$data['scripts'] = array('medkeep');
	  	$this->_renderView($data);
		}



}
