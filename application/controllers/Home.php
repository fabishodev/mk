<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 		function __construct() {
	         parent::__construct();
					 //$this->load->model('Paquete_model', 'paquete_m');
	     }

			 private $defaultData = array(
				 'title'			=> 'Medkeep',
				 //'layout' 		=> 'layout/lytDefault',
				 'layout' 		=> 'layout/lytdefault',
				 'contentView' 	=> 'vUndefined',
				 'stylecss'		=> '',
				 );

			private function _renderView($data = array())
			{
				$data = array_merge($this->defaultData, $data);
				$this->load->view($data['layout'], $data);
			}


		public function index()
		{
			$data = array();
	    $data['contentView'] = 'home/index';
    	$data['scripts'] = array('medkeep');
	  	$this->_renderView($data);
		}



}
