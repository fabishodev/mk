<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Usuario extends CI_Controller
{
    /**
             * Index Page for this controller.
             *
             * Maps to the following URL
             * 		http://example.com/index.php/welcome
             *	- or -
             * 		http://example.com/index.php/welcome/index
             *	- or -
             * Since this controller is set as the default controller in
             * config/routes.php, it's displayed at http://example.com/
             *
             * So any other public methods not prefixed with an underscore will
             * map to /index.php/welcome/<method_name>
             *
             * @see https://codeigniter.com/user_guide/general/urls.html
             */
            public function __construct()
            {
                parent::__construct();
                 date_default_timezone_set('America/Mexico_City');
                $this->load->model('Usuario_model', 'usuario_m');
                $this->load->model('Doctor_model', 'doctor_m');
                  $this->load->model('Blog_model', 'blog_m');
                  $this->load->model('Documento_model', 'documento_m');
                  $this->load->model('Citas_model', 'citas_m');
                    $this->load->model('Tratamiento_model', 'tratamiento_m');
                      $this->load->library('Notificaciones');

            }

    private $defaultData = array(
                 'title' => 'Medkeep',
                 //'layout' 		=> 'layout/lytDefault',
                 'layout' => 'layout/lytdefault',
                 'contentView' => 'vUndefined',
                 'stylecss' => '',
                 );

    private function _renderView($data = array())
    {
        $data = array_merge($this->defaultData, $data);
        $this->load->view($data['layout'], $data);
    }
    public function editarUsuario($id)
			{
        if (!$this->session->userdata('id_usuario')) {
              redirect('usuario');
          }
        $cod_usuario = $this->session->userdata('id_usuario');
        $u = $this->usuario_m->obtenerDetalleUsuario($id);
        $allowed = array('png', 'jpg','jpeg','gif');
        $nombre_achivo = $_FILES['upl']['name'];
        $extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);
        $directorio = 'media/'.$cod_usuario;
        if(!file_exists($directorio)){
            mkdir($directorio, 0777);
        }

        if (isset($_FILES['upl']) && $_FILES['upl']['error'] == 0) {
            if (!in_array(strtolower($extension), $allowed)) {
                $this->session->set_userdata('danger', 'Extension no permitida.');
                redirect('usuario/perfil');
            }

            $archivo = 'media/'.$cod_usuario.'/'.$nombre_achivo;

            if (file_exists($archivo)) {
                unlink($archivo);
            }

            if (move_uploaded_file($_FILES['upl']['tmp_name'], 'media/'.$cod_usuario.'/'.$_FILES['upl']['name'])) {
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'media/'.$cod_usuario.'/'.$nombre_achivo;
                $config['create_thumb'] = false;
                $config['maintain_ratio'] = true;
                $config['width'] = 1280;
                $config['height'] = 440;
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $foto = $nombre_achivo;
            } else {
                $this->session->set_userdata('danger', 'No se pudo agregar la imagen, intentelo de nuevo.');
                redirect('usuario/perfil');
            }
        } else {
            $foto = $u->foto;
        }


				$correo = $this->input->post('correo');
				$telefono = $this->input->post('telefono');
				$pass = $this->input->post('password');
				$nombre = $this->input->post('nombre');
				$ape_paterno = $this->input->post('ape-paterno');
				$ape_materno = $this->input->post('ape-materno');
				if ($pass !== '') {
					$password = do_hash($pass, 'md5');
				} else {
						$password = $u->password;
				}
				$datos = array(
					'correo' => $correo,
					'telefono' => $telefono,
					'password' => $password,
					'nombre' => $nombre,
					'ape_paterno' => $ape_paterno,
					'ape_materno' => $ape_materno,
          'foto' => $foto,
					'fecha_actualizado' => date('Y-m-d H:i:s'),
				);
			 //var_dump($datos);die();
				if ($this->usuario_m->actualizarUsuario($datos,$id)) {
						$this->session->set_userdata('success', 'Usuario actualizado correctamente.');
				} else {
						$this->session->set_userdata('danger', 'No se pudo actualizar el usuario, intentelo de nuevo.');
				}
				redirect('usuario/perfil');
			}

    public function perfil()
    {
      if (!$this->session->userdata('id_usuario')) {
            redirect('usuario');
        }
        $cod_usuario = $this->session->userdata('id_usuario');
        $data = array();
        $data['contentView'] = 'usuario/perfil';
        $data['usuario'] = $this->usuario_m->obtenerDetalleUsuario($cod_usuario);
        $data['scripts'] = array('medkeep');
        $data['success'] = '';
        if ($this->session->userdata('success')) {
            $success = $this->session->userdata('success');
            $data['success'] = $success;
        }
        $data['danger'] = '';
        if ($this->session->userdata('danger')) {
            $danger = $this->session->userdata('danger');
            $data['danger'] = $danger;
        }
        $this->_renderView($data);
    }

    public function determinarClaseEvento()
    {
      $eventos = array("event-success", "event-info", "event-inverse", "event-special");
      $clase = $randomElement = $eventos[array_rand($eventos, 1)];
      return  $clase;
    }

    public function eliminarTratamiento($id)
    {
      if (!$this->session->userdata('id_usuario')) {
            redirect('usuario');
        }

      if ($this->tratamiento_m->eliminarTratamientoPaciente($id)) {
          $this->session->set_userdata('success', 'Tratamiento eliminado correctamente.');
        }else {
          $this->session->set_userdata('danger', 'No se pudo eliminar el tratamiento, intentelo de nuevo.');
      }
      redirect('usuario/tratamiento');
    }

    public function editarTratamiento($id)
    {
      if (!$this->session->userdata('id_usuario')) {
            redirect('usuario');
        }

      $email = $this->session->userdata('correo');
      $nombre_completo = $this->session->userdata('nombre_completo');
      $medicamento = $this->input->post('medicamento-modal');
      $fecha_inicio = $this->input->post('fecha-inicio-tratamiento-modal');
      $fecha_fin = $this->input->post('fecha-fin-tratamiento-modal');
      $hora_tratamiento = $this->input->post('hora-tratamiento-modal');
      $hora_fin = $this->input->post('hora-fin-tratamiento-modal');
      $lapso = $this->input->post('lapso-modal');
      $observaciones = $this->input->post('observacion-modal');
      $fecha_start = date("d-m-Y", strtotime($fecha_inicio)).' '.date("G:i a", strtotime($hora_tratamiento));
      $fecha_end = date("d-m-Y", strtotime($fecha_fin)).' '.date("G:i a", strtotime($hora_tratamiento));
      $start =  $this->_formatear($fecha_start);
      $end = $this->_formatear($fecha_end);
      $title = '<p>Tratamiento de: '.$medicamento.'</p><p>Tomar cada: '.  $lapso.' (hrs)</p><p>Hora Inicio: '.date("G:i", strtotime($hora_tratamiento)).'</p><p>Hora Fin: '.date("G:i", strtotime($hora_fin)).'</p><p>Observaciones: '.  $observaciones.'</p>';
      $body = '<p>Tratamiento de: '.$medicamento.'</p><p>Tomar cada: '.  $lapso.' (hrs)</p><p>Hora Inicio: '.date("G:i", strtotime($hora_tratamiento)).'</p><p>Hora Fin: '.date("G:i", strtotime($hora_fin)).'</p><p>Observaciones: '.  $observaciones.'</p>';
      $class = $this->determinarClaseEvento();


      $tratamiento = array(
        'fecha_inicio' => $fecha_inicio,
        'fecha_fin' => $fecha_fin,
        'hora' => date("G:i", strtotime($hora_tratamiento)),
        'hora_fin' => date("G:i", strtotime($hora_fin)),
        'lapso' => $lapso,
        'medicamento' => $medicamento,
        'observaciones' => $observaciones,
        'fecha_creado' =>  date('Y-m-d H:i:s'),
        'title' => $title,
        'body' => $body,
        'url' => null,
        'class' => $class,
        'start' => $start,
        'end' => $end,
      );

      //echo '<pre>';var_dump($tratamiento); die();

      if ($this->tratamiento_m->actualizarTratamiento($tratamiento,$id)) {
        try {
          if ($this->notificaciones->EnviarCorreoTratamientoActualizado($nombre_completo, $email,$medicamento, $observaciones,  date("d-m-Y", strtotime($fecha_inicio)),date("d-m-Y", strtotime($fecha_fin)), date("G:i", strtotime($hora_tratamiento)),date("G:i", strtotime($hora_fin)),$lapso)) {
            $this->session->set_userdata('success', 'Se actualizado el tratamiento correctamente.');
          }else {
            $this->session->set_userdata('danger', 'No se pudo enviar el correo de actualización de tratamiento.');
          }
        } catch (Exception $e) {
          $this->session->set_userdata('danger', 'No se pudo enviar el correo de actualización de tratamiento.');
        }

      }else {
          $this->session->set_userdata('danger', 'No se pudo actualizar el tratamiento, intentelo de nuevo.');
      }
      redirect('usuario/tratamiento');

    }

    public function detalleTratamiento()
    {
      $id = $this->input->post('id');
        $data = array();
        $data['contentView'] = 'usuario/detalle_tratamiento';
        $data['layout'] = 'layout/lytvacio';
        $data['tratamiento'] = $this->tratamiento_m->obtenerDetalleTratamiento($id);
        $data['scripts'] = array('medkeep');
        $data['success'] = '';
        $this->_renderView($data);
    }

    public function eliminarCita($id)
    {
      if (!$this->session->userdata('id_usuario')) {
            redirect('usuario');
        }

      if ($this->citas_m->eliminarCitaPaciente($id)) {
        if ($this->citas_m->eliminarCitaPacienteProgramada($id)) {
          $this->session->set_userdata('success', 'Cita eliminada correctamente.');
        }else {
            $this->session->set_userdata('danger', 'No se pudo eliminar la cita programada, intentelo de nuevo.');
        }
      }else {
          $this->session->set_userdata('danger', 'No se pudo eliminar la cita, intentelo de nuevo.');
      }
      redirect('usuario/cita');
    }

    public function editarCita($id)
    {
      if (!$this->session->userdata('id_usuario')) {
            redirect('usuario');
        }

        $fecha_cita = $this->input->post('fecha-cita-modal');
        $hora_cita = $this->input->post('hora-cita-modal');
        $motivo_cita = $this->input->post('motivo-cita-modal');
        $doctor = $this->input->post('doctor-modal');
        $hospital = $this->input->post('hospital-modal');

        $email = $this->session->userdata('correo');
        $nombre_completo = $this->session->userdata('nombre_completo');
        // $doctor = $this->doctor_m->obtenerDetalleDoctor($cod_doctor);
        // $nombre_doctor = $doctor->nombre.' '.$doctor->ape_paterno.' '.$doctor->ape_materno;
        $nombre_doctor  = $doctor;

        $fecha_hora = date("d-m-Y", strtotime($fecha_cita)).' '.date("G:i", strtotime($hora_cita));
        $start =  $this->_formatear($fecha_hora);
        $end = $this->_formatear($fecha_hora);
        $title = '<p>Cita con Dr(a): '.$nombre_doctor.'</p><p>Motivo: '.  $motivo_cita.'</p><p>Hora: '.date("G:i", strtotime($hora_cita)).'</p>';
        $body = $motivo_cita;
        $class = 'event-success';

        $datos = array(
        'doctor' => $doctor,
        'hospital' => $hospital,
          'fecha_cita' => $fecha_cita,
          'hora_cita' => date("G:i", strtotime($hora_cita)),
          'confirmacion' => 1,
          'motivo_cita' => $motivo_cita,
          'fecha_actualizado' => date('Y-m-d H:i:s'),
        );

        $programacion = array(
          'title' => $title,
          'body' => $body,
          'url' => null,
          'class' => $class,
          'start' => $start,
          'end' => $end,
        );

       //var_dump($datos);
        if ($this->citas_m->actualizarCita($datos, $id)) {
            if ($this->citas_m->actualizarProgramacionCita($programacion, $id)) {
              try {
                if ($this->notificaciones->EnviarCorreoEditarCita($nombre_completo, $email, $nombre_doctor, $hospital,$motivo_cita, date("d-m-Y", strtotime($fecha_cita)), date("G:i", strtotime($hora_cita)))) {
                  $this->session->set_userdata('success', 'Se actualizado la cita correctamente.');
                }
                else {
                  $this->session->set_userdata('danger', 'No se pudo enviar correo de programación de la cita.');
                }
              } catch (Exception $e) {
                $this->session->set_userdata('danger', 'No se pudo enviar correo de reprogramación de la cita.');
              }


            }else {
                $this->session->set_userdata('danger', 'No se pudo actualizar la programación de la cita, intentelo de nuevo.');
            }

        } else {
            $this->session->set_userdata('danger', 'No se pudo actualizar la cita, intentelo de nuevo.');
        }
        redirect('usuario/cita');
    }

    public function detalleCita()
    {
      $id = $this->input->post('id');
        $data = array();
        $data['contentView'] = 'usuario/detalle_cita';
        $data['layout'] = 'layout/lytvacio';
        $data['cita'] = $this->citas_m->obtenerDetalleCita($id);
        $data['scripts'] = array('medkeep');
        $data['success'] = '';
        $this->_renderView($data);
    }

    public function eliminarDocumento($id)
    {
      //$this->load->helper('file');
      $documento = $this->documento_m->obtenerDetalleDocumento($id);
      $archivo = 'media/'.$documento->cod_usuario.'/'.$documento->nombre_archivo;
      //var_dump($archivo); die();
      if ($this->documento_m->eliminarDocumentoPaciente($id)) {
          unlink($archivo);
        $this->session->set_userdata('success', 'Documento eliminado correctamente.');
      }else {
        $this->session->set_userdata('danger', 'No se pudo eliminar el documento, intentelo de nuevo.');
      }
      redirect('usuario/documentos');

    }

    public function suscribirse()
    {
      if (!$this->session->userdata('id_usuario')) {
            redirect('usuario');
        }
        $data = array();
        $data['contentView'] = 'usuario/suscribirse';
        $data['scripts'] = array('medkeep');
        $this->_renderView($data);
    }


    public function validarNumDocumentos()
    {
          $cod_usuario = $this->session->userdata('id_usuario');
          $usuario = $this->documento_m->obtenerNumDocumentosUsuario($cod_usuario);

          if ($usuario->cod_tipo == 1) {
            if ($usuario->num_documentos >= 1) {
              $respuesta = array('msg' => 'max_documentos');
            }else {
              $respuesta = array('msg' => 'min_documentos');
            }
          }else {
              $respuesta = array('msg' => 'min_documentos');
          }
          echo json_encode($respuesta);
      }

    public function validarNumTratamientos()
    {
          $cod_usuario = $this->session->userdata('id_usuario');
          $usuario = $this->tratamiento_m->obtenerNumTratamientosUsuario($cod_usuario);

          if ($usuario->cod_tipo == 1) {
            if ($usuario->num_tratamientos >= 1) {
              $respuesta = array('msg' => 'max_tratamientos');
            }else {
                $respuesta = array('msg' => 'min_tratamientos');
            }
          }else {
            $respuesta = array('msg' => 'min_tratamientos');
          }
          echo json_encode($respuesta);
      }

    public function validarNumCitas()
    {
          $cod_usuario = $this->session->userdata('id_usuario');
          $usuario = $this->citas_m->obtenerNumCitasUsuario($cod_usuario);

          if ($usuario->cod_tipo == 1) {
            if ($usuario->num_citas >= 1) {
              $respuesta = array('msg' => 'max_citas');
            }else {
                $respuesta = array('msg' => 'min_citas');
            }
          }else {
            $respuesta = array('msg' => 'min_citas');
          }
          echo json_encode($respuesta);
      }

    public function contactar()
    {
      $nombre = $this->input->post('contact_name');
      $correo = $this->input->post('contact_email');
      $asunto = $this->input->post('contact_subject');
      $mensaje = $this->input->post('message');

       $data = array(
         'nombre' => $nombre,
         'correo' => $correo,
         'asunto' => $asunto,
         'mensaje' => $mensaje,
         'fecha_creado' => date('Y-m-d H:i:s'),
       );

      if ($this->usuario_m->registrarMensaje($data)) {
        $respuesta = array('msg' => 'success');
      }else {
          $respuesta = array('msg' => 'failed');
      }

      echo json_encode($respuesta);
    }


    public function agregarTratamiento()
    {
      if (!$this->session->userdata('id_usuario')) {
            redirect('usuario');
        }

        $cod_usuario = $this->session->userdata('id_usuario');
        $email = $this->session->userdata('correo');
        $nombre_completo = $this->session->userdata('nombre_completo');
        $fecha_inicio = $this->input->post('fecha-inicio');
        $fecha_fin = $this->input->post('fecha-fin');
        $hora_tratamiento = $this->input->post('hora-tratamiento');
        $hora_fin = $this->input->post('hora-fin');
        $lapso = $this->input->post('lapso');
        $medicamento = $this->input->post('medicamento');
        $observaciones = $this->input->post('observaciones');
        $fecha_start = date("d-m-Y", strtotime($fecha_inicio)).' '.date("G:i", strtotime($hora_tratamiento));
        $fecha_end = date("d-m-Y", strtotime($fecha_fin)).' '.date("G:i", strtotime($hora_fin));
        $start =  $this->_formatear($fecha_start);
        $end = $this->_formatear($fecha_end);
        $title = '<p>Tratamiento de: '.$medicamento.'</p><p>Tomar cada: '.  $lapso.' (hrs)</p><p>Hora Inicio: '.date("G:i", strtotime($hora_tratamiento)).'</p><p>Hora Fin: '.date("G:i", strtotime($hora_fin)).'</p><p>Observaciones: '.  $observaciones.'</p>';
        $body = '<p>Tratamiento de: '.$medicamento.'</p><p>Tomar cada: '.  $lapso.' (hrs)</p><p>Hora Inicio: '.date("G:i", strtotime($hora_tratamiento)).'</p><p>Hora Fin: '.date("G:i", strtotime($hora_fin)).'</p><p>Observaciones: '.  $observaciones.'</p>';
        $class = $this->determinarClaseEvento();


      $tratamiento = array(
        'cod_usuario' => $cod_usuario,
        'fecha_inicio' => $fecha_inicio,
        'fecha_fin' => $fecha_fin,
        'hora' => date("G:i", strtotime($hora_tratamiento)),
        'hora_fin' => date("G:i", strtotime($hora_fin)),
        'lapso' => $lapso,
        'medicamento' => $medicamento,
        'observaciones' => $observaciones,
        'fecha_creado' =>  date('Y-m-d H:i:s'),
        'title' => $title,
        'body' => $body,
        'url' => null,
        'class' => $class,
        'start' => $start,
        'end' => $end,
      );

        if ($this->usuario_m->registrarTratamientoUsuario($tratamiento)) {
          try {
            if ($this->notificaciones->EnviarCorreoTratamiento($nombre_completo, $email,$medicamento, $observaciones,  date("d-m-Y", strtotime($fecha_inicio)),date("d-m-Y", strtotime($fecha_fin)), date("G:i", strtotime($hora_tratamiento)),date("G:i", strtotime($hora_fin)),$lapso)) {
              $respuesta = array('msg' => 'success');
            }else {
              $respuesta = array('msg' => 'failed-email');
            }
          } catch (Exception $e) {
            $respuesta = array('msg' => 'failed-email');
          }

        }else {
            $respuesta = array('msg' => 'failed');
        }
      echo json_encode($respuesta);
    }

    public function obtenerCitasCalendario()
      {
        if($this->input->is_ajax_request())
        {
          $events = $this->usuario_m->obtenerCalendarioCitas();
          echo json_encode(
            array(
              "success" => 1,
              "result" => $events
            )
          );
        }
      }

    public function obtenerTratamientos($id)
    	{
    		if($this->input->is_ajax_request())
    		{
    			$events = $this->usuario_m->obtenerTratamientosUsuarios($id);
    			echo json_encode(
    				array(
    					"success" => 1,
    					"result" => $events
    				)
    			);
    		}
    	}

    public function obtenerCitas($id)
    	{
    		if($this->input->is_ajax_request())
    		{
    			$events = $this->usuario_m->obtenerCitasUsuarios($id);
    			echo json_encode(
    				array(
    					"success" => 1,
    					"result" => $events
    				)
    			);
    		}
    	}

      // Formatear una fecha a microtime para añadir al evento, tipo 1401517498985.
    function _formatear($fecha)
    {
      //return strtotime(substr($fecha, 0, 4)."-".substr($fecha, 6, 2)."-".substr($fecha, 9, 2)." " .substr($fecha, 10, 6)) * 1000;
    	return strtotime(substr($fecha, 6, 4)."-".substr($fecha, 3, 2)."-".substr($fecha, 0, 2)." " .substr($fecha, 10, 6)) * 1000;
    }

    public function salir()
    {
        $this->session->sess_destroy();
        redirect('home');
    }

    public function agregarDocumento($id)
  {
    if (!$this->session->userdata('id_usuario')) {
          redirect('usuario');
      }

      $documento = $this->documento_m->obtenerNumDocumentosUsuario($id);

      if ($documento->cod_tipo == 1) {
        if ($documento->num_documentos >= 1) {
          $this->session->set_userdata('danger', 'Haz alcanzado el número máximo permitido para guardar documentos como usuario registrado. Suscríbete a nuestra aplicación. Consulta la sección Usuario/Suscribirse.');
          redirect ('usuario/documentos');
        }
      }

      $allowed = array('pdf','png','jpg','gif','jpeg');
      $nombre_achivo = $_FILES['upl']['name'];
      $extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);
      $id_usuario = $id;
      $directorio = 'media/'.$id_usuario;
      $archivo = 'media/'.$id_usuario.'/'.$nombre_achivo;
      $ruta = $archivo;
      if(!file_exists($directorio)){
            $dirmake = mkdir($directorio, 0777);
      }
      if (isset($_FILES['upl']) && $_FILES['upl']['error'] == 0) {
        if (!in_array(strtolower($extension), $allowed)) {
            $this->session->set_userdata('danger', 'Documento con extensión no permitida.');
            redirect ('usuario/documentos');
        }

          if ($_FILES["upl"]["size"] > 3000000) {
              $this->session->set_userdata('danger', 'Documento demasiado grande para almacenarlo dentro del sistema.');
              redirect ('usuario/documentos');
          }

          if (!file_exists($archivo)) {
              if (move_uploaded_file($_FILES['upl']['tmp_name'], $directorio.'/'.$_FILES['upl']['name'])) {
                  $documento = $nombre_achivo;
                  $cod_doctor = $this->input->post('sel-doctor');
                  $doctor = $this->input->post('doctor');
                  $hospital = $this->input->post('hospital');
                  $tipo_documento = $this->input->post('sel-tipo');
                  $nombre = $this->input->post('nombre-documento');
                  $descripcion = $this->input->post('descripcion');

                  $datos = array(
                          'cod_usuario' => $id,
                          'cod_doctor' => $cod_doctor,
                          'doctor' => $doctor,
                          'hospital' => $hospital,
                          'cod_tipo_documento' =>   $tipo_documento,
                          'nombre' => $nombre,
                          'descripcion' => $descripcion,
                          'ruta' => $ruta,
                          'nombre_archivo' => $nombre_achivo,
                          'fecha_creado' => date('Y-m-d H:i:s'),
                        );
                  if ($this->usuario_m->guardarDocumento($datos)) {
                      $this->session->set_userdata('success', 'Documento guardado correctamente.');
                        redirect ('usuario/documentos');
                  }
              } else {
                  $this->session->set_userdata('danger', 'No se pudo agregar el documento, intentelo de nuevo.');
                  redirect ('usuario/documentos');
              }
          } else {
              $this->session->set_userdata('danger', 'Ya existe un archivo con ese nombre, cambie el nombre al archivo e intentelo de nuevo.');

                redirect ('usuario/documentos');
          }
      }
  }


    public function enviarComentario($id)
    {
      if (!$this->session->userdata('id_usuario')) {
            redirect('usuario');
        }
      $cod_usuario = $this->input->post('id-usuario');;
      $asunto = $this->input->post('asunto');
      $comentario = $this->input->post('comentario');

      $datos = array(
        'cod_usuario' => $cod_usuario,
        'cod_noticia' => $id,
        'asunto' => $asunto,
        'comentario' => $comentario,
        'publicado' => 0,
        'fecha_creado' =>  date('Y-m-d H:i:s'),
      );

      if ($this->blog_m->guardarComentario($datos)) {
        $this->session->set_userdata('success', 'Su comentario se ha guardado correctamente, pronto será revisado y autorizado.');
    } else {
        $this->session->set_userdata('danger', 'No se pudo guardar su comentario, intentelo de nuevo.');
    }
      redirect('blog/noticia/'.$id);
    }

    public function agendar()
    {
      if (!$this->session->userdata('id_usuario')) {
            redirect('usuario');
        }

        $cod_usuario = $this->session->userdata('id_usuario');
        $email = $this->session->userdata('correo');
        $nombre_completo = $this->session->userdata('nombre_completo');
        $cod_doctor = $this->input->post('sel-doctor');
        $doctor = $this->input->post('doctor');
        $hospital = $this->input->post('hospital');
        $fecha_cita = $this->input->post('fecha-cita');
        $hora_cita = $this->input->post('hora-cita');
        $motivo_cita = $this->input->post('motivo-cita');
        $fecha_hora = date("d-m-Y", strtotime($fecha_cita)).' '.date("G:i", strtotime($hora_cita));


        //$doctor = $this->doctor_m->obtenerDetalleDoctor($cod_doctor);
        //$nombre_doctor = $doctor->nombre.' '.$doctor->ape_paterno.' '.$doctor->ape_materno;
        $nombre_doctor = $doctor;


        $start =  $this->_formatear($fecha_hora);
        $end = $this->_formatear($fecha_hora);
        $title = '<p>Cita con Dr(a): '.$nombre_doctor.'</p><p>Motivo: '.  $motivo_cita.'</p><p>En: '.$hospital.'</p><p>Hora: '.date("G:i", strtotime($hora_cita)).'</p>';
        $body = $motivo_cita;
        $class = 'event-warning';


      $cita = array(
        'cod_usuario' => $cod_usuario,
        'cod_doctor' => $cod_doctor,
        'doctor' => $doctor,
        'hospital' => $hospital,
        'fecha_cita' => $fecha_cita,
        'hora_cita' => date("G:i", strtotime($hora_cita)),
        'motivo_cita' => $motivo_cita,
        'confirmacion' => 1,
        'notificacion'=> 0,
        'fecha_creado' =>  date('Y-m-d H:i:s'),
      );
        $id = $this->usuario_m->registrarCitaUsuario($cita);
      if ($id) {
        $programacion = array(
          'id' => $id,
          'title' => $title,
          'body' => $body,
          'url' => null,
          'class' => $class,
          'start' => $start,
          'end' => $end,
        );
        if ($this->usuario_m->registrarProgramacionCitaUsuario($programacion)) {

            try {
              if ($this->notificaciones->EnviarCorreoAgendarCita($nombre_completo, $email, $nombre_doctor, $hospital,$motivo_cita, date("d-m-Y", strtotime($fecha_cita)), date("G:i", strtotime($hora_cita)))) {
                $respuesta = array('msg' => 'success');
                //echo json_encode($respuesta);
              }else {
                  $respuesta = array('msg' => 'failed-email');
              }
            } catch (Exception $e) {
              $respuesta = array('msg' => 'failed-email');
            }

        }else {
            $respuesta = array('msg' => 'failed');
        }
      }
      else {
          $respuesta = array('msg' => 'failed');
      }
      echo json_encode($respuesta);
    }

    public function ingresar()
    {
      $correo = $this->input->post('correo');
      $password = $this->input->post('password');
      $usuario = $this->usuario_m->obtenerUsuario($correo, $password);
				//var_dump($usuario);
        if ($usuario) {
          if ($usuario->activo == 0) {
						$respuesta = array('msg' => 'inactive');
          }else {
						$this->session->set_userdata('cod_tipo_usuario', $usuario->cod_tipo);
						$this->session->set_userdata('id_usuario', $usuario->id);
						$this->session->set_userdata('correo', $usuario->correo);
						$this->session->set_userdata('nombre_completo', $usuario->nombre.' '.$usuario->ape_paterno.' '.$usuario->ape_materno);
            $this->session->set_userdata('activo', $usuario->activo);
						$this->session->set_userdata('danger', '');
						//redirect('usuario/registrados');
						$respuesta = array('msg' => 'active');
					}
        } else {
            $respuesta = array('msg' => 'failed');
					}
				echo json_encode($respuesta);
    }

    public function documentos()
    {
      $id_usuario = $this->session->userdata('id_usuario');
      if (!$id_usuario) {
            redirect('usuario');
        }
        $data = array();
        $data['contentView'] = 'usuario/documentos';
        $data['lista_documentos']  = $this->usuario_m->obtenerListaDocumentos($id_usuario);
        $data['lista_doctores']  = $this->doctor_m->obtenerListaDoctores();
        $data['tipos_documento']  = $this->documento_m->obtenerTiposDocumento();
        $data['scripts'] = array('medkeep');
        $data['success'] = '';
        if ($this->session->userdata('success')) {
            $success = $this->session->userdata('success');
            $data['success'] = $success;
        }
        $data['danger'] = '';
        if ($this->session->userdata('danger')) {
            $danger = $this->session->userdata('danger');
            $data['danger'] = $danger;
        }
        $this->_renderView($data);
    }

    public function tratamiento()
    {
      if (!$this->session->userdata('id_usuario')) {
            redirect('usuario');
        }
          $cod_usuario = $this->session->userdata('id_usuario');
        $data = array();
        $data['contentView'] = 'usuario/tratamiento';
        $data['scripts'] = array('medkeep');
        $data['lista_tratamientos']  = $this->tratamiento_m->obtenerListaTratamientosUsuario($cod_usuario);
        $data['success'] = '';
        if ($this->session->userdata('success')) {
            $success = $this->session->userdata('success');
            $data['success'] = $success;
        }
        $data['danger'] = '';
        if ($this->session->userdata('danger')) {
            $danger = $this->session->userdata('danger');
            $data['danger'] = $danger;
        }
        $this->_renderView($data);
    }

    public function cita()
    {
      if (!$this->session->userdata('id_usuario')) {
            redirect('usuario');
        }
          $cod_usuario = $this->session->userdata('id_usuario');
        $data = array();
        $data['contentView'] = 'usuario/cita';
        $data['lista_doctores']  = $this->doctor_m->obtenerListaDoctores();
        $data['citas_proximas']  = $this->citas_m->obtenerProximasCitas($cod_usuario);
        $data['cita'] = $this->citas_m->obtenerDetalleCita(1);
        $data['scripts'] = array('medkeep');
        $data['success'] = '';
        if ($this->session->userdata('success')) {
            $success = $this->session->userdata('success');
            $data['success'] = $success;
        }
        $data['danger'] = '';
        if ($this->session->userdata('danger')) {
            $danger = $this->session->userdata('danger');
            $data['danger'] = $danger;
        }
        $this->_renderView($data);
    }

    public function registrados()
    {
			if (!$this->session->userdata('id_usuario')) {
            redirect('usuario');
        }
        $data = array();
        $data['contentView'] = 'usuario/registrados';
        $data['scripts'] = array('medkeep');
        $this->_renderView($data);
    }

    public function index()
    {
        $data = array();
        $data['contentView'] = 'usuario/index';
        $data['scripts'] = array('medkeep');
        $this->_renderView($data);
    }
}
