$(document).ready(function(){
  //var base_url = "http://medkeep.dev/";
  var base_url = "http://localhost:8888/mk/";

  $('#sel-doctor').change(function() {
      var cod = $('#sel-doctor').get(0).value;
                $.ajax({
                      type: "POST",
                      url: base_url + 'index.php/admin/datosDoctor/',
                      data: {id: cod },
                      dataType: "html",
                      success: function (result) {
                          data = JSON.parse(result);
                          $("#telefono-doc").val(data.telefono);
                          $("#correo-doctor").val(data.correo);
                          $("#especialidad").val(data.especialidad);
                      },
                      error: function (xhr, status) { alert(status); }
                  });
  });

  $( "#form-user" ).submit(function( event ) {
    var errors = "";

    var app_email_address = document.getElementById("correo");
    var app_password = document.getElementById("password");

    if (app_email_address.value == "") {
      errors += 'Ingrese un correo electrónico.';
    } else if (checkcontact(app_email_address.value) == false) {
      errors += 'Ingrese un correo electrónico válido.';
    } else if (app_password.value != "") {
      if (checkmaxminpassword(app_password.value)==false) {
        errors += 'Ingrese una contraseña mayor a 5 caracteres y menor de 8 caracteres.';
      }
    }

    if (errors) {
      alert(errors);
       $('.alert-danger').show();
      event.preventDefault();
    }

  });




  $(".submenu > a").click(function(e) {
    e.preventDefault();
    var $li = $(this).parent("li");
    var $ul = $(this).next("ul");

    if($li.hasClass("open")) {
      $ul.slideUp(350);
      $li.removeClass("open");
    } else {
      $(".nav > li > ul").slideUp(350);
      $(".nav > li").removeClass("open");
      $ul.slideDown(350);
      $li.addClass("open");
    }
  });

});

function checkmaxminpassword(input) {
  if (input.length <= 8 && input.length >= 5) {
    return true;
  } else {
    return false;
  }
}

function checkcontact(input) {
  var pattern1 = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
  if (pattern1.test(input)) {
    return true;
  } else {
    return false;
  }
}
