<!-- Mobile Menu -->
//var base_url = "http://medkeep.dev/";
var base_url = "http://localhost:8888/mk/";

$(document).ready(function() {
  "use strict";
  $("#menu").mmenu({
    "classes": "mm-slide",
    "offCanvas": {
      "position": "right"
    },
    "footer": {
      "add": true,
      "title": "Copyrights 2015 Medical Guide. all rights reserved."
    },

    "header": {
      "title": "Medical Guide",
      "add": true,
      "update": true
    },

  });
});



<!-- Sticky Header -->

<!-- Header One -->
$(window).scroll(function() {
  if ($(this).scrollTop() > 1) {
    $('header').addClass("sticky");
  } else {
    $('header').removeClass("sticky");
  }
});


<!-- Header Two -->
$(window).scroll(function() {
  if ($(this).scrollTop() > 1) {
    $('.header2').addClass("sticky");
  } else {
    $('.header2').removeClass("sticky");
  }
});






<!-- Smooth Scrol -->
$(function() {

  var $window = $(window); //Window object

  var scrollTime = 0.6; //Scroll time
  var scrollDistance = 235; //Distance. Use smaller value for shorter scroll and greater value for longer scroll

  $window.on("mousewheel DOMMouseScroll", function(event) {

    event.preventDefault();

    var delta = event.originalEvent.wheelDelta / 125 || -event.originalEvent.detail / 3;
    var scrollTop = $window.scrollTop();
    var finalScroll = scrollTop - parseInt(delta * scrollDistance);

    TweenMax.to($window, scrollTime, {
      scrollTo: {
        y: finalScroll,
        autoKill: true
      },
      ease: Power1.easeOut,
      autoKill: true,
      overwrite: 5
    });

  });

});




<!-- Time Table -->
$(function() {
  var Accordion = function(el, multiple) {
    this.el = el || {};
    this.multiple = multiple || false;

    // Variables privadas
    var links = this.el.find('.link');
    // Evento
    links.on('click', {
      el: this.el,
      multiple: this.multiple
    }, this.dropdown)
  }

  Accordion.prototype.dropdown = function(e) {
    var $el = e.data.el;
    $this = $(this),
      $prev = $this.prev();

    $prev.slideToggle();
    $this.parent().toggleClass('open');

    if (!e.data.multiple) {
      $el.find('.submenu').not($prev).slideUp().parent().removeClass('open');
    };
  }

  var accordion = new Accordion($('#accordion2'), false);
});







<!-- Make an Appointment Accordion -->
var Accordion = function(el, multiple) {
  this.el = el || {};
  this.multiple = multiple || false;

  // Variables privadas
  var links = this.el.find('.link');
  // Evento
  links.on('click', {
    el: this.el,
    multiple: this.multiple
  }, this.dropdown)
}

Accordion.prototype.dropdown = function(e) {
  var $el = e.data.el;
  $this = $(this),
    $next = $this.next();

  $next.slideToggle();
  $this.parent().toggleClass('open');

  if (!e.data.multiple) {
    $el.find('.bgcolor-3').not($next).slideUp().parent().removeClass('open');
  };
}

var accordion = new Accordion($('#accordion'), false);





<!-- Why Choose Accordion -->
$(function() {
  var Accordion = function(el, multiple) {
    this.el = el || {};
    this.multiple = multiple || false;

    // Variables privadas
    var links = this.el.find('.link');
    // Evento
    links.on('click', {
      el: this.el,
      multiple: this.multiple
    }, this.dropdown)
  }

  Accordion.prototype.dropdown = function(e) {
    var $el = e.data.el;
    $this = $(this),
      $next = $this.next();

    $next.slideToggle();
    $this.parent().toggleClass('open');

    if (!e.data.multiple) {
      $el.find('.submenu-active').not($next).slideUp().parent().removeClass('open');
      $el.find('.submenu').not($next).slideUp().parent().removeClass('open');

    };
  }

  var accordion = new Accordion($('#why-choose'), false);
});





<!-- Date Picker and input hover -->
// trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim

[].slice.call(document.querySelectorAll('input.input__field')).forEach(function(inputEl) {
  // in case the input is already filled..

  // events:
  inputEl.addEventListener('focus', onInputFocus);
  inputEl.addEventListener('blur', onInputBlur);
});

function onInputFocus(ev) {
  classie.add(ev.target.parentNode, 'input--filled');
}

function onInputBlur(ev) {
  if (ev.target.value.trim() === '') {
    classie.remove(ev.target.parentNode, 'input--filled');
  }
}

//date picker
$("#datepicker").datepicker({
  inline: true
});


[].slice.call(document.querySelectorAll('textarea.input__field')).forEach(function(inputEl) {
  // in case the input is already filled..
  if (inputEl.value.trim() !== '') {
    classie.add(inputEl.parentNode, 'input--filled');
  }

  // events:
  inputEl.addEventListener('focus', onInputFocus);
  inputEl.addEventListener('blur', onInputBlur);
});


//date picker
$("#datepicker").datepicker({
  inline: true
});




<!-- Welcome Tabs -->
/* jQuery activation and setting options for the tabs*/
var tabbedNav = $("#tabbed-nav").zozoTabs({
  orientation: "horizontal",
  theme: "silver",
  position: "top-left",
  size: "medium",
  animation: {
    duration: 600,
    easing: "easeOutQuint",
    effects: "fade"
  },
  defaultTab: "tab1"
});

/* Changing animation effects*/
$("#config input.effects").change(function() {
  var effects = $('input[type=radio]:checked').attr("id");
  tabbedNav.data("zozoTabs").setOptions({
    "animation": {
      "effects": effects
    }
  });
});






<!-- All Carousel -->
<!-- Home News-Posts Carousel -->
$("#owl-demo").owlCarousel({
  items: 3,
  lazyLoad: true,
  navigation: true
});



$("#owl-demo4").owlCarousel({
  items: 3,
  lazyLoad: true,
  navigation: true
});



<!-- Testimonials Carousel -->
$("#owl-demo2").owlCarousel({
  autoPlay: 111110,
  stopOnHover: true,

  paginationSpeed: 1000,
  goToFirstSpeed: 2000,
  singleItem: true,
  autoHeight: true,

});





<!-- Team Detail -->
$("#team-detail").owlCarousel({

  navigation: true,
  slideSpeed: 300,
  paginationSpeed: 400,
  singleItem: true

  // "singleItem:true" is a shortcut for:
  // items : 1,
  // itemsDesktop : false,
  // itemsDesktopSmall : false,
  // itemsTablet: false,
  // itemsMobile : false

});




<!-- Home2 services slide Carousel -->
$("#services-slide").owlCarousel({

  navigation: true,
  slideSpeed: 300,
  paginationSpeed: 400,
  singleItem: true

  // "singleItem:true" is a shortcut for:
  // items : 1,
  // itemsDesktop : false,
  // itemsDesktopSmall : false,
  // itemsTablet: false,
  // itemsMobile : false

});


<!-- Blog images slide Carousel -->
$("#blog-slide").owlCarousel({

  navigation: true,
  slideSpeed: 300,
  paginationSpeed: 400,
  singleItem: true

  // "singleItem:true" is a shortcut for:
  // items : 1,
  // itemsDesktop : false,
  // itemsDesktopSmall : false,
  // itemsTablet: false,
  // itemsMobile : false

});





<!-- Back to Top -->
jQuery(document).ready(function($) {
  // browser window scroll (in pixels) after which the "back to top" link is shown
  var offset = 300,
    //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
    offset_opacity = 1200,
    //duration of the top scrolling animation (in ms)
    scroll_top_duration = 1400,
    //grab the "back to top" link
    $back_to_top = $('.cd-top');

  //hide or show the "back to top" link
  $(window).scroll(function() {
    ($(this).scrollTop() > offset) ? $back_to_top.addClass('cd-is-visible'): $back_to_top.removeClass('cd-is-visible cd-fade-out');
    if ($(this).scrollTop() > offset_opacity) {
      $back_to_top.addClass('cd-fade-out');
    }
  });

  //smooth scroll to top
  $back_to_top.on('click', function(event) {
    event.preventDefault();
    $('body,html').animate({
      scrollTop: 0,
    }, scroll_top_duration);
  });

});





//Procedures Links
var Accordion = function(el, multiple) {
  this.el = el || {};
  this.multiple = multiple || false;

  // Variables privadas
  var links = this.el.find('.link');
  // Evento
  links.on('click', {
    el: this.el,
    multiple: this.multiple
  }, this.dropdown)
}

Accordion.prototype.dropdown = function(e) {
  var $el = e.data.el;
  $this = $(this),
    $next = $this.next();

  $next.slideToggle();
  $this.parent().toggleClass('open');

  if (!e.data.multiple) {
    $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
  };
}

var accordion = new Accordion($('#procedures-links'), false);



//Procedures FAQ'S
var Accordion = function(el, multiple) {
  this.el = el || {};
  this.multiple = multiple || false;

  // Variables privadas
  var links = this.el.find('.link');
  // Evento
  links.on('click', {
    el: this.el,
    multiple: this.multiple
  }, this.dropdown)
}

Accordion.prototype.dropdown = function(e) {
  var $el = e.data.el;
  $this = $(this),
    $next = $this.next();

  $next.slideToggle();
  $this.parent().toggleClass('open');

  if (!e.data.multiple) {
    $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
  };
}

var accordion = new Accordion($('#procedures-faq'), false);


//PreLoader
jQuery(window).load(function() { // makes sure the whole site is loaded
  jQuery('#status').fadeOut(); // will first fade out the loading animation
  jQuery('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
  jQuery('body').delay(350).css({
    'overflow': 'visible'
  });
})

// Appointment newsletter contact Form
function validateNumDocumentos() {
    $.ajax({
      type: "POST",
      url: base_url + 'index.php/usuario/validarNumDocumentos',
      success: function(msg) {
        var result = JSON.parse(msg);
        alert(result);
        if (result.msg == 'max_documentos') {
          alert('Haz alcanzado el número máximo permitido para guardar documentos como usuario registrado. Suscríbete a nuestra aplicación. Consulta la sección Usuario/Suscribirse.');
          return false;
        }
        return true;
      }
    });
}

function validateFormTratamiento() {
  //alert('hi');
  var errors = "";
  var app_fecha_inicio = document.getElementById("fecha-inicio");
  var app_fecha_fin = document.getElementById("fecha-fin");
  var app_hora = document.getElementById("hora-tratamiento");
  var app_hora_fin = document.getElementById("hora-fin");
  var app_lapso = document.getElementById("lapso");
  var app_medicamento = document.getElementById("medicamento");
  var app_observaciones = document.getElementById("observaciones");


  if (app_fecha_inicio.value == "") {
    errors += 'Seleccione fecha de inicio.';
  } else if (app_fecha_fin.value == "") {
    errors += 'Seleccione fecha de fin.';
  } else if (app_hora.value == "") {
    errors += 'Seleccione hora inicio para su tratamiento.';
  } else if (app_hora_fin.value == "") {
    errors += 'Seleccione hora fin para su tratamiento.';
  } else if (app_lapso.value == "") {
    if (isNaN(app_lapso.value)) {
        errors += 'Ingrese un número de horas entre 1 y 24.';
    }else if (app_lapso.value >= 0){
        errors += 'Ingrese un número de horas entre 1 y 24.';
    }
    errors += 'Ingrese el número de horas para cada toma de tratamiento entre 1 y 24.';
  } else if (app_medicamento.value == "") {
    errors += 'Ingrese nombre del medicamento.';
  }else if (app_observaciones.value == "") {
    errors += 'Ingrese observaciones del tratamiento.';
  }

  if (errors) {
    alert(errors);
    return false;
  } else {
    $.ajax({
      type: "POST",
      url: base_url + 'index.php/usuario/validarNumTratamientos',
      data: $("#appointment_form").serialize(),
      success: function(msg) {
        var result = JSON.parse(msg);
        if (result.msg == 'max_tratamientos') {
          alert('Haz alcanzado el número máximo permitido para guardar tratamientos como usuario registrado. Suscríbete a nuestra aplicación. Consulta la sección Usuario/Suscribirse.');
          return false;
        } else {
          $.ajax({
            type: "POST",
            url: base_url + 'index.php/usuario/agregarTratamiento',
            data: $("#appointment_form").serialize(),
            success: function(msg) {
              var result = JSON.parse(msg);
              if (result.msg == 'failed') {
                alert('No se pudo agregar el tratamiento, intentelo de nuevo.')
                return false;
              }else if(result.msg == 'failed-email') {
                alert('Su tratamiento fue agregado correctamente, favor de verificar correo para recibir notificaciones.');
                window.location = base_url + 'index.php/usuario/cita';
              } else {
                  alert('Su tratamiento fue agregado correctamente.');
              window.location= base_url + 'index.php/usuario/tratamiento';
              }
            }
          });
        }
      }
    });

  }
}

function checkformathourdate(input) {
  var pattern1 = /^(?:2[0-3]|[01][0-9]):[0-5][0-9]$/;
  if (pattern1.test(input)) {
    return true;
  } else {
    return false;
  }
}

function checkformatphone(input) {
  var pattern1 = /^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/;
  if (pattern1.test(input)) {
    return true;
  } else {
    return false;
  }
}

function checkmaxminpassword(input) {
  if (input.length <= 8 && input.length >= 5) {
    return true;
  } else {
    return false;
  }
}

function checkcontact(input) {
  var pattern1 = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
  if (pattern1.test(input)) {
    return true;
  } else {
    return false;
  }
}

function validateFieldDate() {
  //alert('hi');
  var errors = "";
  var app_fecha = document.getElementById("fecha-cita");
  var app_hora = document.getElementById("hora-cita");
  var app_doctor = document.getElementById("doctor");
  var app_hospital = document.getElementById("hospital");
  var app_motivo = document.getElementById("motivo-cita");


  if (app_fecha.value == "") {
    errors += 'Seleccione fecha para su cita.';
  } else if (app_hora.value == "") {
    errors += 'Seleccione fecha para su cita.';
  } else if (app_doctor.value == "") {
    errors += 'Escriba Doctor(a) para su cita.';
  } else if (app_hospital.value == "") {
    errors += 'Escriba el hospital para su cita.';
  } else if (app_motivo.value == "") {
    errors += 'Ingrese el motivo de su cita.';
  }

  if (errors) {
    alert(errors);
    return false;
  } else {
    $.ajax({
      type: "POST",
      url: base_url + 'index.php/usuario/validarNumCitas',
      data: $("#appointment_form").serialize(),
      success: function(msg) {
          var result = JSON.parse(msg);
          if (result.msg == 'max_citas') {
            alert('Haz alcanzado el número máximo permitido para agendar citas como usuario registrado.')
            return false;
          }else{
            $.ajax({
              type: "POST",
              url: base_url + 'index.php/usuario/agendar',
              data: $("#appointment_form").serialize(),
              success: function(msg) {
                var result = JSON.parse(msg);
                if (result.msg == 'failed') {
                  alert('No se pudo registrar la cita, intentelo de nuevo.');
                  return false;
                } else if(result.msg == 'failed-email') {
                  alert('Su cita fue agendada correctamente, favor de verificar correo para recibir notificaciones.');
                  window.location = base_url + 'index.php/usuario/cita';
                }else {
                  alert('Su cita fue agendada correctamente.');
                  window.location = base_url + 'index.php/usuario/cita';
                }
              }
            });
          }
      }
    });

  }
}

function validateUpdateUser() {
  //alert('hi');
  var errors = "";

  var app_email_address = document.getElementById("correo");
  var app_password = document.getElementById("password");

  if (app_email_address.value == "") {
    errors += 'Ingrese un correo electrónico.';
  } else if (checkcontact(app_email_address.value) == false) {
    errors += 'Ingrese un correo electrónico válido.';
  } else if (app_password.value == "") {
    errors += 'Ingrese una contraseña.';
  }else if (checkmaxminpassword(app_password.value)==false) {
    errors += 'Ingrese una contraseña mayor a 5 caracteres y menor de 8 caracteres.';
  }

  if (errors) {
    $(".alert").hide();
    $(".alert-danger").show();
    //document.getElementById("error").style.display = "block";
    document.getElementsByClassName('alert-danger').innerHTML = errors;
    return false;
  }
}

function validateUserRegister() {
  //alert('hi');
  var errors = "";

  var app_email_address = document.getElementById("correo");
  var app_password = document.getElementById("password");

  if (app_email_address.value == "") {
    errors += 'Ingrese un correo electrónico.';
  } else if (checkcontact(app_email_address.value) == false) {
    errors += 'Ingrese un correo electrónico válido.';
  } else if (app_password.value == "") {
    errors += 'Ingrese una contraseña.';
  }else if (checkmaxminpassword(app_password.value)==false) {
    errors += 'Ingrese una contraseña mayor a 5 caracteres y menor de 8 caracteres.';
  }

  if (errors) {
    document.getElementById("error").style.display = "block";
    document.getElementById("error").innerHTML = errors;
    return false;
  } else {
    $.ajax({
      type: "POST",
      url: base_url + 'index.php/usuario/ingresar',
      data: $("#appointment_form").serialize(),
      success: function(msg) {
        var result = JSON.parse(msg);
        if (result.msg == 'failed') {
          document.getElementById("error").style.display = "none";
          document.getElementById("correo").value = "";
          document.getElementById("password").value = "";
          document.getElementById("error").style.display = "block";
          document.getElementById("error").innerHTML = "Oops! No reconocemos correo y/o contraseña, intentelo de nuevo.";
        }else if (result.msg == 'inactive') {
          document.getElementById("error").style.display = "none";
          document.getElementById("correo").value = "";
          document.getElementById("password").value = "";
          document.getElementById("error").style.display = "block";
          document.getElementById("error").innerHTML = "Oops! Su cuenta no ha sido activada, contacte al administrador.";
        }else if(result.msg == 'active') {
          window.location= base_url + 'index.php/usuario/registrados';
        }
      }
    });
  }
}

function validateAppointment() {
  //alert('hi');
  var errors = "";

  var app_name = document.getElementById("nombre");
  var app_first = document.getElementById("ape-paterno");
  var app_last = document.getElementById("ape-materno");
  var app_email_address = document.getElementById("correo");
  var app_password = document.getElementById("password");
  var app_phone = document.getElementById("telefono");

  if (app_name.value == "") {
    errors += 'Ingrese su nombre.';
  } else if (app_first.value == "") {
    errors += 'Ingrese su apellido paterno.';
  } else if (app_last.value == "") {
    errors += 'Ingrese su apellido materno.';
  } else if (app_email_address.value == "") {
    errors += 'Ingrese un correo electrónico.';
  } else if (checkcontact(app_email_address.value) == false) {
    errors += 'Ingrese un correo electrónico válido.';
  }else if (checkformatphone(app_phone.value)==false) {
    errors += 'Ingrese un teléfono con formato correcto, 10 números sin espacios.';
  } else if (app_phone.value == "") {
    errors += 'Ingrese un numero de télefono.';
  } else if (app_password.value == "") {
    errors += 'Ingrese una contraseña.';
  }else if (checkmaxminpassword(app_password.value)==false) {
    errors += 'Ingrese una contraseña mayor a 5 caracteres y menor de 8 caracteres.';
  }


  if (errors) {
    document.getElementById("error").style.display = "block";
    document.getElementById("error").innerHTML = errors;
    return false;
  } else {
    $.ajax({
      type: "POST",
      url: base_url + 'index.php/registro/existe',
      data: $("#appointment_form").serialize(),
      success: function(msg) {
        var result = JSON.parse(msg);
        if (result.msg == 'failed') {
          document.getElementById("error").style.display = "block";
          document.getElementById("error").innerHTML = "Oops! Ya existe el correo proporcionado.";
          return false;
        } else {

          $.ajax({
            type: "POST",
            url: base_url + 'index.php/registro/solicitud',
            data: $("#appointment_form").serialize(),
            success: function(msg) {
              var result = JSON.parse(msg);
              if (result.msg == 'success') {
                document.getElementById("error").style.display = "none";
                document.getElementById("nombre").value = "";
                document.getElementById("ape-paterno").value = "";
                document.getElementById("ape-materno").value = "";
                document.getElementById("correo").value = "";
                document.getElementById("telefono").value = "";
                document.getElementById("password").value = "";
                $("#appointment_form").hide();
                document.getElementById("success").style.display = "block";
                document.getElementById("success").innerHTML = "Gracias por registrarse, a partir de ahora ya puede utilizar nuestra plataforma!!";
              } else {
                document.getElementById("error").style.display = "block";
                document.getElementById("error").innerHTML = "Oops! Un error ha ocurrido durante el registro, intentelo de nuevo.";
              }
            }

          });

        }
      }

    });



  }
}

function validateSubscription() {
  //alert('hi');

  var footer_name = document.getElementById("subscribe_name");
  var footer_email_address = document.getElementById("subscribe_email");

  if (footer_name.value == "") {
    footer_name.className = "input error";
    return false;
  } else if (footer_email_address.value == "") {
    footer_email_address.className = "input error";
    return false;
  } else if (checkcontact(footer_email_address.value) == false) {
    footer_email_address.className = "input error";
    return false;
  } else {

    $.ajax({
      type: "POST",
      url: 'process.php',
      data: $("#subscribe_form").serialize(),
      success: function(msg) {
        if (msg == 'success') {
          footer_name.className = "input";
          footer_name.value = "";
          footer_email_address.className = "input";
          footer_email_address.value = "";

          $("#subscribe_form").hide();
          document.getElementById("subscribe_success").style.display = "block";
          document.getElementById("subscribe_success").innerHTML = "Thank You! We'll contact you shortly.";
        } else {
          document.getElementById("subscribe_error").style.display = "block";
          document.getElementById("subscribe_error").innerHTML = "Oops! Something went wrong while prceeding.";
        }
      }

    });

  }
}

function removeChecks() {
  var footer_name = document.getElementById("subscribe_name");
  var footer_email_address = document.getElementById("subscribe_email");

  if (footer_name.value != "") {
    footer_name.className = "input";

  }
  if (footer_email_address.value != "" && checkcontact(footer_email_address.value) == true) {
    footer_email_address.className = "input";

  }

}


function validateContact() {
  //alert('hi');
  var errors = "";

  var contact_name = document.getElementById("contact_name");
  var contact_email_address = document.getElementById("contact_email");
  var contact_subject = document.getElementById("contact_subject");

  if (contact_name.value == "") {
    errors += 'Please provide your name.';
  } else if (contact_email_address.value == "") {
    errors += 'Please provide an email address.';
  } else if (checkcontact(contact_email_address.value) == false) {
    errors += 'Please provide a valid email address.';
  } else if (contact_subject.value == "") {
    errors += 'Please provide a subject.';
  }


  if (errors) {
    document.getElementById("error").style.display = "block";
    document.getElementById("error").innerHTML = errors;
    return false;
  } else {

    $.ajax({
      type: "POST",
      url: base_url + 'index.php/usuario/contactar',
      data: $("#contact_form").serialize(),
      success: function(msg) {
          var result = JSON.parse(msg);
        if (result.msg == 'success') {
          document.getElementById("error").style.display = "none";
          document.getElementById("contact_name").value = "";
          document.getElementById("contact_email").value = "";
          document.getElementById("contact_subject").value = "";
          document.getElementById("message").value = "";
          $("#contact_form").hide();
          document.getElementById("success").style.display = "block";
          document.getElementById("success").innerHTML = "Gracias! Pronto nos pondremos en contacto contigo.";
        } else {
          document.getElementById("error").style.display = "block";
          document.getElementById("error").innerHTML = "Oops! Algo ha sucedido al enviar tu mensaje, intentalo de nuevo.";
        }
      }

    });

  }
}

//Switcher
jQuery(document).ready(function($) {

  jQuery("#default-color").click(function() {
    jQuery("#color").attr("href", "css/default-color.css");
    //jQuery(".link img" ).attr("src", "images/timetable-menu-brown.png");
    return false;
  });

  jQuery("#brown").click(function() {
    jQuery("#color").attr("href", "css/theme-colors/brown.css");
    jQuery(".link img.time-tab").attr("src", "images/timetable-menu-brown.png");
    return false;
  });

  jQuery("#pink").click(function() {
    jQuery("#color").attr("href", "css/theme-colors/pink.css");
    jQuery(".link img.time-tab").attr("src", "images/timetable-menu-pink.png");
    return false;
  });

  jQuery("#dark-blue").click(function() {
    jQuery("#color").attr("href", "css/theme-colors/dark-blue.css");
    jQuery(".link img.time-tab").attr("src", "images/timetable-menu-dark-blue.png");
    return false;
  });


  jQuery("#green").click(function() {
    jQuery("#color").attr("href", "css/theme-colors/green.css");
    jQuery(".link img.time-tab").attr("src", "images/timetable-menu-green.png");
    return false;
  });

  jQuery("#light-green").click(function() {
    jQuery("#color").attr("href", "css/theme-colors/light-green.css");
    jQuery(".link img.time-tab").attr("src", "images/timetable-menu-light-green.png");
    return false;
  });


  jQuery("#orange").click(function() {
    jQuery("#color").attr("href", "css/theme-colors/orange.css");
    jQuery(".link img.time-tab").attr("src", "images/timetable-menu-orange.png");
    return false;
  });

  jQuery("#light-blue").click(function() {
    jQuery("#color").attr("href", "css/theme-colors/light-blue.css");
    jQuery(".link img.time-tab").attr("src", "images/timetable-menu-light-blue.png");
    return false;
  });

  jQuery("#purple").click(function() {
    jQuery("#color").attr("href", "css/theme-colors/purple.css");
    jQuery(".link img.time-tab").attr("src", "images/timetable-menu-purple.png");
    return false;
  });

  jQuery("#red").click(function() {
    jQuery("#color").attr("href", "css/theme-colors/red.css");
    jQuery(".link img.time-tab").attr("src", "images/timetable-menu-red.png");
    return false;
  });

  jQuery("#yellow").click(function() {
    jQuery("#color").attr("href", "css/theme-colors/yellow.css");
    jQuery(".link img.time-tab").attr("src", "images/timetable-menu-yellow.png");
    return false;
  });



  jQuery("#light").click(function() {
    jQuery("#footer").addClass("footer-light");
    jQuery("#footer").removeClass("footer");
    //jQuery("#footer img" ).attr("src", "images/footer-logo.jpg");
  });
  jQuery("#dark").click(function() {
    jQuery("#footer").addClass("footer");
    jQuery("#footer").removeClass("footer-light");
    //jQuery("#footer img" ).attr("src", "images/footer-logo-dark.jpg");
  });

  jQuery("#header-one").click(function() {
    jQuery("#header-1").show();
    jQuery("#header-2").hide();
  });
  jQuery("#header-two").click(function() {
    jQuery("#header-2").show();
    jQuery("#header-1").hide();
  });



  // picker buttton
  jQuery(".picker_close").click(function() {

    jQuery("#choose_color").toggleClass("position");

  });



});
