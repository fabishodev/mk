/*  medkeep.js
 */
var medkeep = window.medkeep || {};
medkeep.funciones = (function() {
  var base_url = "http://localhost:8888/mk/";
  return {
    alerta_eliminar_documento: function() {
      $(".eliminar-documento").click(function (e) {
            if (confirm("¿Estas seguro de realizar esta acción?")) {
              return true;
            } else {
                e.preventDefault();
            }

        });
      },
    alerta_eliminar_tratamiento: function() {
      $(".eliminar-tratamiento").click(function (e) {
            if (confirm("¿Estas seguro de realizar esta acción?")) {
              return true;
            } else {
                e.preventDefault();
            }

        });
      },
    obtener_detalle_tratamiento: function() {
      $(document).on('click', '.link-obtener-tratamiento', function(e) {
         var id = $(this).children(".id-tratamiento").val();
         $('#detalle-tratamiento').load(base_url+'index.php/usuario/detalleTratamiento', {
           id: id
         });

      });
    },
    alerta_eliminar_cita: function() {
      $(".eliminar-cita").click(function (e) {
            if (confirm("¿Estas seguro de realizar esta acción?")) {
              return true;
            } else {
                e.preventDefault();
            }

        });
      },
    obtener_detalle_cita: function() {
      $(document).on('click', '.link-obtener-cita', function(e) {
         var id = $(this).children(".id-cita").val();
         $('#detalle-cita').load(base_url+'index.php/usuario/detalleCita', {
           id: id
         });

      });
    },
    menu_home: function() {
      $("#home").addClass("item-select");
      $("#servicios").removeClass("item-select");
      $("#registro").removeClass("item-select");
      $("#blog").removeClass("item-select");
      $("#contacto").removeClass("item-select");
      $("#usuario").removeClass("item-select");
      $("#cita").removeClass("item-select");
      $("#tratamiento").removeClass("item-select");
      $("#documento").removeClass("item-select");
      $("#suscribirse").removeClass("item-select");
      $("#perfil").removeClass("item-select");
    },
    menu_servicios: function() {
      $("#home").removeClass("item-select");
      $("#servicios").addClass("item-select");
      $("#registro").removeClass("item-select");
      $("#blog").removeClass("item-select");
      $("#contacto").removeClass("item-select");
      $("#usuario").removeClass("item-select");
      $("#cita").removeClass("item-select");
      $("#tratamiento").removeClass("item-select");
      $("#documento").removeClass("item-select");
      $("#suscribirse").removeClass("item-select");
      $("#perfil").removeClass("item-select");
    },
    menu_registro: function() {
      $("#home").removeClass("item-select");
      $("#servicios").removeClass("item-select");
      $("#registro").addClass("item-select");
      $("#blog").removeClass("item-select");
      $("#contacto").removeClass("item-select");
      $("#usuario").removeClass("item-select");
      $("#cita").removeClass("item-select");
      $("#tratamiento").removeClass("item-select");
      $("#documento").removeClass("item-select");
      $("#suscribirse").removeClass("item-select");
      $("#perfil").removeClass("item-select");
    },
    menu_blog: function() {
      $("#home").removeClass("item-select");
      $("#servicios").removeClass("item-select");
      $("#registro").removeClass("item-select");
      $("#blog").addClass("item-select");
      $("#contacto").removeClass("item-select");
      $("#usuario").removeClass("item-select");
      $("#cita").removeClass("item-select");
      $("#tratamiento").removeClass("item-select");
      $("#documento").removeClass("item-select");
      $("#suscribirse").removeClass("item-select");
      $("#perfil").removeClass("item-select");
    },
    menu_contacto: function() {
      $("#home").removeClass("item-select");
      $("#servicios").removeClass("item-select");
      $("#registro").removeClass("item-select");
      $("#blog").removeClass("item-select");
      $("#contacto").addClass("item-select");
      $("#usuario").removeClass("item-select");
      $("#cita").removeClass("item-select");
      $("#tratamiento").removeClass("item-select");
      $("#documento").removeClass("item-select");
      $("#suscribirse").removeClass("item-select");
      $("#perfil").removeClass("item-select");
    },
    menu_usuario: function() {
      $("#home").removeClass("item-select");
      $("#servicios").removeClass("item-select");
      $("#registro").removeClass("item-select");
      $("#blog").removeClass("item-select");
      $("#contacto").removeClass("item-select");
      $("#usuario").addClass("item-select");
      $("#cita").removeClass("item-select");
      $("#tratamiento").removeClass("item-select");
      $("#documento").removeClass("item-select");
      $("#suscribirse").removeClass("item-select");
      $("#perfil").removeClass("item-select");
    },
    menu_usuario_cita: function() {
      $("#home").removeClass("item-select");
      $("#servicios").removeClass("item-select");
      $("#registro").removeClass("item-select");
      $("#blog").removeClass("item-select");
      $("#contacto").removeClass("item-select");
      $("#usuario").removeClass("item-select");
      $("#cita").addClass("item-select");
      $("#tratamiento").removeClass("item-select");
      $("#documento").removeClass("item-select");
      $("#suscribirse").removeClass("item-select");
      $("#perfil").removeClass("item-select");
    },
    menu_usuario_tratamiento: function() {
      $("#home").removeClass("item-select");
      $("#servicios").removeClass("item-select");
      $("#registro").removeClass("item-select");
      $("#blog").removeClass("item-select");
      $("#contacto").removeClass("item-select");
      $("#usuario").removeClass("item-select");
      $("#cita").removeClass("item-select");
      $("#tratamiento").addClass("item-select");
      $("#documento").removeClass("item-select");
      $("#suscribirse").removeClass("item-select");
      $("#perfil").removeClass("item-select");
    },
    menu_usuario_documento: function() {
      $("#home").removeClass("item-select");
      $("#servicios").removeClass("item-select");
      $("#registro").removeClass("item-select");
      $("#blog").removeClass("item-select");
      $("#contacto").removeClass("item-select");
      $("#usuario").removeClass("item-select");
      $("#cita").removeClass("item-select");
      $("#tratamiento").removeClass("item-select");
      $("#documento").addClass("item-select");
      $("#suscribirse").removeClass("item-select");
      $("#perfil").removeClass("item-select");
    },
    menu_suscribirse: function() {
      $("#home").removeClass("item-select");
      $("#servicios").removeClass("item-select");
      $("#registro").removeClass("item-select");
      $("#blog").removeClass("item-select");
      $("#contacto").removeClass("item-select");
      $("#usuario").removeClass("item-select");
      $("#cita").removeClass("item-select");
      $("#tratamiento").removeClass("item-select");
      $("#documento").removeClass("item-select");
      $("#suscribirse").addClass("item-select");
      $("#perfil").removeClass("item-select");
    },
    menu_usuario_perfil: function() {
      $("#home").removeClass("item-select");
      $("#servicios").removeClass("item-select");
      $("#registro").removeClass("item-select");
      $("#blog").removeClass("item-select");
      $("#contacto").removeClass("item-select");
      $("#usuario").removeClass("item-select");
      $("#cita").removeClass("item-select");
      $("#tratamiento").removeClass("item-select");
      $("#documento").removeClass("item-select");
      $("#suscribirse").removeClass("item-select");
      $("#perfil").addClass("item-select");
    },
    menu_home_mobile: function() {
      $("#home-mobile").addClass("select");
      $("#servicios-mobile").removeClass("select");
      $("#registro-mobile").removeClass("select");
      $("#blog-mobile").removeClass("select");
      $("#contacto-mobile").removeClass("select");
      $("#usuario-mobile").removeClass("select");
      $("#cita-mobile").removeClass("select");
      $("#tratamiento-mobile").removeClass("select");
      $("#documento-mobile").removeClass("select");
      $("#suscribirse-mobile").removeClass("select");
      $("#perfil-mobile").removeClass("select");
    },
    menu_servicios_mobile: function() {
      $("#home-mobile").removeClass("select");
      $("#servicios-mobile").addClass("select");
      $("#registro-mobile").removeClass("select");
      $("#blog-mobile").removeClass("select");
      $("#contacto-mobile").removeClass("select");
      $("#usuario-mobile").addClass("select");
      $("#cita-mobile").removeClass("select");
      $("#tratamiento-mobile").removeClass("select");
      $("#documento-mobile").removeClass("select");
      $("#suscribirse-mobile").removeClass("select");
      $("#perfil-mobile").removeClass("select");
    },
    menu_registro_mobile: function() {
      $("#home-mobile").removeClass("select");
      $("#servicios-mobile").removeClass("select");
      $("#registro-mobile").addClass("select");
      $("#blog-mobile").removeClass("select");
      $("#contacto-mobile").removeClass("select");
      $("#usuario-mobile").addClass("select");
      $("#cita-mobile").removeClass("select");
      $("#tratamiento-mobile").removeClass("select");
      $("#documento-mobile").removeClass("select");
      $("#suscribirse-mobile").removeClass("select");
      $("#perfil-mobile").removeClass("select");
    },
    menu_blog_mobile: function() {
      $("#home-mobile").removeClass("select");
      $("#servicios-mobile").removeClass("select");
      $("#registro-mobile").removeClass("select");
      $("#blog-mobile").addClass("select");
      $("#contacto-mobile").removeClass("select");
      $("#usuario-mobile").addClass("select");
      $("#cita-mobile").removeClass("select");
      $("#tratamiento-mobile").removeClass("select");
      $("#documento-mobile").removeClass("select");
      $("#suscribirse-mobile").removeClass("select");
      $("#perfil-mobile").removeClass("select");
    },
    menu_contacto_mobile: function() {
      $("#home-mobile").addClass("select");
      $("#servicios-mobile").removeClass("select");
      $("#registro-mobile").removeClass("select");
      $("#blog-mobile").removeClass("select");
      $("#contacto-mobile").addClass("select");
        $("#usuario-mobile").addClass("select");
      $("#cita-mobile").removeClass("select");
      $("#tratamiento-mobile").removeClass("select");
      $("#documento-mobile").removeClass("select");
      $("#suscribirse-mobile").removeClass("select");
      $("#perfil-mobile").removeClass("select");
    },
    menu_usuario_mobile: function() {
      $("#home-mobile").removeClass("select");
      $("#servicios-mobile").removeClass("select");
      $("#registro-mobile").removeClass("select");
      $("#blog-mobile").removeClass("select");
      $("#contacto-mobile").removeClass("select");
      $("#usuario-mobile").addClass("select");
      $("#cita-mobile").removeClass("select");
      $("#tratamiento-mobile").removeClass("select");
      $("#documento-mobile").removeClass("select");
      $("#suscribirse-mobile").removeClass("select");
      $("#perfil-mobile").removeClass("select");
    },
    menu_usuario_cita_mobile: function() {
      $("#home-mobile").removeClass("select");
      $("#servicios-mobile").removeClass("select");
      $("#registro-mobile").removeClass("select");
      $("#blog-mobile").removeClass("select");
      $("#contacto-mobile").removeClass("select");
        $("#usuario-mobile").addClass("select");
      $("#cita-mobile").addClass("select");
      $("#tratamiento-mobile").removeClass("select");
      $("#documento-mobile").removeClass("select");
      $("#suscribirse-mobile").removeClass("select");
      $("#perfil-mobile").removeClass("select");
    },
    menu_usuario_tratamiento_mobile: function() {
      $("#home-mobile").removeClass("select");
      $("#servicios-mobile").removeClass("select");
      $("#registro-mobile").removeClass("select");
      $("#blog-mobile").removeClass("select");
      $("#contacto-mobile").removeClass("select");
        $("#usuario-mobile").addClass("select");
      $("#cita-mobile").removeClass("select");
      $("#tratamiento-mobile").addClass("select");
      $("#documento-mobile").removeClass("select");
      $("#suscribirse-mobile").removeClass("select");
      $("#perfil-mobile").removeClass("select");
    },
    menu_usuario_documento_mobile: function() {
      $("#home-mobile").removeClass("select");
      $("#servicios-mobile").removeClass("select");
      $("#registro-mobile").removeClass("select");
      $("#blog-mobile").removeClass("select");
      $("#contacto-mobile").removeClass("select");
        $("#usuario-mobile").addClass("select");
      $("#cita-mobile").removeClass("select");
      $("#tratamiento-mobile").removeClass("select");
      $("#documento-mobile").addClass("select");
      $("#suscribirse-mobile").removeClass("select");
      $("#perfil-mobile").removeClass("select");
    },
    menu_suscribirse_mobile: function() {
      $("#home-mobile").removeClass("select");
      $("#servicios-mobile").removeClass("select");
      $("#registro-mobile").removeClass("select");
      $("#blog-mobile").removeClass("select");
      $("#contacto-mobile").removeClass("select");
      $("#usuario-mobile").addClass("select");
      $("#cita-mobile").removeClass("select");
      $("#tratamiento-mobile").removeClass("select");
      $("#documento-mobile").removeClass("select");
      $("#suscribirse-mobile").addClass("select");
      $("#perfil-mobile").removeClass("select");
    },
    menu_perfil_mobile: function() {
        $("#home-mobile").removeClass("select");
        $("#servicios-mobile").removeClass("select");
        $("#registro-mobile").removeClass("select");
        $("#blog-mobile").removeClass("select");
        $("#contacto-mobile").removeClass("select");
        $("#usuario-mobile").addClass("select");
        $("#cita-mobile").removeClass("select");
        $("#tratamiento-mobile").removeClass("select");
        $("#documento-mobile").removeClass("select");
        $("#suscribirse-mobile").removeClass("select");
        $("#perfil-mobile").addClass("select");
      }


  }
})();
